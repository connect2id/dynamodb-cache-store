<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<xs:schema elementFormDefault="qualified" version="1.7"
           targetNamespace="urn:infinispan:config:store:dynamodb:1.7"
           xmlns:tns="urn:infinispan:config:store:dynamodb:1.7"
           xmlns:config="urn:infinispan:config:9.4" xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:import namespace="urn:infinispan:config:9.4"
               schemaLocation="http://www.infinispan.org/schemas/infinispan-config-9.4.xsd" />

    <xs:element name="dynamodb-store" type="tns:dynamodb-store"/>

    <xs:complexType name="dynamodb-store">
        <xs:complexContent>
            <xs:extension base="config:store">

                <xs:attribute name="endpoint" type="xs:string">
                    <xs:annotation>
                        <xs:documentation>
                            The DynamoDB endpoint. If set overrides region.
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>

                <xs:attribute name="region" type="xs:string" default="us-west-2">
                    <xs:annotation>
                        <xs:documentation>
                            The DynamoDB region.
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>

                <xs:attribute name="item-transformer" type="xs:string">
                    <xs:annotation>
                        <xs:documentation>
                            The required class for transforming between Infinispan entries
                            (key / value pairs and optional metadata) and a corresponding
                            DynamoDB item.
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>

                <xs:attribute name="query-executor" type="xs:string">
                    <xs:annotation>
                        <xs:documentation>
                            Optional class for executing direct queries against
                            DynamoDB. If set the list of indexed attributes
                            must also be specified.
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>

                <xs:attribute name="indexed-attributes" type="xs:string">
                    <xs:annotation>
                        <xs:documentation>
                            Optional space or comma separated list of indexed
                            DynamoDB table attributes. Must be specified if a
                            query executor is set.
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>

                <xs:attribute name="consistent-reads" type="xs:boolean" default="false">
                    <xs:annotation>
                        <xs:documentation>
                            If true reads will be consistent. Defaults to
                            false.
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>

                <xs:attribute name="read-capacity" type="xs:int" default="1">
                    <xs:annotation>
                        <xs:documentation>
                            The read capacity to provision when creating a new
                            DynamoDB table and indices. Defaults to 1.
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>

                <xs:attribute name="write-capacity" type="xs:int" default="1">
                    <xs:annotation>
                        <xs:documentation>
                            The write capacity to provision when creating a new
                            DynamoDB table and indices. Defaults to 1.
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>

                <xs:attribute name="encryption-at-rest" type="xs:boolean" default="false">
                    <xs:annotation>
                        <xs:documentation>
                            If true the DynamoDB table will be created with
                            encryption at rest. Defaults to false.
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>

                <xs:attribute name="table-prefix" type="xs:string">
                    <xs:annotation>
                        <xs:documentation>
                            Optional DynamoDB table prefix.
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>

                <xs:attribute name="apply-range-key" type="xs:string">
                    <xs:annotation>
                        <xs:documentation>
                            Optional range key to apply to all DynamoDB
                            operations. If set must constitute a valid key
                            name.
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>

                <xs:attribute name="range-key-value" type="xs:string">
                    <xs:annotation>
                        <xs:documentation>
                            The value of the optional range key. Its type must
                            be string.
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>

                <xs:attribute name="enable-stream" type="xs:boolean" default="false">
                    <xs:annotation>
                        <xs:documentation>
                            If true the DynamoDB table will be created with an
                            enabled stream (intended for a global table).
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>

                <xs:attribute name="enable-continuous-backups" type="xs:boolean" default="false">
                    <xs:annotation>
                        <xs:documentation>
                            If true continuous backups / point in time recovery
                            will be enabled for the DynamoDB table.
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>

                <xs:attribute name="enable-ttl" type="xs:boolean" default="false">
                    <xs:annotation>
                        <xs:documentation>
                            If true automatic expiration of items with a
                            specified TTL attribute will be enabled for the
                            DynamoDB table.
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>

                <xs:attribute name="purge-limit" type="xs:int" default="-1">
                    <xs:annotation>
                        <xs:documentation>
                            If greater than -1 limits the number of expired
                            entries to purge during a run of the expired
                            entry reaper task.
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>

                <xs:attribute name="http-proxy-host" type="xs:string">
                    <xs:annotation>
                        <xs:documentation>
                            The HTTP proxy host, -1 if not specified.
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>

                <xs:attribute name="http-proxy-port" type="xs:int" default="-1">
                    <xs:annotation>
                        <xs:documentation>
                            The HTTP proxy port, -1 if not specified.
                        </xs:documentation>
                    </xs:annotation>
                </xs:attribute>

            </xs:extension>
        </xs:complexContent>
    </xs:complexType>

</xs:schema>