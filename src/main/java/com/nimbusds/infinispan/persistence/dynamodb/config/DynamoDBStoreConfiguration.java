package com.nimbusds.infinispan.persistence.dynamodb.config;


import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.codahale.metrics.MetricRegistry;
import com.nimbusds.common.config.LoggableConfiguration;
import com.nimbusds.infinispan.persistence.dynamodb.DynamoDBStore;
import com.nimbusds.infinispan.persistence.dynamodb.logging.Loggers;
import net.jcip.annotations.Immutable;
import org.infinispan.commons.configuration.BuiltBy;
import org.infinispan.commons.configuration.ConfigurationFor;
import org.infinispan.commons.configuration.attributes.Attribute;
import org.infinispan.commons.configuration.attributes.AttributeDefinition;
import org.infinispan.commons.configuration.attributes.AttributeSet;
import org.infinispan.commons.util.StringPropertyReplacer;
import org.infinispan.configuration.cache.AbstractStoreConfiguration;
import org.infinispan.configuration.cache.AsyncStoreConfiguration;

import java.util.Objects;
import java.util.Properties;
import java.util.Set;


/**
 * DynamoDB store configuration.
 */
@Immutable
@BuiltBy(DynamoDBStoreConfigurationBuilder.class)
@ConfigurationFor(DynamoDBStore.class)
public class DynamoDBStoreConfiguration extends AbstractStoreConfiguration implements LoggableConfiguration {
	
	
	static final AttributeDefinition<String> ENDPOINT = AttributeDefinition.builder("endpoint", null, String.class).build();
	static final AttributeDefinition<Regions> REGION = AttributeDefinition.builder("region", null, Regions.class).build();
	static final AttributeDefinition<Class> ITEM_TRANSFORMER = AttributeDefinition.builder("recordTransformer", null, Class.class).build();
	static final AttributeDefinition<Class> QUERY_EXECUTOR = AttributeDefinition.builder("queryExecutor", null, Class.class).build();
	static final AttributeDefinition<Set> INDEX_ATTRIBUTES = AttributeDefinition.builder("indexAttributes", null, Set.class).build();
	static final AttributeDefinition<Boolean> CONSISTENT_READS = AttributeDefinition.builder("consistentReads", false, Boolean.class).build();
	static final AttributeDefinition<Long> READ_CAPACITY = AttributeDefinition.builder("readCapacity", 1L, Long.class).build();
	static final AttributeDefinition<Long> WRITE_CAPACITY = AttributeDefinition.builder("writeCapacity", 1L, Long.class).build();
	static final AttributeDefinition<Capacity> PURGE_MAX_READ_CAPACITY = AttributeDefinition.builder("purgeMaxReadCapacity", new Capacity(10, Capacity.Measure.PERCENT), Capacity.class).build();
	static final AttributeDefinition<Long> PURGE_LIMIT = AttributeDefinition.builder("purgeLimit", -1L, Long.class).build();
	static final AttributeDefinition<Boolean> ENCRYPTION_AT_REST = AttributeDefinition.builder("encryptionAtRest", false, Boolean.class).build();
	static final AttributeDefinition<Boolean> STREAM = AttributeDefinition.builder("stream", false, Boolean.class).build();
	static final AttributeDefinition<String> TABLE_PREFIX = AttributeDefinition.builder("tablePrefix", "", String.class).build();
	static final AttributeDefinition<MetricRegistry> METRIC_REGISTRY = AttributeDefinition.builder("metricRegistry", null, MetricRegistry.class).build();
	static final AttributeDefinition<String> APPLY_RANGE_KEY = AttributeDefinition.builder("applyRangeKey", null, String.class).build();
	static final AttributeDefinition<String> RANGE_KEY_VALUE = AttributeDefinition.builder("rangeKeyValue", null, String.class).build();
	static final AttributeDefinition<Boolean> DELETION_PROTECTION = AttributeDefinition.builder("deletionProtection", false, Boolean.class).build();
	static final AttributeDefinition<Boolean> CONTINUOUS_BACKUPS = AttributeDefinition.builder("continuousBackups", false, Boolean.class).build();
	static final AttributeDefinition<Boolean> TTL = AttributeDefinition.builder("ttl", false, Boolean.class).build();
	static final AttributeDefinition<String> HTTP_PROXY_HOST = AttributeDefinition.builder("httpProxyHost", null, String.class).build();
	static final AttributeDefinition<Integer> HTTP_PROXY_PORT = AttributeDefinition.builder("httpProxyPort", -1, Integer.class).build();
	static final AttributeDefinition<String> HMAC_SHA_256_KEY = AttributeDefinition.builder("hmacSHA256Key", null, String.class).build();
	
	
	/**
	 * Returns the attribute definitions for the DynamoDB store configuration.
	 *
	 * @return The attribute definitions.
	 */
	public static AttributeSet attributeDefinitionSet() {
		return new AttributeSet(DynamoDBStoreConfiguration.class,
			AbstractStoreConfiguration.attributeDefinitionSet(),
			ENDPOINT,
			REGION,
			ITEM_TRANSFORMER,
			QUERY_EXECUTOR,
			INDEX_ATTRIBUTES,
			CONSISTENT_READS,
			READ_CAPACITY,
			WRITE_CAPACITY,
			PURGE_MAX_READ_CAPACITY,
			PURGE_LIMIT,
			ENCRYPTION_AT_REST,
			STREAM,
			TABLE_PREFIX,
			APPLY_RANGE_KEY,
			RANGE_KEY_VALUE,
			METRIC_REGISTRY,
			DELETION_PROTECTION,
			CONTINUOUS_BACKUPS,
			TTL,
			HTTP_PROXY_HOST,
			HTTP_PROXY_PORT,
			HMAC_SHA_256_KEY);
	}
	
	
	private final Attribute<String> endpoint;
	private final Attribute<Regions> region;
	private final Attribute<Class> itemTransformerClass;
	private final Attribute<Class> queryExecutorClass;
	private final Attribute<Set> indexAttributes;
	private final Attribute<Boolean> consistentRead;
	private final Attribute<Long> readCapacity;
	private final Attribute<Long> writeCapacity;
	private final Attribute<Capacity> purgeMaxReadCapacity;
	private final Attribute<Long> purgeLimit;
	private final Attribute<Boolean> encryptionAtRest;
	private final Attribute<Boolean> stream;
	private final Attribute<String> tablePrefix;
	private final Attribute<String> applyRangeKey;
	private final Attribute<String> rangeKeyValue;
	private final Attribute<MetricRegistry> metricRegistry;
	private final Attribute<Boolean> deletionProtection;
	private final Attribute<Boolean> continuousBackups;
	private final Attribute<Boolean> ttl;
	private final Attribute<String> httpProxyHost;
	private final Attribute<Integer> httpProxyPort;
	private final Attribute<String> hmacSHA256Key;


	/**
	 * Creates a new DynamoDB store configuration.
	 *
	 * @param attributes  The configuration attributes. Must not be
	 *                    {@code null}.
	 * @param asyncConfig Configuration for the async cache loader.
	 */
	public DynamoDBStoreConfiguration(final AttributeSet attributes,
					  final AsyncStoreConfiguration asyncConfig) {

		super(attributes, asyncConfig);
		
		endpoint = Objects.requireNonNull(attributes.attribute(ENDPOINT));
		region = Objects.requireNonNull(attributes.attribute(REGION));
		itemTransformerClass = Objects.requireNonNull(attributes.attribute(ITEM_TRANSFORMER));
		queryExecutorClass = attributes.attribute(QUERY_EXECUTOR);
		indexAttributes = attributes.attribute(INDEX_ATTRIBUTES);
		consistentRead = attributes.attribute(CONSISTENT_READS);
		readCapacity = Objects.requireNonNull(attributes.attribute(READ_CAPACITY));
		writeCapacity = Objects.requireNonNull(attributes.attribute(WRITE_CAPACITY));
		purgeMaxReadCapacity = Objects.requireNonNull(attributes.attribute(PURGE_MAX_READ_CAPACITY));
		purgeLimit = attributes.attribute(PURGE_LIMIT);
		encryptionAtRest = attributes.attribute(ENCRYPTION_AT_REST);
		stream = Objects.requireNonNull(attributes.attribute(STREAM));
		tablePrefix = Objects.requireNonNull(attributes.attribute(TABLE_PREFIX));
		applyRangeKey = Objects.requireNonNull(attributes.attribute(APPLY_RANGE_KEY));
		rangeKeyValue = Objects.requireNonNull(attributes.attribute(RANGE_KEY_VALUE));
		deletionProtection = Objects.requireNonNull(attributes.attribute(DELETION_PROTECTION));
		continuousBackups = Objects.requireNonNull(attributes.attribute(CONTINUOUS_BACKUPS));
		ttl = Objects.requireNonNull(attributes.attribute(TTL));
		httpProxyHost = attributes.attribute(HTTP_PROXY_HOST);
		httpProxyPort = attributes.attribute(HTTP_PROXY_PORT);
		hmacSHA256Key = attributes.attribute(HMAC_SHA_256_KEY);

		metricRegistry = attributes.attribute(METRIC_REGISTRY);
	}
	
	
	@Override
	public boolean segmented() {
		return false;
	}
	
	
	public String getEndpoint() {
		return endpoint.get();
	}


	public Regions getRegion() {
		return region.get();
	}


	public Class getItemTransformerClass() {
		return itemTransformerClass.get();
	}


	public Class getQueryExecutorClass() {
		return queryExecutorClass.get();
	}


	@SuppressWarnings("unchecked")
	public Set<String> getIndexAttributes() {
		return (Set<String>) indexAttributes.get();
	}


	public boolean useConsistentReads() {
		return consistentRead.get();
	}


	public ProvisionedThroughput getProvisionedThroughputForCreateTable() {
		return new ProvisionedThroughput(readCapacity.get(), writeCapacity.get());
	}


	public Capacity getPurgeMaxReadCapacity() {
		return purgeMaxReadCapacity.get();
	}


	public long getPurgeLimit() {
		return purgeLimit.get();
	}


	public boolean isCreateTableWithEncryptionAtRest() {
		return encryptionAtRest.get();
	}


	public boolean isCreateTableWithStream() {
		return stream.get();
	}


	public String getTablePrefix() {
		return tablePrefix.get();
	}


	public String getRangeKeyToApply() {
		String keyName = applyRangeKey.get();
		if (keyName == null || keyName.trim().isEmpty()) {
			return null;
		}
		return keyName;
	}


	public String getRangeKeyValue() {
		String keyValue = rangeKeyValue.get();
		if (keyValue == null || keyValue.trim().isEmpty()) {
			return null;
		}
		return keyValue;
	}


	public boolean isEnableDeletionProtection() {
		return deletionProtection.get();
	}


	public boolean isEnableContinuousBackups() {
		return continuousBackups.get();
	}


	public boolean isEnableTTL() {
		return ttl.get();
	}


	public String getHTTPProxyHost() {
		return httpProxyHost.get();
	}


	public int getHTTPProxyPort() {
		return httpProxyPort.get();
	}


	public String getHMACSHA256Key() {
		return hmacSHA256Key.get();
	}


	public MetricRegistry getMetricRegistry() {
		return metricRegistry.get();
	}
	
	
	@Override
	public Properties properties() {
		
		// Interpolate with system properties where ${sysPropName} is found
		
		Properties interpolatedProps = new Properties();
		
		for (String name : super.properties().stringPropertyNames()) {
			interpolatedProps.setProperty(name, StringPropertyReplacer.replaceProperties(super.properties().getProperty(name)));
		}
		
		return interpolatedProps;
	}
	
	
	@Override
	public void log() {
		
		Loggers.MAIN_LOG.info("[DS0000] Infinispan DynamoDB store: Endpoint (overrides region): {}", getEndpoint() != null ? getEndpoint() : "not set");
		Loggers.MAIN_LOG.info("[DS0001] Infinispan DynamoDB store: Region: {}", getRegion());
		Loggers.MAIN_LOG.info("[DS0002] Infinispan DynamoDB store: Item transformer class: {}", getItemTransformerClass().getCanonicalName());
		Loggers.MAIN_LOG.info("[DS0009] Infinispan DynamoDB store: Query executor class: {}", getQueryExecutorClass() != null ? getQueryExecutorClass().getCanonicalName() : "none");
		Loggers.MAIN_LOG.info("[DS0010] Infinispan DynamoDB store: Index attributes: {}", getIndexAttributes() != null ? getIndexAttributes() : "none");
		Loggers.MAIN_LOG.info("[DS0015] Infinispan DynamoDB store: Consistent reads: {}", useConsistentReads());
		Loggers.MAIN_LOG.info("[DS0005] Infinispan DynamoDB store: Create table with provisioned capacity: read={} write={}", getProvisionedThroughputForCreateTable().getReadCapacityUnits(), getProvisionedThroughputForCreateTable().getWriteCapacityUnits());
		Loggers.MAIN_LOG.info("[DS0020] Infinispan DynamoDB store: Max read capacity for expired item scans: {}", getPurgeMaxReadCapacity());
		Loggers.MAIN_LOG.info("[DS0016] Infinispan DynamoDB store: Expired entries purge limit: {}", this::getPurgeLimit);
		Loggers.MAIN_LOG.info("[DS0011] Infinispan DynamoDB store: Create table with encryption at rest: {}", isCreateTableWithEncryptionAtRest());
		Loggers.MAIN_LOG.info("[DS0003] Infinispan DynamoDB store: Create table with stream (for global table): {}", isCreateTableWithStream());
		Loggers.MAIN_LOG.info("[DS0006] Infinispan DynamoDB store: Table prefix: {}", getTablePrefix().isEmpty() ? "none" : getTablePrefix());
		Loggers.MAIN_LOG.info("[DS0007] Infinispan DynamoDB store: Apply range key: {}", getRangeKeyToApply() != null ? getRangeKeyToApply() : "none");
		Loggers.MAIN_LOG.info("[DS0008] Infinispan DynamoDB store: Range key value: {}", getRangeKeyValue() != null ? getRangeKeyValue() : "none");
		Loggers.MAIN_LOG.info("[DS0021] Infinispan DynamoDB store: Enable deletion protection: {}", isEnableDeletionProtection());
		Loggers.MAIN_LOG.info("[DS0012] Infinispan DynamoDB store: Enable continuous backups / PITR: {}", isEnableContinuousBackups());
		Loggers.MAIN_LOG.info("[DS0014] Infinispan DynamoDB store: Enable TTL: {}", isEnableTTL());
		Loggers.MAIN_LOG.info("[DS0017] Infinispan DynamoDB store: HTTP proxy host: {}", getHTTPProxyHost() != null ? getHTTPProxyHost() : "none");
		Loggers.MAIN_LOG.info("[DS0018] Infinispan DynamoDB store: HTTP proxy port: {}", getHTTPProxyPort());
		Loggers.MAIN_LOG.info("[DS0019] Infinispan DynamoDB store: HMAC SHA-256 key: {}", getHMACSHA256Key() != null ? "configured" : "none");
		Loggers.MAIN_LOG.info("[DS0013] Infinispan DynamoDB store: Explicit metric registry: {}", getMetricRegistry() != null);
	}
}
