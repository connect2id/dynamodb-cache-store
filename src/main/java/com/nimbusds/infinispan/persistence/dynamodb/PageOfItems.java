package com.nimbusds.infinispan.persistence.dynamodb;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.model.ConsumedCapacity;
import net.jcip.annotations.Immutable;

import java.util.Iterator;


/**
 * Page of DynamoDB items.
 */
@Immutable
record PageOfItems(Iterator<Item> itemIterator,
                   ConsumedCapacity consumedCapacity) {


        /**
         * Creates a new page of DynamoDB items.
         *
         * @param itemIterator     The item iterator.
         * @param consumedCapacity The consumed capacity.
         */
        PageOfItems(final Iterator<Item> itemIterator, final ConsumedCapacity consumedCapacity) {
                assert itemIterator != null;
                this.itemIterator = itemIterator;
                assert consumedCapacity != null;
                this.consumedCapacity = consumedCapacity;
        }
}
