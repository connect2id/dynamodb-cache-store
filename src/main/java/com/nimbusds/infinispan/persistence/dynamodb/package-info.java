/**
 * DynamoDB store for Infinispan.
 */
package com.nimbusds.infinispan.persistence.dynamodb;