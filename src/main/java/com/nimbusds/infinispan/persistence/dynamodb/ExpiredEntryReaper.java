package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.codahale.metrics.Timer;
import com.google.common.util.concurrent.RateLimiter;
import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.dynamodb.config.Capacity;
import com.nimbusds.infinispan.persistence.dynamodb.logging.Loggers;
import net.jcip.annotations.ThreadSafe;
import org.infinispan.metadata.InternalMetadata;
import org.infinispan.metadata.impl.PrivateMetadata;
import org.infinispan.persistence.spi.AdvancedCacheExpirationWriter;
import org.infinispan.persistence.spi.AdvancedCacheWriter;
import org.infinispan.persistence.spi.MarshallableEntry;
import org.infinispan.persistence.spi.MarshallableEntryFactory;

import java.util.Date;
import java.util.Iterator;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;


/**
 * Reaper of expired persisted Infinispan entries.
 */
@ThreadSafe
class ExpiredEntryReaper<K, V> {


	/**
	 * The DynamoDB scan page size to use.
	 */
	static final int PAGE_SIZE = 100;
	
	
	/**
	 * The Infinispan marshallable entry factory.
	 */
	private final MarshallableEntryFactory<K, V> mEntryFactory;
	
	
	/**
	 * The DynamoDB table.
	 */
	private final Table table;


	/**
	 * The DynamoDB request factory.
	 */
	private final RequestFactory<K, V> requestFactory;


	/**
	 * The configured maximum read capacity to use when scanning the
	 * DynamoDB table for expired items.
	 */
	private final Capacity purgeMaxReadCapacity;
	
	
	/**
	 * Limits the number of expired entries to purge, -1 for no limit.
	 */
	private final long purgeLimit;
	
	
	/**
	 * The purge timer to use.
	 */
	private final Timer purgeTimer;


	/**
	 * The delete timer to use.
	 */
	private final Timer deleteTimer;


	/**
	 * Creates a new reaper for expired persisted Infinispan entries.
	 *
	 * @param mEntryFactory        The Infinispan marshallable entry
	 *                             factory. Must not be {@code null}.
	 * @param table                The DynamoDB table. Must not be
	 *                             {@code null}.
	 * @param requestFactory       The DynamoDB request factory. Must not
	 *                             be {@code null}.
	 * @param purgeMaxReadCapacity The configured maximum read capacity to
	 *                             use when scanning the DynamoDB table for
	 *                             expired items, -1.0 to set to 1/10th of
	 *                             the reported provisioned read capacity
	 *                             for the table.
	 * @param purgeLimit           Limits the number of expired entries to
	 *                             purge, -1 for no limit.
	 * @param purgeTimer           The purge timer to use. Must not be
	 *                             {@code null}.
	 */
	ExpiredEntryReaper(final MarshallableEntryFactory<K,V> mEntryFactory,
			   final Table table,
			   final RequestFactory<K, V> requestFactory,
			   final Capacity purgeMaxReadCapacity,
			   final long purgeLimit,
			   final Timer purgeTimer,
			   final Timer deleteTimer) {
		
		this.mEntryFactory = Objects.requireNonNull(mEntryFactory);
		this.table = Objects.requireNonNull(table);
		this.requestFactory = Objects.requireNonNull(requestFactory);
		this.purgeMaxReadCapacity = Objects.requireNonNull(purgeMaxReadCapacity);
		this.purgeLimit = purgeLimit;
		this.purgeTimer = Objects.requireNonNull(purgeTimer);
		this.deleteTimer = Objects.requireNonNull(deleteTimer);
	}


	/**
	 * Resolves the absolute maximum purge read capacity to use.
	 *
	 * @return The absolute maximum purge read capacity.
	 */
	double resolveAbsolutePurgeMaxReadCapacity() {

		if (purgeMaxReadCapacity.measure().equals(Capacity.Measure.ABSOLUTE)) {
			return purgeMaxReadCapacity.value();
		}

		assert Capacity.Measure.PERCENT.equals(purgeMaxReadCapacity.measure());

		long reportedReadCapacity = table.getDescription().getProvisionedThroughput().getReadCapacityUnits();

		assert reportedReadCapacity > 0.0;

		double resolvedCapacity = (reportedReadCapacity / 100.0) * purgeMaxReadCapacity.value();

		Loggers.DYNAMODB_LOG.debug("[DS1000] Resolved purge max read capacity: {}", resolvedCapacity);

		return resolvedCapacity;
	}


	/**
	 * Purges the expired persisted entries according to their metadata
	 * timestamps (if set / persisted).
	 *
	 * @param purgeListener The purge listener. Must not be {@code null}.
	 *
	 * @return The number of purged entries.
	 */
	long purge(final AdvancedCacheWriter.PurgeListener<? super K> purgeListener) {

		return purgePaged(infinispanEntry -> {
                        // Notify listener, interested in the Infinispan entry key
                        purgeListener.entryPurged(infinispanEntry.getKey());
                });
	}


	/**
	 * Purges the expired persisted entries according to their metadata
	 * timestamps (if set / persisted), with an extended listener for the
	 * complete purged entry.
	 *
	 * @param purgeListener The purge listener. Must not be {@code null}.
	 *
	 * @return The number of purged entries.
	 */
	long purgeExtended(final AdvancedCacheExpirationWriter.ExpirationPurgeListener<K,V> purgeListener) {

		return purgePaged(infinispanEntry -> {
                        // Notify listener, interested in the Infinispan entry
                        MarshallableEntry<K, V> mEntry =
                                mEntryFactory.create(
                                        infinispanEntry.getKey(),
                                        infinispanEntry.getValue(),
                                        infinispanEntry.getMetadata(),
                                        PrivateMetadata.empty(),
                                        infinispanEntry.created(),
                                        infinispanEntry.lastUsed()
                                );
                        assert mEntry != null;
                        purgeListener.marshalledEntryPurged(mEntry);
                });
	}


	/**
	 * Performs purge with a rate-limited paged scan.
	 *
	 * @param entryConsumer Consumer for the purged Infinispan entries.
	 *
	 * @return The number of purged entries.
	 */
	private long purgePaged(final Consumer<InfinispanEntry<K, V>> entryConsumer) {

		// Based on https://aws.amazon.com/blogs/developer/rate-limited-scans-in-amazon-dynamodb/

		final var retrievalCounter = new AtomicLong(0);
		final var deleteCounter = new AtomicLong(0);

		try (Timer.Context purgeTimerCtx = purgeTimer.time()) {

			// Init the rate limiter with the specified read capacity units / second
			RateLimiter rateLimiter = RateLimiter.create(resolveAbsolutePurgeMaxReadCapacity());

			// Track the consumed read capacity for each page
			int permitsToConsume = 1;

			Iterator<PageOfItems> pageIterator = requestFactory.getAllItemsPaged(table, PAGE_SIZE);

			while (pageIterator.hasNext()) {

				if (purgeLimit > -1 && deleteCounter.get() >= purgeLimit) {
					// Purged enough entries, stop
					break;
				}

				// Proceeds immediately or sleeps until recharged
				rateLimiter.acquire(permitsToConsume);

				final long now = new Date().getTime();

				PageOfItems page = pageIterator.next();

				Iterator<Item> itemIterator = page.itemIterator();

				while (itemIterator.hasNext()) {

					Item item = itemIterator.next();

					retrievalCounter.incrementAndGet();

					InfinispanEntry<K, V> infinispanEntry;
					try {

						infinispanEntry =requestFactory.getItemTransformer().toInfinispanEntry(item);
					} catch (Exception e) {
						Loggers.DYNAMODB_LOG.error("[DS0152] Purge item transform exception: {}: {}", e.getMessage(), e);
						continue;
					}

					InternalMetadata metadata = infinispanEntry.getMetadata();

					if (metadata == null) {
						continue; // no metadata found
					}

					if (!metadata.isExpired(now)) {
						continue; // not expired
					}

					// Delete batching will not work here, doesn't support
					// attribute retrieval to confirm deletion
					final boolean deleted;
					try (Timer.Context deleteTimerCtx = deleteTimer.time()) {
						deleted = table.deleteItem(requestFactory.resolveDeleteItemSpec(infinispanEntry.getKey())).getItem() != null;
					}

					if (deleted) {
						// Notify listener
						entryConsumer.accept(infinispanEntry);
						deleteCounter.incrementAndGet();
					}
				}

				permitsToConsume = (int) (page.consumedCapacity().getCapacityUnits() - 1.0);

				if (permitsToConsume <= 0) {
					permitsToConsume = 1;
				}
			}

			Loggers.DYNAMODB_LOG.debug("[DS0128] DynamoDB store: Purged {} expired out of {} {} cache entries",
				deleteCounter.get(), retrievalCounter.get(), table.getTableName());

			return deleteCounter.get();

		} catch (Exception e) {
			Loggers.DYNAMODB_LOG.error("[DS0151] Purge exception: {}: {}", e.getMessage(), e);
			return deleteCounter.get();
		}
	}
}
