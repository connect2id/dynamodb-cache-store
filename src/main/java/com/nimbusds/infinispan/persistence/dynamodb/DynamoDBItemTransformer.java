package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.services.dynamodbv2.document.Item;

import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfiguration;


/**
 * Interface for transforming between Infinispan entries (key / value pair and
 * metadata) and a corresponding DynamoDB item. Implementations must be
 * thread-safe.
 *
 * <p>To specify an entry transformer for a given Infinispan cache that is
 * backed by a DynamoDB store, provide its class name to the
 * {@link DynamoDBStoreConfiguration store configuration}.
 */
public interface DynamoDBItemTransformer<K,V> {
	
	
	/**
	 * Initialisation context.
	 */
	interface InitContext {
		
		
		/**
		 * Checks if automatic item expiration is enabled for the
		 * DynamoDB table. The item transformer should then specify
		 * a {@link #getHashKeyAttributeName() TTL attribute name} and
		 * populate it for items that are to be expired automatically
		 * by DynamoDB.
		 *
		 * @return {@code true} if automatic item expiration is
		 *         enabled.
		 */
		boolean isEnableTTL();
	}
	
	
	/**
	 * Provides initialisation.
	 *
	 * @param initContext The initialisation context.
	 */
	default void init(final InitContext initContext) { }
	
	
	/**
	 * Returns the base name of the DynamoDB table. Required to create and
	 * to connect to the DynamoDB table for storing the cache entries. The
	 * final DynamoDB table name is formed by prefixing the optional
	 * configuration {@code table-prefix} to the base name.
	 *
	 * @return The table name.
	 */
	String getTableName();
	
	
	/**
	 * Returns the DynamoDB hash key attribute name. Required to create the
	 * DynamoDB table for storing the cache entries.
	 *
	 * @return The hash key attribute name.
	 */
	String getHashKeyAttributeName();
	
	
	/**
	 * Returns the optional DynamoDB range key attribute name. Required to
	 * create the DynamoDB table for storing the cache entries.
	 *
	 * @return The range key attribute name, {@code null} if none.
	 */
	default String getRangeKeyAttributeName() {
		return null;
	}
	
	
	/**
	 * Returns the optional DynamoDB time-to-live attribute name. Required
	 * to create the DynamoDB table for the storing the cache entries with
	 * DynamoDB managed expiration.
	 *
	 * @return The TTL attribute name, {@code null} if none.
	 */
	default String getTTLAttributeName() {
		return null;
	}
	
	
	/**
	 * Resolves the DynamoDB hash key value (of scalar attribute type
	 * string) for the specified Infinispan entry key.
	 *
	 * <p>Use {@link #resolvePrimaryKey(Object)} instead.
	 *
	 * @param key The Infinispan entry key. Not {@code null}.
	 *
	 * @return The DynamoDB hash key value.
	 */
	@Deprecated
	String resolveHashKey(final K key);
	
	
	/**
	 * Resolves the DynamoDB primary key (hash key value of scalar
	 * attribute type string, with optional additional range key value) for
	 * the specified Infinispan entry key.
	 *
	 * @param key The Infinispan entry key. Not {@code null}.
	 *
	 * @return The DynamoDB primary key value.
	 */
	default PrimaryKeyValue resolvePrimaryKey(final K key) {
		return new PrimaryKeyValue(resolveHashKey(key), null);
	}
	

	/**
	 * Transforms the specified Infinispan entry (key / value pair with
	 * optional metadata) to a DynamoDB item.
	 *
	 * <p>Example:
	 *
	 * <p>Infinispan entry:
	 *
	 * <ul>
	 *     <li>Key: cae7t
	 *     <li>Value: Java POJO with fields {@code uid=cae7t},
	 *         {@code givenName=Alice}, {@code surname=Adams} and
	 *         {@code email=alice@wonderland.net}.
	 *     <li>Metadata: Specifies the entry expiration and other
	 *         properties.
	 * </ul>
	 *
	 * <p>Resulting DynamoDB item:
	 *
	 * <pre>
	 * uid: cae7t (key)
	 * surname: Adams
	 * given_name: Alice
	 * email: alice@wonderland.net
	 * </pre>
	 *
	 * @param infinispanEntry The Infinispan entry. Not {@code null}.
	 *
	 * @return The DynamoDB item.
	 */
	Item toItem(final InfinispanEntry<K,V> infinispanEntry);


	/**
	 * Transforms the specified DynamoDB item to an Infinispan entry (key /
	 * value / metadata triple).
	 *
	 * <p>Example:
	 *
	 * <p>DynamoDB item:
	 *
	 * <pre>
	 * uid: cae7t
	 * surname: Adams
	 * given_name: Alice
	 * email: alice@wonderland.net
	 * </pre>
	 *
	 * <p>Resulting Infinispan entry:
	 *
	 * <ul>
	 *     <li>Key: cae7t
	 *     <li>Value: Java POJO with fields {@code uid=cae7t},
	 *         {@code givenName=Alice}, {@code surname=Adams} and
	 *         {@code email=alice@wonderland.net}.
	 *     <li>Metadata: Default metadata (no expiration, etc).
	 * </ul>
	 *
	 * @param item The DynamoDB item. Must not be {@code null}.
	 *
	 * @return The Infinispan entry (key / value pair with optional
	 *         metadata).
	 */
	InfinispanEntry<K,V> toInfinispanEntry(final Item item);
}
