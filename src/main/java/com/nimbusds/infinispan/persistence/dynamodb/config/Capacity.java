package com.nimbusds.infinispan.persistence.dynamodb.config;


/**
 * Capacity specified as absolute value or percentage.
 */
public record Capacity(double value,
                       com.nimbusds.infinispan.persistence.dynamodb.config.Capacity.Measure measure) {

        /**
         * The measure.
         */
        public enum Measure {

                ABSOLUTE,

                PERCENT
        }

        /**
         * Creates a new capacity.
         *
         * @param value   The value. Must be positive.
         * @param measure The measure. Must not be {@code null}.
         */
        public Capacity(final double value, final Measure measure) {
                assert value > 0.0;
                this.value = value;
                assert measure != null;
                this.measure = measure;
        }


        @Override
        public String toString() {
                if (Measure.ABSOLUTE.equals(measure)) {
                        return value + "";
                } else {
                        return value + "%";
                }
        }


        /**
         * Parses a capacity from the specified string.
         *
         * @param s The string. Must not be {@code null}.
         * @return The parsed capacity.
         * @throws NumberFormatException If parsing failed.
         */
        public static Capacity parse(final String s)
                throws NumberFormatException {

                String doubleString;
                Measure measure;

                if (s.trim().endsWith("%")) {
                        doubleString = s.trim().replace("%", "");
                        measure = Measure.PERCENT;
                } else {
                        doubleString = s.trim();
                        measure = Measure.ABSOLUTE;
                }

                double value;
                try {
                        value = Double.parseDouble(doubleString);
                } catch (NumberFormatException e) {
                        throw new NumberFormatException("Illegal capacity: " + e.getMessage());
                }

                return new Capacity(value, measure);
        }
}
