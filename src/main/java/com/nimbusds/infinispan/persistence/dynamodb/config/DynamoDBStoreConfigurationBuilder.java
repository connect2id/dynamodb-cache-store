package com.nimbusds.infinispan.persistence.dynamodb.config;


import com.amazonaws.regions.Regions;
import com.codahale.metrics.MetricRegistry;
import org.infinispan.commons.CacheConfigurationException;
import org.infinispan.configuration.cache.AbstractStoreConfigurationBuilder;
import org.infinispan.configuration.cache.PersistenceConfigurationBuilder;

import java.util.Base64;
import java.util.Properties;
import java.util.Set;


/**
 * DynamoDB store configuration builder.
 *
 * <p>Used by the Infinispan ConfigurationBuilder to implement fluent
 * configuration for the DynamoDB CacheLoader / CacheWriter. Methods should use
 * the fluent style, rather than the setter/getter style and should return an
 * instance of this object.
 */
public class DynamoDBStoreConfigurationBuilder
	extends AbstractStoreConfigurationBuilder<DynamoDBStoreConfiguration,DynamoDBStoreConfigurationBuilder>
	implements DynamoDBStoreConfigurationChildBuilder<DynamoDBStoreConfigurationBuilder> {
	
	
	/**
	 * Creates a new DynamoDB store configuration builder.
	 *
	 * @param builder The general persistence configuration builder.
	 */
	public DynamoDBStoreConfigurationBuilder(final PersistenceConfigurationBuilder builder) {
		super(builder, DynamoDBStoreConfiguration.attributeDefinitionSet());
	}


	@Override
	public DynamoDBStoreConfiguration create() {
        	// This method should construct a new instance of a
		// DynamoDBStoreConfiguration object. There will be one
		// instance per cache.
		return new DynamoDBStoreConfiguration(
			this.attributes.protect(),
			this.async.create());
	}
	
	
	@Override
	public DynamoDBStoreConfigurationBuilder endpoint(final String endpoint) {
		
		this.attributes.attribute(DynamoDBStoreConfiguration.ENDPOINT).set(endpoint);
		return this;
	}
	
	
	@Override
	public DynamoDBStoreConfigurationBuilder region(final Regions region) {
		
		this.attributes.attribute(DynamoDBStoreConfiguration.REGION).set(region);
		return this;
	}
	
	
	@Override
	public DynamoDBStoreConfigurationBuilder itemTransformerClass(final Class itemTransformerClass) {
		
		this.attributes.attribute(DynamoDBStoreConfiguration.ITEM_TRANSFORMER).set(itemTransformerClass);
		return this;
	}
	
	
	@Override
	public DynamoDBStoreConfigurationBuilder queryExecutorClass(final Class queryExecutorClass) {
		
		this.attributes.attribute(DynamoDBStoreConfiguration.QUERY_EXECUTOR).set(queryExecutorClass);
		return this;
	}
	
	
	@Override
	public DynamoDBStoreConfigurationBuilder indexAttributes(final Set<String> indexAttributes) {
		
		this.attributes.attribute(DynamoDBStoreConfiguration.INDEX_ATTRIBUTES).set(indexAttributes);
		return this;
	}
	
	
	@Override
	public DynamoDBStoreConfigurationBuilder consistentReads(final boolean enable) {
		
		this.attributes.attribute(DynamoDBStoreConfiguration.CONSISTENT_READS).set(enable);
		return this;
	}
	
	
	@Override
	public DynamoDBStoreConfigurationBuilder readCapacity(long readCapacity) {
		
		this.attributes.attribute(DynamoDBStoreConfiguration.READ_CAPACITY).set(readCapacity);
		return this;
	}
	
	
	@Override
	public DynamoDBStoreConfigurationBuilder writeCapacity(long writeCapacity) {
		
		this.attributes.attribute(DynamoDBStoreConfiguration.WRITE_CAPACITY).set(writeCapacity);
		return this;
	}


	@Override
	public DynamoDBStoreConfigurationBuilder purgeMaxReadCapacity(final Capacity purgeReadCapacity) {

		this.attributes.attribute(DynamoDBStoreConfiguration.PURGE_MAX_READ_CAPACITY).set(purgeReadCapacity);
		return this;
	}
	
	
	@Override
	public DynamoDBStoreConfigurationBuilder encryptionAtRest(boolean enable) {
		
		this.attributes.attribute(DynamoDBStoreConfiguration.ENCRYPTION_AT_REST).set(enable);
		return this;
	}
	
	
	@Override
	public DynamoDBStoreConfigurationBuilder tablePrefix(final String tablePrefix) {
		
		this.attributes.attribute(DynamoDBStoreConfiguration.TABLE_PREFIX).set(tablePrefix);
		return this;
	}

	@Override
	public DynamoDBStoreConfigurationBuilder metricRegistry(final MetricRegistry metricRegistry) {
		
		this.attributes.attribute(DynamoDBStoreConfiguration.METRIC_REGISTRY).set(metricRegistry);
		return this;
	}


	@Override
	public DynamoDBStoreConfigurationBuilder applyRangeKey(final String rangeKeyName) {
		
		this.attributes.attribute(DynamoDBStoreConfiguration.APPLY_RANGE_KEY).set(rangeKeyName);
		return this;
	}
	
	
	@Override
	public DynamoDBStoreConfigurationBuilder rangeKeyValue(final String rangeKeyValue) {
		
		this.attributes.attribute(DynamoDBStoreConfiguration.RANGE_KEY_VALUE).set(rangeKeyValue);
		return this;
	}
	
	
	@Override
	public DynamoDBStoreConfigurationBuilder stream(final boolean enable) {
		
		this.attributes.attribute(DynamoDBStoreConfiguration.STREAM).set(enable);
		return this;
	}
	
	
	@Override
	public DynamoDBStoreConfigurationBuilder continuousBackups(final boolean enable) {
		
		this.attributes.attribute(DynamoDBStoreConfiguration.CONTINUOUS_BACKUPS).set(enable);
		return this;
	}


	@Override
	public DynamoDBStoreConfigurationBuilder deletionProtection(final boolean enable) {

		this.attributes.attribute(DynamoDBStoreConfiguration.DELETION_PROTECTION).set(enable);
		return this;
	}


	@Override
	public DynamoDBStoreConfigurationBuilder ttl(final boolean enable) {
		
		this.attributes.attribute(DynamoDBStoreConfiguration.TTL).set(enable);
		return this;
	}
	
	
	@Override
	public DynamoDBStoreConfigurationBuilder purgeLimit(final long purgeLimit) {
		
		this.attributes.attribute(DynamoDBStoreConfiguration.PURGE_LIMIT).set(purgeLimit);
		return this;
	}
	
	
	@Override
	public DynamoDBStoreConfigurationBuilder httpProxyHost(final String host) {
		
		this.attributes.attribute(DynamoDBStoreConfiguration.HTTP_PROXY_HOST).set(host);
		return this;
	}
	
	
	@Override
	public DynamoDBStoreConfigurationBuilder httpProxyPort(final int port) {
		
		this.attributes.attribute(DynamoDBStoreConfiguration.HTTP_PROXY_PORT).set(port);
		return this;
	}
	
	
	@Override
	public DynamoDBStoreConfigurationBuilder hmacSHA256Key(final String key) {
		
		this.attributes.attribute(DynamoDBStoreConfiguration.HMAC_SHA_256_KEY).set(key);
		return this;
	}
	
	
	@Override
	public DynamoDBStoreConfigurationBuilder withProperties(final Properties properties) {
		
		return properties(properties);
	}
	
	
	@Override
	public void validate() {
		
		super.validate();

		if (this.attributes.attribute(DynamoDBStoreConfiguration.ITEM_TRANSFORMER).get() == null) {
			throw new CacheConfigurationException("A DynamoDB store item transformer class must be specified");
		}
		
		if (this.attributes.attribute(DynamoDBStoreConfiguration.QUERY_EXECUTOR) != null
			&& this.attributes.attribute(DynamoDBStoreConfiguration.INDEX_ATTRIBUTES) == null) {
			
			throw new CacheConfigurationException("The indexed attributes must be specified if a query executor is set");
		}
		
		if (this.attributes.attribute(DynamoDBStoreConfiguration.QUERY_EXECUTOR) == null
			&& this.attributes.attribute(DynamoDBStoreConfiguration.INDEX_ATTRIBUTES) != null) {
		
			throw new CacheConfigurationException("The query executor must be set if indexed attributes are specified");
		}
		
		String rangeKeyName = this.attributes.attribute(DynamoDBStoreConfiguration.APPLY_RANGE_KEY).get();
		String rangeKeyValue = this.attributes.attribute(DynamoDBStoreConfiguration.RANGE_KEY_VALUE).get();
		
		if (rangeKeyName != null && ! rangeKeyName.trim().isEmpty()) {
			
			if (rangeKeyValue == null || rangeKeyValue.trim().isEmpty()) {
				throw new CacheConfigurationException("A range key value must be specified");
			}
		}
		
		if (this.attributes.attribute(DynamoDBStoreConfiguration.HMAC_SHA_256_KEY).get() != null) {
			
			byte[] keyBytes = Base64.getDecoder().decode(this.attributes.attribute(DynamoDBStoreConfiguration.HMAC_SHA_256_KEY).get());
			
			if (keyBytes.length < (256 / 8)) {
				throw new CacheConfigurationException("The HMAC SHA-256 key must be at least 256 bits long");
			}
		}
	}


	@Override
	public DynamoDBStoreConfigurationBuilder self() {
		return this;
	}
}
