package com.nimbusds.infinispan.persistence.dynamodb.config;


import com.amazonaws.regions.Regions;
import org.infinispan.commons.CacheConfigurationException;
import org.infinispan.commons.configuration.io.ConfigurationReader;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.cache.PersistenceConfigurationBuilder;
import org.infinispan.configuration.parsing.*;
import org.kohsuke.MetaInfServices;

import java.util.Set;

import static org.infinispan.commons.util.StringPropertyReplacer.replaceProperties;


/**
 * XML configuration parser.
 */
@MetaInfServices
@Namespaces({
	@Namespace(uri = "urn:infinispan:config:store:dynamodb:3.0", root = "dynamodb-store"),
	@Namespace(root = "dynamodb-store")
})
public class DynamoDBStoreConfigurationParser_3_0 implements ConfigurationParser {
	
	
	@Override
	public void readElement(final ConfigurationReader reader, final ConfigurationBuilderHolder holder) {
		ConfigurationBuilder builder = holder.getCurrentConfigurationBuilder();
		
		Element element = Element.forName(reader.getLocalName());
		switch (element) {
			case DYNAMODB_STORE: {
				this.parseDynamoDBStore(reader, builder.persistence(), holder.getClassLoader());
				break;
			}
			default: {
				throw ParseUtils.unexpectedElement(reader);
			}
		}
	}
	
	
	/**
	 * Parses a DynamoDB store XML element.
	 */
	private void parseDynamoDBStore(final ConfigurationReader reader,
					final PersistenceConfigurationBuilder persistenceBuilder,
					final ClassLoader classLoader) {
		
		var builder = new DynamoDBStoreConfigurationBuilder(persistenceBuilder);
		
		this.parseDynamoDBStoreAttributes(reader, builder, classLoader);
		
		while (reader.hasNext() && (reader.nextElement() != ConfigurationReader.ElementType.END_ELEMENT)) {
			Element element = Element.forName(reader.getLocalName());
			switch (element) {
				default: {
					Parser.parseStoreElement(reader, builder);
					break;
				}
			}
		}
		
		persistenceBuilder.addStore(builder);
	}
	
	
	/**
	 * Parses the attributes of an XML store element.
	 */
	private void parseDynamoDBStoreAttributes(final ConfigurationReader reader,
						  final DynamoDBStoreConfigurationBuilder builder,
						  final ClassLoader classLoader) {
		
		for (int i = 0; i < reader.getAttributeCount(); i++) {
			ParseUtils.requireNoNamespaceAttribute(reader, i);
			String value = replaceProperties(reader.getAttributeValue(i));
			Attribute attribute = Attribute.forName(reader.getAttributeName(i));
			switch (attribute) {
				
				case ENDPOINT:
					builder.endpoint(value);
					break;
					
				case REGION:
					if (value != null && ! value.trim().isEmpty()) {
						try {
							builder.region(Regions.fromName(value));
						} catch (Exception e) {
							throw new CacheConfigurationException(e);
						}
					}
					break;
				
				case ITEM_TRANSFORMER:
					try {
						builder.itemTransformerClass(classLoader.loadClass(value));
					} catch (ClassNotFoundException e) {
						throw new CacheConfigurationException(e);
					}
					break;
					
				case QUERY_EXECUTOR:
					try {
						builder.queryExecutorClass(classLoader.loadClass(value));
					} catch (ClassNotFoundException e) {
						throw new CacheConfigurationException(e);
					}
					
				case INDEX_ATTRIBUTES:
					builder.indexAttributes(parseStringSet(value));
					break;
					
				case CONSISTENT_READS:
					builder.consistentReads(Boolean.parseBoolean(value));
					break;
					
				case READ_CAPACITY:
					try {
						builder.readCapacity(Long.parseLong(value));
					} catch (NumberFormatException e) {
						throw new CacheConfigurationException(e);
					}
					break;
					
				case WRITE_CAPACITY:
					try {
						builder.writeCapacity(Long.parseLong(value));
					} catch (NumberFormatException e) {
						throw new CacheConfigurationException(e);
					}
					break;

				case PURGE_READ_CAPACITY:
					try {
						builder.purgeMaxReadCapacity(Capacity.parse(value));
					} catch (NumberFormatException e) {
						throw new CacheConfigurationException(e);
					}
					break;

				case PURGE_LIMIT:
					try {
						builder.purgeLimit(Integer.parseInt(value));
					} catch (NumberFormatException e) {
						throw new CacheConfigurationException(e);
					}
					break;
				
				case ENCRYPTION_AT_REST:
					builder.encryptionAtRest(Boolean.parseBoolean(value));
					break;

				case STREAM:
					builder.stream(Boolean.parseBoolean(value));
					break;
				
				case TABLE_PREFIX:
					builder.tablePrefix(value);
					break;
					
				case APPLY_RANGE_KEY:
					builder.applyRangeKey(value);
					break;
					
				case RANGE_KEY_VALUE:
					builder.rangeKeyValue(value);
					break;

				case DELETION_PROTECTION:
					builder.deletionProtection(Boolean.parseBoolean(value));
					break;

				case CONTINUOUS_BACKUPS:
					builder.continuousBackups(Boolean.parseBoolean(value));
					break;
					
				case TTL:
					builder.ttl(Boolean.parseBoolean(value));
					break;
					
				case HTTP_PROXY_HOST:
					if (value != null && ! value.trim().isEmpty()) {
						builder.httpProxyHost(value);
					}
					break;
					
				case HTTP_PROXY_PORT:
					try {
						builder.httpProxyPort(Integer.parseInt(value));
					} catch (NumberFormatException e) {
						throw new CacheConfigurationException(e);
					}
					break;
					
				case HMAC_SHA256_KEY:
					if (value != null && ! value.trim().isEmpty()) {
						builder.hmacSHA256Key(value);
					}
					break;
				
				default: {
					Parser.parseStoreAttribute(reader, i, builder);
					break;
				}
			}
		}
	}
	
	
	@Override
	public Namespace[] getNamespaces() {
		return ParseUtils.getNamespaceAnnotations(getClass());
	}
	
	
	/**
	 * Splits a string of comma and / or white space separated strings
	 * into a string set.
	 *
	 * @param s The string to split, {@code null} if not specified.
	 *
	 * @return The string set, {@code null} if not specified.
	 */
	static Set<String> parseStringSet(final String s) {
		
		if (null == s || s.trim().isEmpty()) {
			return null;
		}
		
		return Set.of(s.replaceAll("^[,\\s]+", "").split("[,\\s]+"));
	}
}
