package com.nimbusds.infinispan.persistence.dynamodb;


/**
 * Invalid HMAC exception.
 */
class InvalidHMACException extends Exception {
	
	
	/**
	 * Creates a new invalid HMAC exception.
	 */
	public InvalidHMACException(final String message) {
		super(message);
	}
}
