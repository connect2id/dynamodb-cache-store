package com.nimbusds.infinispan.persistence.dynamodb.config;


import java.util.HashMap;
import java.util.Map;


/**
 * DynamoDB store XML configuration attributes.
 */
public enum Attribute {
	
	
	UNKNOWN(null), // must be first
	ENDPOINT("endpoint"),
	REGION("region"),
	ITEM_TRANSFORMER("item-transformer"),
	QUERY_EXECUTOR("query-executor"),
	INDEX_ATTRIBUTES("index-attributes"),
	CONSISTENT_READS("consistent-reads"),
	READ_CAPACITY("read-capacity"),
	WRITE_CAPACITY("write-capacity"),
	PURGE_READ_CAPACITY("purge-max-read-capacity"),
	PURGE_LIMIT("purge-limit"),
	ENCRYPTION_AT_REST("encryption-at-rest"),
	STREAM("stream"),
	TABLE_PREFIX("table-prefix"),
	APPLY_RANGE_KEY("apply-range-key"),
	RANGE_KEY_VALUE("range-key-value"),
	DELETION_PROTECTION("deletion-protection"),
	CONTINUOUS_BACKUPS("continuous-backups"),
	TTL("ttl"),
	HTTP_PROXY_HOST("http-proxy-host"),
	HTTP_PROXY_PORT("http-proxy-port"),
	HMAC_SHA256_KEY("hmac-sha256-key");


	/**
	 * The attribute name.
	 */
	private final String name;
	
	
	/**
	 * Creates a new attribute with the specified name.
	 *
	 * @param name The attribute name.
	 */
	Attribute(final String name) {
		this.name = name;
	}
	
	
	/**
	 * Gets the local name of this attribute.
	 *
	 * @return The local name.
	 */
	public String getLocalName() {
		return name;
	}
	
	
	/**
	 * The enumerated attributes as map.
	 */
	private static final Map<String, Attribute> attributes;
	
	
	static {
		final Map<String, Attribute> map = new HashMap<>();
		for (Attribute attribute : values()) {
			final String name = attribute.getLocalName();
			if (name != null) {
				map.put(name, attribute);
			}
		}
		attributes = map;
	}
	
	
	/**
	 * Returns the matching attribute for the specified local name.
	 *
	 * @param localName The local name.
	 *
	 * @return The attribute.
	 */
	public static Attribute forName(final String localName) {
		final Attribute attribute = attributes.get(localName);
		return attribute == null ? UNKNOWN : attribute;
	}
}
