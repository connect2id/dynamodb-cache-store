package com.nimbusds.infinispan.persistence.dynamodb;


/**
 * DynamoDB primary key value (hash key, with optional range key).
 *
 * @param hashKeyValue  The hash key value.
 * @param rangeKeyValue The optional range key value.
 */
public record PrimaryKeyValue(String hashKeyValue, String rangeKeyValue) {


	/**
	 * Creates a new DynamoDB primary key value.
	 *
	 * @param hashKeyValue  The hash key value. Must not be {@code null}.
	 * @param rangeKeyValue The optional range key value, {@code null} if
	 *                      none.
	 */
	public PrimaryKeyValue {
		if (hashKeyValue == null) {
			throw new IllegalArgumentException("The hash key value must not be null");
		}
	}
}
