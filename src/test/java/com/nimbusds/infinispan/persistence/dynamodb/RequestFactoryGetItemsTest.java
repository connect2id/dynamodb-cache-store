package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.common.InternalMetadataBuilder;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;


public class RequestFactoryGetItemsTest extends TestWithDynamoDB {
	
	
	static class ItemTransformer implements DynamoDBItemTransformer<String,String> {
		@Override
		public String getTableName() {
			return "test_users";
		}
		
		
		@Override
		public String getHashKeyAttributeName() {
			return "uid";
		}
		
		
		@Override
		public String resolveHashKey(String key) {
			return key;
		}
		
		
		@Override
		public Item toItem(InfinispanEntry<String, String> infinispanEntry) {
			return new Item()
				.withPrimaryKey("uid", infinispanEntry.getKey())
				.with("email", infinispanEntry.getValue());
		}
		
		
		@Override
		public InfinispanEntry<String, String> toInfinispanEntry(Item item) {
			String uid = item.getString("uid");
			String email = item.getString("email");
			
			return new InfinispanEntry<>(
				uid,
				email,
				null);
		}
	}
	
	
	static class ItemTransformerWithRangeKey extends ItemTransformer {


		@Override
		public String getTableName() {
			return "test_users_multi_tenant";
		}

		@Override
		public String getRangeKeyAttributeName() {
			return "tid";
		}
		
		
		@Override
		public PrimaryKeyValue resolvePrimaryKey(String key) {
			String[] parts = key.split(":"); // tid:uid
			return new PrimaryKeyValue(parts[1], parts[0]);
		}
		
		
		@Override
		public Item toItem(InfinispanEntry<String, String> infinispanEntry) {
			
			Item item = super.toItem(infinispanEntry);
			
			PrimaryKeyValue pkValue = resolvePrimaryKey(infinispanEntry.getKey());
			
			return item
				.withPrimaryKey(
					new PrimaryKey("uid", pkValue.hashKeyValue(), "tid", pkValue.rangeKeyValue())
				);
		}
		
		
		@Override
		public InfinispanEntry<String, String> toInfinispanEntry(Item item) {
			
			String tid = item.getString("tid");
			String uid = item.getString("uid");
			String email = item.getString("email");
			
			return new InfinispanEntry<>(
				tid + ":" + uid,
				email,
				null);
		}
	}
	
	
	public void testGetAllItems() {
		
		var tr = new ItemTransformer();
		
		var rf = new RequestFactory<>(
			tr,
			null,
			false,
			new ProvisionedThroughput(1L, 1L),
			false,
                        false, "",
			null,
			null,
			false);
		
		CreateTableRequest createTableRequest = rf.resolveCreateTableRequest();
		dynamoDB.createTable(createTableRequest);

		Table table = dynamoDB.getTable(createTableRequest.getTableName());

		Map<String, String> entriesToAdd = new HashMap<>();

		for (int i=0; i<1050; i++) {
			var key = "u-" + i;
			var email = "u-" + i + "@example.com";
			entriesToAdd.put(key, email);
		}

		entriesToAdd.forEach((key, email) -> {
                        var entry = new InfinispanEntry<>(key, email, new InternalMetadataBuilder().build());
                        Item item = rf.resolveItem(entry);
                        table.putItem(item);
                });

		ItemCollection<?> allItems = rf.getAllItems(table);

		for (Item item: allItems) {
			var entry = rf.getItemTransformer().toInfinispanEntry(item);
			assertEquals(entry.getValue(), entriesToAdd.get(entry.getKey()));
		}
	}


	@Test
	public void testGetAllItemsPaged() {

		for (ItemTransformer tr: List.of(new ItemTransformer(), new ItemTransformerWithRangeKey())) {

			var rf = new RequestFactory<>(
				tr,
				null,
				false,
				new ProvisionedThroughput(1L, 1L),
				false,
                                false, "",
				null,
				null,
				false);

			CreateTableRequest createTableRequest = rf.resolveCreateTableRequest();
			dynamoDB.createTable(createTableRequest);

			Table table = dynamoDB.getTable(createTableRequest.getTableName());

			Map<String, String> entriesToAdd = new HashMap<>();

			final int numTotalEntries = 1050;

			for (int i = 0; i < numTotalEntries; i++) {
				var key = tr instanceof ItemTransformerWithRangeKey ? "t1:u-" + i : "u-" + i;
				var email = "u-" + i + "@example.com";
				entriesToAdd.put(key, email);
			}

			entriesToAdd.forEach((key, email) -> {
				var entry = new InfinispanEntry<>(key, email, new InternalMetadataBuilder().build());
				Item item = rf.resolveItem(entry);
				table.putItem(item);
			});

			List<Item> collectedItems = new LinkedList<>();

			Iterator<PageOfItems> pageIterator = rf.getAllItemsPaged(table, 50);

			int numPages = 0;

			while (pageIterator.hasNext()) {

				numPages++;

				PageOfItems page = pageIterator.next();

				Iterator<Item> itemIterator = page.itemIterator();

				int numItemsPerPage = 0;

				while (itemIterator.hasNext()) {
					numItemsPerPage++;
					Item item = itemIterator.next();
					collectedItems.add(item);
				}

				if (numItemsPerPage > 0) {
					assertTrue(page.consumedCapacity().getCapacityUnits() > 0.0);
					assertNull(page.consumedCapacity().getReadCapacityUnits());
					assertNull(page.consumedCapacity().getWriteCapacityUnits());
				} else {
                                        assertEquals(0.0, page.consumedCapacity().getCapacityUnits().doubleValue(), 0.0);
				}

				assertTrue(50 >= numItemsPerPage);
			}

			assertEquals(numTotalEntries / 50 + 1, numPages);

			assertEquals(numTotalEntries, collectedItems.size());

			for (Item item : collectedItems) {
				var entry = rf.getItemTransformer().toInfinispanEntry(item);
				assertEquals(entry.getValue(), entriesToAdd.get(entry.getKey()));
			}
		}
	}


	@Test
	public void testGetAllItemsPaged_withSinglePage() {

		for (ItemTransformer tr: List.of(new ItemTransformer(), new ItemTransformerWithRangeKey())) {

			var rf = new RequestFactory<>(
				tr,
				null,
				false,
				new ProvisionedThroughput(1L, 1L),
				false,
                                false, "",
				null,
				null,
				false);

			CreateTableRequest createTableRequest = rf.resolveCreateTableRequest();
			dynamoDB.createTable(createTableRequest);

			Table table = dynamoDB.getTable(createTableRequest.getTableName());

			Map<String, String> entriesToAdd = new HashMap<>();

			final int numTotalEntries = 49;

			for (int i = 0; i < numTotalEntries; i++) {
				var key = tr instanceof ItemTransformerWithRangeKey ? "t1:u-" + i : "u-" + i;
				var email = "u-" + i + "@example.com";
				entriesToAdd.put(key, email);
			}

			entriesToAdd.forEach((key, email) -> {
				var entry = new InfinispanEntry<>(key, email, new InternalMetadataBuilder().build());
				Item item = rf.resolveItem(entry);
				table.putItem(item);
			});

			List<Item> collectedItems = new LinkedList<>();

			Iterator<PageOfItems> pageIterator = rf.getAllItemsPaged(table, 50);

			assertTrue(pageIterator.hasNext());

			PageOfItems page = pageIterator.next();

			Iterator<Item> itemIterator = page.itemIterator();

			int numItemsPerPage = 0;

			while (itemIterator.hasNext()) {
				numItemsPerPage++;
				Item item = itemIterator.next();
				collectedItems.add(item);
			}

			assertEquals(numTotalEntries, numItemsPerPage);

			assertFalse(pageIterator.hasNext());

			assertEquals(numTotalEntries, collectedItems.size());

			for (Item item : collectedItems) {
				var entry = rf.getItemTransformer().toInfinispanEntry(item);
				assertEquals(entry.getValue(), entriesToAdd.get(entry.getKey()));
			}
		}
	}


	@Test
	public void testGetAllItemsPaged_withTwoPages_lastPagePartiallyFull() {

		for (ItemTransformer tr: List.of(new ItemTransformer(), new ItemTransformerWithRangeKey())) {

			var rf = new RequestFactory<>(
				tr,
				null,
				false,
				new ProvisionedThroughput(1L, 1L),
				false,
                                false, "",
				null,
				null,
				false);

			CreateTableRequest createTableRequest = rf.resolveCreateTableRequest();
			dynamoDB.createTable(createTableRequest);

			Table table = dynamoDB.getTable(createTableRequest.getTableName());

			Map<String, String> entriesToAdd = new HashMap<>();

			final int numTotalEntries = 55;

			for (int i = 0; i < numTotalEntries; i++) {
				var key = tr instanceof ItemTransformerWithRangeKey ? "t1:u-" + i : "u-" + i;
				var email = "u-" + i + "@example.com";
				entriesToAdd.put(key, email);
			}

			entriesToAdd.forEach((key, email) -> {
				var entry = new InfinispanEntry<>(key, email, new InternalMetadataBuilder().build());
				Item item = rf.resolveItem(entry);
				table.putItem(item);
			});

			List<Item> collectedItems = new LinkedList<>();

			Iterator<PageOfItems> pageIterator = rf.getAllItemsPaged(table, 50);

			assertTrue(pageIterator.hasNext());

			PageOfItems page = pageIterator.next();

			Iterator<Item> itemIterator = page.itemIterator();

			int numItemsPerPage = 0;

			while (itemIterator.hasNext()) {
				numItemsPerPage++;
				Item item = itemIterator.next();
				collectedItems.add(item);
			}

			assertEquals(50, numItemsPerPage);

			numItemsPerPage = 0;

			assertTrue(pageIterator.hasNext());
			page = pageIterator.next();
			itemIterator = page.itemIterator();

			while (itemIterator.hasNext()) {
				numItemsPerPage++;
				Item item = itemIterator.next();
				collectedItems.add(item);
			}

			assertEquals(5, numItemsPerPage);

			assertFalse(pageIterator.hasNext());

			assertEquals(numTotalEntries, collectedItems.size());

			for (Item item : collectedItems) {
				var entry = rf.getItemTransformer().toInfinispanEntry(item);
				assertEquals(entry.getValue(), entriesToAdd.get(entry.getKey()));
			}
		}
	}
}
