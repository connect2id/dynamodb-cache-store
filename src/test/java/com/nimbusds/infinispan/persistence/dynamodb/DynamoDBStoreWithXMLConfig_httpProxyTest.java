package com.nimbusds.infinispan.persistence.dynamodb;


import org.infinispan.Cache;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.junit.After;
import org.junit.Test;
import org.mockserver.integration.ClientAndServer;

import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfiguration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;


public class DynamoDBStoreWithXMLConfig_httpProxyTest extends TestWithDynamoDB {
	
	
	public static final String CACHE_NAME = "users";
	
	
	protected EmbeddedCacheManager cacheMgr;
	
	
	private ClientAndServer mockServer;
	
	
	@Override
	public void setUp()
		throws Exception {
		
		super.setUp();

		mockServer = startClientAndServer("localhost", 8000, 8001);

		cacheMgr = new DefaultCacheManager("test-infinispan-http-proxy.xml");

		cacheMgr.start();
	}
	
	
	@After
	@Override
	public void tearDown()
		throws Exception {
		
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		mockServer.stop();
		
		super.tearDown();
	}
	

	@Test
	public void testWithEnabledProxy() {
		
		Cache<String,User> cache = cacheMgr.getCache(CACHE_NAME);
		
		var store = (DynamoDBStore) DynamoDBStore.getInstances().get(CACHE_NAME);
		assertNotNull(store);
		
		assertEquals("localhost", ((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration(CACHE_NAME).persistence().stores().get(0)).getHTTPProxyHost());
		assertEquals(8001, ((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration(CACHE_NAME).persistence().stores().get(0)).getHTTPProxyPort());
		
		var u1 = new User("Alice Adams", "alice@wonderland.net");
		
		cache.put("u1", u1);
		
		assertEquals(u1.getEmail(), cache.get("u1").getEmail());
	}
}