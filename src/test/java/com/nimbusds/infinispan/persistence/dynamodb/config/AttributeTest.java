package com.nimbusds.infinispan.persistence.dynamodb.config;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;


public class AttributeTest {
	

	@Test
	public void testLocalNames() {
		
		assertNull(Attribute.UNKNOWN.getLocalName());
		assertEquals("endpoint", Attribute.ENDPOINT.getLocalName());
		assertEquals("region", Attribute.REGION.getLocalName());
		assertEquals("item-transformer", Attribute.ITEM_TRANSFORMER.getLocalName());
		assertEquals("query-executor", Attribute.QUERY_EXECUTOR.getLocalName());
		assertEquals("index-attributes", Attribute.INDEX_ATTRIBUTES.getLocalName());
		assertEquals("consistent-reads", Attribute.CONSISTENT_READS.getLocalName());
		assertEquals("read-capacity", Attribute.READ_CAPACITY.getLocalName());
		assertEquals("write-capacity", Attribute.WRITE_CAPACITY.getLocalName());
		assertEquals("purge-max-read-capacity", Attribute.PURGE_READ_CAPACITY.getLocalName());
		assertEquals("encryption-at-rest", Attribute.ENCRYPTION_AT_REST.getLocalName());
		assertEquals("stream", Attribute.STREAM.getLocalName());
		assertEquals("deletion-protection", Attribute.DELETION_PROTECTION.getLocalName());
		assertEquals("table-prefix", Attribute.TABLE_PREFIX.getLocalName());
		assertEquals("apply-range-key", Attribute.APPLY_RANGE_KEY.getLocalName());
		assertEquals("range-key-value", Attribute.RANGE_KEY_VALUE.getLocalName());
		assertEquals("continuous-backups", Attribute.CONTINUOUS_BACKUPS.getLocalName());
		assertEquals("ttl", Attribute.TTL.getLocalName());
		assertEquals("purge-limit", Attribute.PURGE_LIMIT.getLocalName());
		assertEquals("http-proxy-host", Attribute.HTTP_PROXY_HOST.getLocalName());
		assertEquals("http-proxy-port", Attribute.HTTP_PROXY_PORT.getLocalName());
		assertEquals("hmac-sha256-key", Attribute.HMAC_SHA256_KEY.getLocalName());
		
		assertEquals(22, Attribute.values().length);
	}
}
