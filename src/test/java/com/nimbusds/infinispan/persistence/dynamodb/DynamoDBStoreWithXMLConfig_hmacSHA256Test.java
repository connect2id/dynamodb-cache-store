package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfiguration;
import org.infinispan.Cache;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.persistence.spi.PersistenceException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockserver.integration.ClientAndServer;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import static org.junit.Assert.*;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;


public class DynamoDBStoreWithXMLConfig_hmacSHA256Test extends TestWithDynamoDB {
	
	
	public static final String CACHE_NAME = "users";
	
	
	private static final String HMAC_KEY = "ks32J/1JLlPFlmTu7CokHF67sVOjFq57HmcLcf27ctI=";
	
	
	protected EmbeddedCacheManager cacheMgr;
	
	
	private ClientAndServer mockServer;
	

	@Before
	@Override
	public void setUp()
		throws Exception {
		
		super.setUp();
		
		mockServer = startClientAndServer("localhost", 8000, 8001);
		
		// Configure via system property interpolation
		System.setProperty("dynamodb.hmacSHA256Key", HMAC_KEY);

		cacheMgr = new DefaultCacheManager("test-infinispan-hmac.xml");

		cacheMgr.start();
	}
	
	
	@After
	@Override
	public void tearDown()
		throws Exception {
		
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		mockServer.stop();
		
		System.clearProperty("dynamodb.hmacSHA256Key");
		
		super.tearDown();
	}
	

	@Test
	public void testWithHMACSHA256()
		throws InvalidKeyException, NoSuchAlgorithmException, InvalidHMACException {
		
		Cache<String,User> cache = cacheMgr.getCache(CACHE_NAME);
		
		@SuppressWarnings("unchecked")
		var store = (DynamoDBStore<String, User>) DynamoDBStore.getInstances().get(CACHE_NAME);
		assertNotNull(store);
		
		var config = (DynamoDBStoreConfiguration)cacheMgr
			.getCacheConfiguration(CACHE_NAME)
			.persistence()
			.stores()
			.get(0);
		
		assertEquals(HMAC_KEY, config.getHMACSHA256Key());
		
		var u1 = new User("Alice Adams", "alice@wonderland.net");
		
		cache.put("u1", u1);
		
		assertEquals(u1.getEmail(), cache.get("u1").getEmail());
		
		assertEquals(0L, store.getMeters().invalidItemHmacCounter.getCount());
		
		// Verify stored HMAC
		Item item = store.getTable().getItem(new GetItemSpec().withPrimaryKey(new PrimaryKey("uid", "u1")));
		ItemHMAC itemHMAC = new ItemHMAC(HMAC_KEY);
		itemHMAC.verify(item);
		
		// Remove HMAC
		store.getTable().putItem(item.removeAttribute(ItemHMAC.ATTRIBUTE_NAME));
		
		cache.getAdvancedCache().getDataContainer().clear(); // Clear cache
		
		try {
			cache.get("u1");
			fail();
		} catch (PersistenceException e) {
			assertEquals("Missing item HMAC attribute: _hmac#s256", e.getMessage());
		}
		
		assertEquals(1L, store.getMeters().invalidItemHmacCounter.getCount());
		
		// Break HMAC
		byte[] badHMAC = Base64.getDecoder().decode("F/x6p/WE0dFYIXHaSz78eAxNcGWEkwy6stK5A+EEDSs=");
		store.getTable().putItem(item.withBinary(ItemHMAC.ATTRIBUTE_NAME, badHMAC));
		
		cache.getAdvancedCache().getDataContainer().clear(); // Clear cache
		
		try {
			cache.get("u1");
			fail();
		} catch (PersistenceException e) {
			assertEquals("Invalid item HMAC: Stored: F/x6p/WE0dFYIXHaSz78eAxNcGWEkwy6stK5A+EEDSs= Computed: x6y2BJy8PUjl3ExaG5hOa1eanvYlIBJqA/5Z8Eog+ok= Item: {\"uid\":\"u1\",\"surname\":\"Adams\",\"given_name\":\"Alice\",\"email\":\"alice@wonderland.net\"}", e.getMessage());
		}
		
		assertEquals(2L, store.getMeters().invalidItemHmacCounter.getCount());
	}
}