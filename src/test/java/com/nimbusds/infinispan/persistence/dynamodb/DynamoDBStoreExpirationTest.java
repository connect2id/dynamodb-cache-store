package com.nimbusds.infinispan.persistence.dynamodb;


import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.infinispan.Cache;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.context.Flag;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.junit.After;
import org.junit.Before;

import com.nimbusds.common.monitor.MonitorRegistries;
import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfigurationBuilder;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


public class DynamoDBStoreExpirationTest extends TestWithDynamoDB {
	
	
	
	public static final String CACHE_NAME = "users";
	
	
	protected EmbeddedCacheManager cacheMgr;
	
	
	@Before
	@Override
	public void setUp()
		throws Exception {
		
		super.setUp();
		
		cacheMgr = new DefaultCacheManager();
		
		var b = new ConfigurationBuilder();
		b.persistence()
			.addStore(DynamoDBStoreConfigurationBuilder.class)
			.endpoint(endpoint)
			.itemTransformerClass(ExpiringUserItemTransformer.class)
			.segmented(false)
			.create();
		
		b.memory()
			.whenFull(EvictionStrategy.REMOVE)
			.maxCount(10)
			.create();
		
		cacheMgr.defineConfiguration(CACHE_NAME, b.build());
		
		cacheMgr.start();
	}
	
	
	@After
	@Override
	public void tearDown()
		throws Exception {
		
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		super.tearDown();
	}
	

	@Test
	public void testDoNotLoadExpired() {
		
		Cache<String, User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		String[] keys = new String[100];
		
		Instant created = Instant.now().minus(1L, ChronoUnit.DAYS);
		
		for (int i=0; i < 100; i ++) {
			
			String key = "a" + i;
			keys[i] = key;
			User value = new User("Alice Adams-" + i, "alice-" + i + "@email.com", created, new HashSet<>(Arrays.asList("admin", "audit")));
			
			assertNull(myMap.putIfAbsent(key, value));
		}
		
		assertEquals(10, myMap.getAdvancedCache().withFlags(Flag.SKIP_CACHE_LOAD).size());
		assertEquals(10, myMap.size());
		
		var numFoundExpired = new AtomicInteger();
		for (int i=0; i < 100; i ++) {
			
			String key = "a" + i;
			keys[i] = key;
			
			if (myMap.get(key) == null) {
				numFoundExpired.incrementAndGet();
			}
		}
		
		assertEquals(90, numFoundExpired.get());
	}
	

	@Test
	public void testDoNotProcessExpired() {
		
		Cache<String, User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("users.dynamoDB.processTimer").getCount());
		
		String[] keys = new String[100];
		
		Instant created = Instant.now().minus(1L, ChronoUnit.DAYS);
		
		for (int i=0; i < 100; i ++) {
			
			String key = "a" + i;
			keys[i] = key;
			User value = new User("Alice Adams-" + i, "alice-" + i + "@email.com", created, new HashSet<>(Arrays.asList("admin", "audit")));
			
			assertNull(myMap.putIfAbsent(key, value));
		}
		
		Map<String,User> filteredMap = myMap
			.entrySet()
			.stream()
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		
		assertEquals(10, filteredMap.size());
		assertEquals(10, myMap.getAdvancedCache().withFlags(Flag.SKIP_CACHE_LOAD).size());
		
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get("users.dynamoDB.processTimer").getCount());
	}
	

	@Test
	public void testExpiration()
		throws Exception {
		
		Cache<String, User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		assertEquals(10, myMap.getCacheConfiguration().memory().maxCount());
		assertEquals(EvictionStrategy.REMOVE, myMap.getCacheConfiguration().memory().whenFull());
		assertEquals(60000L, myMap.getCacheConfiguration().expiration().wakeUpInterval());

                Instant created = Instant.now().minus(1L, ChronoUnit.DAYS);

                String[] keys = new String[100];
                for (int i = 0; i < 100; i ++) {
			
			String key = "a" + i;
			keys[i] = key;
			User value = new User("Alice Adams-" + i, "alice-" + i + "@email.com", created, new HashSet<>(Arrays.asList("admin", "audit")));
			
			assertNull(myMap.putIfAbsent(key, value));
		}
		
		assertEquals(10, myMap.getAdvancedCache().withFlags(Flag.SKIP_CACHE_LOAD).size());
		assertEquals(10, myMap.size());
	
		// reap expired entries
		myMap.getAdvancedCache().getExpirationManager().processExpiration();
		
		while (10 != myMap.size()) {
			Thread.sleep(100);
		}
		
		assertEquals(10, myMap.size()); // only in-memory entries remain
	}
}
