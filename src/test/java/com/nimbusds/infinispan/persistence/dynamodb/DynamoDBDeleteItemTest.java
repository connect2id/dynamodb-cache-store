package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.dynamodbv2.model.*;
import org.junit.Assert;
import org.junit.Test;


public class DynamoDBDeleteItemTest extends TestWithDynamoDB {


        @Test
        public void testDeleteOutcome_noReturnValues()
		throws Exception {
		
		Table table = dynamoDB.createTable(new CreateTableRequest()
			.withTableName("test-deletion")
			.withKeySchema(new KeySchemaElement("key", KeyType.HASH))
			.withAttributeDefinitions(new AttributeDefinition("key", ScalarAttributeType.S))
			.withProvisionedThroughput(new ProvisionedThroughput(1L, 1L)));
		
		table.waitForActive();
		
		table.putItem(new Item()
			.withPrimaryKey("key", "1")
			.with("apples", 10)
			.with("pears", 20));
		
		DeleteItemOutcome dio = table.deleteItem(new DeleteItemSpec().withPrimaryKey("key", "1"));
		
		Assert.assertNull(dio.getItem());
		Assert.assertNotNull(dio.getDeleteItemResult());
		Assert.assertNull(dio.getDeleteItemResult().getAttributes());
		
		dio = table.deleteItem(new DeleteItemSpec().withPrimaryKey("key", "invalid-key"));
		
		Assert.assertNull(dio.getItem());
		Assert.assertNotNull(dio.getDeleteItemResult());
		Assert.assertNull(dio.getDeleteItemResult().getAttributes());
	}


        @Test
        public void testDeleteOutcome_withReturnValues()
		throws Exception {
		
		Table table = dynamoDB.createTable(new CreateTableRequest()
			.withTableName("test-deletion")
			.withKeySchema(new KeySchemaElement("key", KeyType.HASH))
			.withAttributeDefinitions(new AttributeDefinition("key", ScalarAttributeType.S))
			.withProvisionedThroughput(new ProvisionedThroughput(1L, 1L)));
		
		table.waitForActive();
		
		table.putItem(new Item()
			.withPrimaryKey("key", "1")
			.with("apples", 10)
			.with("pears", 20));
		
		DeleteItemOutcome dio = table.deleteItem(new DeleteItemSpec().withPrimaryKey("key", "1").withReturnValues(ReturnValue.ALL_OLD));
		
		Assert.assertNotNull(dio.getItem());
		Assert.assertNotNull(dio.getDeleteItemResult());
		Assert.assertNotNull(dio.getDeleteItemResult().getAttributes());
		
		dio = table.deleteItem(new DeleteItemSpec().withPrimaryKey("key", "invalid-key").withReturnValues(ReturnValue.ALL_OLD));
		
		Assert.assertNull(dio.getItem());
		Assert.assertNotNull(dio.getDeleteItemResult());
		Assert.assertNull(dio.getDeleteItemResult().getAttributes());
	}
}
