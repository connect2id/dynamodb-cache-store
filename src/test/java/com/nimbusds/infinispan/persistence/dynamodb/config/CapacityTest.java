package com.nimbusds.infinispan.persistence.dynamodb.config;

import org.junit.Assert;
import org.junit.Test;


public class CapacityTest {


        @Test
        public void testAbsolute() {

                double value = 10.5;

                var capacity = new Capacity(value, Capacity.Measure.ABSOLUTE);

                Assert.assertEquals(value, capacity.value(), 0.0);
                Assert.assertEquals(Capacity.Measure.ABSOLUTE, capacity.measure());

                String s = capacity.toString();

                Assert.assertEquals("10.5", s);

                capacity = Capacity.parse(s);

                Assert.assertEquals(value, capacity.value(), 0.0);
                Assert.assertEquals(Capacity.Measure.ABSOLUTE, capacity.measure());
        }


        @Test
        public void testPercentage() {

                double value = 20.0;

                var capacity = new Capacity(value, Capacity.Measure.PERCENT);

                Assert.assertEquals(value, capacity.value(), 0.0);
                Assert.assertEquals(Capacity.Measure.PERCENT, capacity.measure());

                String s = capacity.toString();

                Assert.assertEquals("20.0%", s);

                capacity = Capacity.parse(s);

                Assert.assertEquals(value, capacity.value(), 0.0);
                Assert.assertEquals(Capacity.Measure.PERCENT, capacity.measure());
        }


        @Test
        public void testValueMustBePositive() {

                try {
                        new Capacity(0.0, Capacity.Measure.ABSOLUTE);
                        Assert.fail();
                } catch (AssertionError e) {
                        e.getMessage();
                }

                try {
                        new Capacity(0.0, Capacity.Measure.PERCENT);
                        Assert.fail();
                } catch (AssertionError e) {
                        e.getMessage();
                }
        }


        @Test
        public void testMeasureMustBeNonNull() {

                try {
                        new Capacity(1.0, null);
                        Assert.fail();
                } catch (AssertionError e) {
                        e.getMessage();
                }
        }


        @Test
        public void testParseException() {

                try {
                        Capacity.parse("abc");
                        Assert.fail();
                } catch (NumberFormatException e) {
                        Assert.assertEquals("Illegal capacity: For input string: \"abc\"", e.getMessage());
                }

                try {
                        Capacity.parse("10%10");
                        Assert.fail();
                } catch (NumberFormatException e) {
                        Assert.assertEquals("Illegal capacity: For input string: \"10%10\"", e.getMessage());
                }
        }
}
