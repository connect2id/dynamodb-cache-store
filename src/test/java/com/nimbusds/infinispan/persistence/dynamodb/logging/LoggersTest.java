package com.nimbusds.infinispan.persistence.dynamodb.logging;


import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class LoggersTest {


        @Test
        public void testNames() {

		assertEquals("MAIN", Loggers.MAIN_LOG.getName());
		assertEquals("DYN", Loggers.DYNAMODB_LOG.getName());
	}
}
