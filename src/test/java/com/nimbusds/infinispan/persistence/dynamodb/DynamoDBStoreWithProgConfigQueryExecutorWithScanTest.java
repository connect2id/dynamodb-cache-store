package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.services.dynamodbv2.document.Item;
import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.common.query.QueryExecutor;
import com.nimbusds.infinispan.persistence.common.query.SimpleMatchQuery;
import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfigurationBuilder;
import com.nimbusds.infinispan.persistence.dynamodb.query.SimpleMatchQueryExecutor;
import org.infinispan.Cache;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.eviction.EvictionType;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.time.Instant;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;


/**
 * Tests scan queries.
 */
public class DynamoDBStoreWithProgConfigQueryExecutorWithScanTest extends TestWithDynamoDB {
	
	
	public static final String CACHE_NAME = "users";


	protected EmbeddedCacheManager cacheMgr;
	

	@Before
	@Override
	public void setUp()
		throws Exception {
		
		super.setUp();
		
		cacheMgr = new DefaultCacheManager();

		// no indexed attributes!!!
		var b = new ConfigurationBuilder();
		b.persistence()
			.addStore(DynamoDBStoreConfigurationBuilder.class)
			.endpoint(endpoint)
			.itemTransformerClass(UserItemTransformer.class)
			.queryExecutorClass(SimpleMatchQueryExecutor.class)
			.segmented(false)
			.create();
		
		b.memory()
			.whenFull(EvictionStrategy.REMOVE)
			.maxCount(100)
			.create();

		cacheMgr.defineConfiguration(CACHE_NAME, b.build());

		cacheMgr.start();
	}


	@After
	@Override
	public void tearDown()
		throws Exception {
		
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		super.tearDown();
	}
	

	@Test
	public void testSimpleObjectLifeCycle() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		assertNotNull(DynamoDBStore.getInstances().get(CACHE_NAME));
		assertEquals(1, DynamoDBStore.getInstances().size());
		
		assertNull(((DynamoDBStore)DynamoDBStore.getInstances().get(CACHE_NAME)).getConfiguration().getIndexAttributes());
		
		// Initial size
		assertEquals(0, myMap.size());
		
		// Store new object with all fields defined
		Instant created = Instant.ofEpochMilli(1474012953000L);
		User u1 = new User("Alice Adams", "alice@wonderland.net", created, new HashSet<>(Arrays.asList("admin", "audit")));
		assertNull(myMap.putIfAbsent("u1", u1));
	
		// Query
		final List<InfinispanEntry<String,User>> queryResult = new LinkedList<>();
		
		QueryExecutor<String,User> queryExecutor = DynamoDBStore.getInstances().get(CACHE_NAME).getQueryExecutor();
		
		queryExecutor.executeQuery(new SimpleMatchQuery<>("email", "alice@wonderland.net"), queryResult::add);
		
		assertEquals("u1", queryResult.get(0).getKey());
		assertEquals("Alice Adams", queryResult.get(0).getValue().getName());
		assertEquals("alice@wonderland.net", queryResult.get(0).getValue().getEmail());
		assertEquals(created, queryResult.get(0).getValue().getCreated());
		assertEquals(new HashSet<>(Arrays.asList("admin", "audit")), queryResult.get(0).getValue().getPermissions());
		assertEquals(1, queryResult.size());
		
		queryResult.clear();
		
		// Update object
		User u2 = new User("Bob Brown", "bob@wonderland.net", created, new HashSet<>(Arrays.asList("browse", "pay")));
		assertNotNull(myMap.replace("u1", u2));
		
		// Confirm put
		Item item = dynamoDB.getTable("test-users").getItem("uid", "u1");
		assertEquals("u1", item.get("uid"));
		assertEquals("Bob",  item.get("given_name"));
		assertEquals("Brown",  item.get("surname"));
		assertEquals(u2.getEmail(),  item.get("email"));
		assertEquals(created, Instant.ofEpochMilli(item.getLong("created")));
		assertEquals(u2.getPermissions(), item.getStringSet("permissions"));
		assertEquals(6, item.numberOfAttributes());
		
		// Query
		queryExecutor.executeQuery(new SimpleMatchQuery<>("email", "bob@wonderland.net"), queryResult::add);
		
		assertEquals("u1", queryResult.get(0).getKey());
		assertEquals("Bob Brown", queryResult.get(0).getValue().getName());
		assertEquals("bob@wonderland.net", queryResult.get(0).getValue().getEmail());
		assertEquals(created, queryResult.get(0).getValue().getCreated());
		assertEquals(new HashSet<>(Arrays.asList("browse", "pay")), queryResult.get(0).getValue().getPermissions());
		assertEquals(1, queryResult.size());
		
		queryResult.clear();
		
		// Update object with fewer defined fields
		User u3 = new User("Claire Cox", "claire@wonderland.net");
		assertNotNull(myMap.replace("u1", u3));
		
		// Remove object
		User out = myMap.remove("u1");
		assertEquals(u3.getName(), out.getName());
		assertEquals(u3.getEmail(), out.getEmail());
		assertNull(out.getCreated());
		assertNull(out.getPermissions());
		
		// Query
		queryExecutor.executeQuery(new SimpleMatchQuery<>("email", "alice@wonderland.net"), queryResult::add);
		queryExecutor.executeQuery(new SimpleMatchQuery<>("email", "bob@wonderland.net"), queryResult::add);
		queryExecutor.executeQuery(new SimpleMatchQuery<>("email", "claire@wonderland.net"), queryResult::add);
		assertEquals(0, queryResult.size());
		
		// Zero count
		assertEquals(0, myMap.size());
		assertEquals(0, myMap.getAdvancedCache().size());
		assertEquals(0, myMap.getAdvancedCache().getDataContainer().size());
	}
	

	@Test
	public void testMultiplePutIfAbsentWithEviction() {
		
		Cache<String, User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		assertEquals(100, myMap.getCacheConfiguration().memory().size());
		assertEquals(EvictionType.COUNT, myMap.getCacheConfiguration().memory().evictionType());
		assertEquals(60000L, myMap.getCacheConfiguration().expiration().wakeUpInterval());
		
		String[] keys = new String[200];
		
		final Instant created = Instant.ofEpochMilli(1474102000L);
		
		for (int i=0; i < 200; i ++) {
			
			String key = "a" + i;
			keys[i] = key;
			User value = new User("Alice Adams-" + i, "alice@email.com", created, new HashSet<>(Arrays.asList("admin", "audit")));
			
			assertNull(myMap.putIfAbsent(key, value));
		}
		
		// Confirm put
		for (int i=0; i < 200; i ++) {
			
			Item item = dynamoDB.getTable("test-users").getItem("uid", keys[i]);
			assertEquals(keys[i], item.get("uid"));
			assertEquals("Alice",  item.get("given_name"));
			assertEquals("Adams-" + i,  item.get("surname"));
			assertEquals("alice@email.com",  item.get("email"));
			assertEquals(created.toEpochMilli(), item.getLong("created"));
			assertEquals(new HashSet<>(Arrays.asList("admin", "audit")), item.getStringSet("permissions"));
			assertEquals(6, item.numberOfAttributes());
		}
		
		// Query
		QueryExecutor<String,User> queryExecutor = DynamoDBStore.getInstances().get(CACHE_NAME).getQueryExecutor();
		
		List<InfinispanEntry<String, User>> result = new LinkedList<>();
		
		queryExecutor.executeQuery(new SimpleMatchQuery<>("email", "alice@email.com"), result::add);
		
		result.forEach(en -> assertEquals("alice@email.com", en.getValue().getEmail()));
		
		assertEquals(200, result.size());
	}
}