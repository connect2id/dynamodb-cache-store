package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.services.dynamodbv2.document.BatchWriteItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.TableWriteItems;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.WriteRequest;
import com.codahale.metrics.Timer;
import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.dynamodb.config.Capacity;
import org.infinispan.persistence.spi.AdvancedCacheExpirationWriter;
import org.infinispan.persistence.spi.MarshallableEntry;
import org.junit.Test;

import java.time.Duration;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.*;


public class ExpiredEntryReaperTest extends TestWithDynamoDB {
	
	
	static final long READ_CAPACITY = 100L;
	
	
	static final long WRITE_CAPACITY = 100L;


	static final Capacity PURGE_MAX_READ_CAPACITY = new Capacity(10.0, Capacity.Measure.PERCENT);


	@Test
	public void testConstant() {

		assertEquals(100, ExpiredEntryReaper.PAGE_SIZE);
	}


	@Test
	public void testResolvePurgeMaxReadCapacity() throws InterruptedException {

		var requestFactory = new RequestFactory<>(
			new ExpiringUserItemTransformer(),
			null,
			false,
			new ProvisionedThroughput(READ_CAPACITY, WRITE_CAPACITY),
			false,
                        false, "",
			null,
			null,
			false);

		// Create table
		Instant startTs = Instant.now();

		Table table = dynamoDB.createTable(requestFactory.resolveCreateTableRequest());

		table.waitForActive();

		System.out.println("Created table " + table.getTableName() + ": " + Duration.between(startTs, Instant.now()));

		for (Capacity configuredPurgeMaxReadCapacity: List.of(new Capacity(10, Capacity.Measure.PERCENT), new Capacity(20.0, Capacity.Measure.ABSOLUTE), new Capacity(50.0, Capacity.Measure.ABSOLUTE))) {

			var purgeTimer = new Timer();
			var deleteTimer = new Timer();

			var reaper = new ExpiredEntryReaper<>(new MarshalledEntryFactoryImpl<>(), table, requestFactory, configuredPurgeMaxReadCapacity, -1L, purgeTimer, deleteTimer);

			long readCapacityUnits = table.getDescription().getProvisionedThroughput().getReadCapacityUnits();
			assertEquals(READ_CAPACITY, readCapacityUnits);

			if (Capacity.Measure.ABSOLUTE.equals(configuredPurgeMaxReadCapacity.measure())) {
				assertEquals(configuredPurgeMaxReadCapacity.value(), reaper.resolveAbsolutePurgeMaxReadCapacity(), 0.0);
			} else if (Capacity.Measure.PERCENT.equals(configuredPurgeMaxReadCapacity.measure())){
				assertEquals(READ_CAPACITY / 10.0, reaper.resolveAbsolutePurgeMaxReadCapacity(), 0.0);
			} else {
				fail();
			}
		}
	}
	

	@Test
	public void testExpireWithKeyListener()
		throws Exception {
		
		var requestFactory = new RequestFactory<>(
			new ExpiringUserItemTransformer(),
			null,
			false,
			new ProvisionedThroughput(READ_CAPACITY, WRITE_CAPACITY),
			false,
                        false, "",
			null,
			null,
			false);
		
		// Create table
		Instant startTs = Instant.now();
		
		Table table = dynamoDB.createTable(requestFactory.resolveCreateTableRequest());
		
		table.waitForActive();
		
		System.out.println("Created table " + table.getTableName() + ": " + Duration.between(startTs, Instant.now()));
		
		startTs = Instant.now();
		
		for (int i=0; i < 2000; ++i) {
			
			var user = new User("Alice " + i, "hello-" + i + "@email.com");
			
			table.putItem(requestFactory.resolveItem(new InfinispanEntry<>(i + "", user, null)));
		}
		
		System.out.println("Added 2000 items: " + Duration.between(startTs, Instant.now()));
		
		
		startTs = Instant.now();
		
		var counter = new AtomicInteger(0);
		table.scan().forEach(item -> counter.incrementAndGet());
		assertEquals(2000, counter.get());
		
		System.out.println("Scanned 2000 items: " + Duration.between(startTs, Instant.now()));
		
		var purgeTimer = new Timer();
		var deleteTimer = new Timer();
		
		var reaper = new ExpiredEntryReaper<>(new MarshalledEntryFactoryImpl<>(), table, requestFactory, PURGE_MAX_READ_CAPACITY, -1L, purgeTimer, deleteTimer);
		
		List<String> deletedKeys = new LinkedList<>();
		
		startTs = Instant.now();
		
		// Purge expired entries (all must be marked as such)
		assertEquals(2000, reaper.purge(key -> {
			assertNotNull(key);
			deletedKeys.add(key);
		}));
		
		assertEquals(1, purgeTimer.getCount());
		
		System.out.println("Purged 2000 items: " + Duration.between(startTs, Instant.now()));
		
		
		// Check deleted keys pushed to listener
		for (int i=0; i < 2000;++i) {
			assertTrue(deletedKeys.contains(i + ""));
		}
		
		assertEquals(2000, deletedKeys.size());

		assertEquals(1, purgeTimer.getCount());
		assertEquals(2000, deleteTimer.getCount());
	}
	

	@Test
	public void testExpireWithKeyListener_limit_1000()
		throws Exception {
		
		var requestFactory = new RequestFactory<>(
			new ExpiringUserItemTransformer(),
			null,
			false,
			new ProvisionedThroughput(READ_CAPACITY, WRITE_CAPACITY),
			false,
                        false, "",
			null,
			null,
			false);
		
		// Create table
		Instant startTs = Instant.now();
		
		Table table = dynamoDB.createTable(requestFactory.resolveCreateTableRequest());
		
		table.waitForActive();
		
		System.out.println("Created table " + table.getTableName() + ": " + Duration.between(startTs, Instant.now()));
		
		startTs = Instant.now();
		
		for (int i=0; i < 2000; ++i) {
			
			User user = new User("Alice " + i, "hello-" + i + "@email.com");
			
			table.putItem(requestFactory.resolveItem(new InfinispanEntry<>(i + "", user, null)));
		}
		
		System.out.println("Added 2000 items: " + Duration.between(startTs, Instant.now()));
		
		
		startTs = Instant.now();
		
		var counter = new AtomicInteger(0);
		table.scan().forEach(item -> counter.incrementAndGet());
		assertEquals(2000, counter.get());
		
		System.out.println("Scanned 2000 items: " + Duration.between(startTs, Instant.now()));
		
		var purgeTimer = new Timer();
		var deleteTimer = new Timer();
		
		var reaper = new ExpiredEntryReaper<>(new MarshalledEntryFactoryImpl<>(), table, requestFactory, PURGE_MAX_READ_CAPACITY, 1000, purgeTimer, deleteTimer);
		
		List<String> deletedKeys = new LinkedList<>();
		
		startTs = Instant.now();
		
		// Purge expired entries (all must be marked as such)
		assertEquals(1000, reaper.purge(key -> {
			assertNotNull(key);
			deletedKeys.add(key);
		}));
		
		assertEquals(1, purgeTimer.getCount());
		assertEquals(1000, deleteTimer.getCount());
		
		System.out.println("Purged 1000 items: " + Duration.between(startTs, Instant.now()));
		
		
		// Check deleted keys pushed to listener
		assertEquals(1000, deletedKeys.size());
	}
	

	@Test
	public void testExpireWithEntryListener()
		throws Exception {
		
		var requestFactory = new RequestFactory<>(
			new ExpiringUserItemTransformer(),
			null,
			false,
			new ProvisionedThroughput(READ_CAPACITY, WRITE_CAPACITY),
			false,
                        false, "",
			null,
			null,
			false);
		
		// Create table
		Instant startTs = Instant.now();
		
		Table table = dynamoDB.createTable(requestFactory.resolveCreateTableRequest());
		
		table.waitForActive();
		
		System.out.println("Created table " + table.getTableName() + ": " + Duration.between(startTs, Instant.now()));
		
		startTs = Instant.now();
		
		for (int i=0; i < 2000; ++i) {
			
			var user = new User("Alice " + i, "hello-" + i + "@email.com");
			
			table.putItem(requestFactory.resolveItem(new InfinispanEntry<>(i + "", user, null)));
		}
		
		System.out.println("Added 2000 items: " + Duration.between(startTs, Instant.now()));
		
		
		startTs = Instant.now();
		
		var counter = new AtomicInteger(0);
		table.scan().forEach(item -> counter.incrementAndGet());
		assertEquals(2000, counter.get());
		
		System.out.println("Scanned 2000 items: " + Duration.between(startTs, Instant.now()));
		
		var purgeTimer = new Timer();
		var deleteTimer = new Timer();
		
		var reaper = new ExpiredEntryReaper<>(new MarshalledEntryFactoryImpl<>(), table, requestFactory, PURGE_MAX_READ_CAPACITY, -1, purgeTimer, deleteTimer);
		
		List<String> deletedKeys = new LinkedList<>();
		
		startTs = Instant.now();
		
		// Purge expired entries (all must be marked as such)
		assertEquals(2000, reaper.purgeExtended(
			new AdvancedCacheExpirationWriter.ExpirationPurgeListener<>() {
				
				@Override
				public void marshalledEntryPurged(MarshallableEntry<String, User> entry) {
					assertNotNull(entry);
					deletedKeys.add(entry.getKey());
					assertNotNull(entry.getValue());
					assertNotNull(entry.getMetadata());
				}
				
				
				@Override
				public void entryPurged(String s) {
					fail(); // never called
				}
			}));
		
		assertEquals(1, purgeTimer.getCount());
		assertEquals(2000, deleteTimer.getCount());
		
		System.out.println("Purged 2000 items: " + Duration.between(startTs, Instant.now()));
		
		
		// Check deleted keys pushed to listener
		for (int i=0; i < 2000;++i) {
			assertTrue(deletedKeys.contains(i + ""));
		}
		
		assertEquals(2000, deletedKeys.size());
	}


	@Test
	public void testExpireWithEntryListener_limit_1000()
		throws Exception {
		
		var requestFactory = new RequestFactory<>(
			new ExpiringUserItemTransformer(),
			null,
			false,
			new ProvisionedThroughput(READ_CAPACITY, WRITE_CAPACITY),
			false,
                        false, "",
			null,
			null,
			false);
		
		// Create table
		Instant startTs = Instant.now();
		
		Table table = dynamoDB.createTable(requestFactory.resolveCreateTableRequest());
		
		table.waitForActive();
		
		System.out.println("Created table " + table.getTableName() + ": " + Duration.between(startTs, Instant.now()));
		
		startTs = Instant.now();
		
		for (int i=0; i < 2000; ++i) {
			
			var user = new User("Alice " + i, "hello-" + i + "@email.com");
			
			table.putItem(requestFactory.resolveItem(new InfinispanEntry<>(i + "", user, null)));
		}
		
		System.out.println("Added 2000 items: " + Duration.between(startTs, Instant.now()));
		
		
		startTs = Instant.now();
		
		var counter = new AtomicInteger(0);
		table.scan().forEach(item -> counter.incrementAndGet());
		assertEquals(2000, counter.get());
		
		System.out.println("Scanned 2000 items: " + Duration.between(startTs, Instant.now()));
		
		var purgeTimer = new Timer();
		var deleteTimer = new Timer();
		
		var reaper = new ExpiredEntryReaper<>(new MarshalledEntryFactoryImpl<>(), table, requestFactory, PURGE_MAX_READ_CAPACITY, 1000, purgeTimer, deleteTimer);
		
		List<String> deletedKeys = new LinkedList<>();
		
		startTs = Instant.now();
		
		// Purge expired entries (all must be marked as such)
		assertEquals(1000, reaper.purgeExtended(
			new AdvancedCacheExpirationWriter.ExpirationPurgeListener<>() {
				
				@Override
				public void marshalledEntryPurged(MarshallableEntry<String, User> entry) {
					deletedKeys.add(entry.getKey());
					assertNotNull(entry.getValue());
					assertNotNull(entry.getMetadata());
				}
				
				
				@Override
				public void entryPurged(String s) {
					fail(); // never called
				}
			}));
		
		assertEquals(1, purgeTimer.getCount());
		assertEquals(1000, deleteTimer.getCount());
		
		System.out.println("Purged 1000 items: " + Duration.between(startTs, Instant.now()));
		
		
		// Check deleted keys pushed to listener
		assertEquals(1000, deletedKeys.size());
	}
	
	
	static class BatchQueue25 extends LinkedList<Item> {
		
		List<Item> pollBatch() {
		
			List<Item> batch = new LinkedList<>();
			
			while(peek() != null && batch.size() <= 25) {
				batch.add(poll());
			}
			
			return batch.isEmpty() ? null : batch;
		}
	}
	
	
	private void batchPut(final Table table, final BatchQueue25 queue) {
		
		while (queue.peek() != null) {
			
			TableWriteItems writeItems = new TableWriteItems(table.getTableName())
				.withItemsToPut(queue.pollBatch());
			
			BatchWriteItemOutcome outcome = dynamoDB.batchWriteItem(writeItems);
			
			do {
				Map<String,List<WriteRequest>> unprocessedItems = outcome.getUnprocessedItems();
				
				if (outcome.getUnprocessedItems().size() > 0) {
					System.out.println("Retrieving unprocessed items...");
					outcome = dynamoDB.batchWriteItemUnprocessed(unprocessedItems);
				}
				
			} while (outcome.getUnprocessedItems().size() > 0);
		}
	}
	

	@Test
	public void testExpire_withBatchAdd()
		throws Exception {
		
		var requestFactory = new RequestFactory<>(
			new ExpiringUserItemTransformer(),
			null,
			false,
			new ProvisionedThroughput(READ_CAPACITY, WRITE_CAPACITY),
			false,
                        false, "",
			null,
			null,
			false);
		
		// Create table
		Instant startTs = Instant.now();
		
		Table table = dynamoDB.createTable(requestFactory.resolveCreateTableRequest());
		
		table.waitForActive();
		
		System.out.println("Created table " + table.getTableName() + ": " + Duration.between(startTs, Instant.now()));
		
		startTs = Instant.now();
		
		var users = new BatchQueue25();
		
		for (int i=0; i < 2000; ++i) {
			
			User user = new User("Alice " + i, "hello-" + i + "@email.com");
			users.add(requestFactory.resolveItem(new InfinispanEntry<>(i + "", user, null)));
			batchPut(table, users);
		}
		
		System.out.println("Added 2000 items: " + Duration.between(startTs, Instant.now()));
		
		
		startTs = Instant.now();
		
		var counter = new AtomicInteger(0);
		table.scan().forEach(item -> counter.incrementAndGet());
		assertEquals(2000, counter.get());
		
		System.out.println("Scanned 2000 items: " + Duration.between(startTs, Instant.now()));
		
		var purgeTimer = new Timer();
		var deleteTimer = new Timer();
		
		var reaper = new ExpiredEntryReaper<>(new MarshalledEntryFactoryImpl<>(), table, requestFactory, PURGE_MAX_READ_CAPACITY, -1, purgeTimer, deleteTimer);
		
		List<String> deletedKeys = new LinkedList<>();
		
		startTs = Instant.now();
		
		// Purge expired entries (all must be marked as such)
		assertEquals(2000, reaper.purge(key -> {
			assertNotNull(key);
			deletedKeys.add(key);
		}));
		
		assertEquals(1, purgeTimer.getCount());
		assertEquals(2000, deleteTimer.getCount());
		
		System.out.println("Purged 2000 items: " + Duration.between(startTs, Instant.now()));
		
		
		// Check deleted keys pushed to listener
		for (int i=0; i < 2000;++i) {
			assertTrue(deletedKeys.contains(i + ""));
		}
		
		assertEquals(2000, deletedKeys.size());
	}
	

	@Test
	public void testExpire_withRangeKey()
		throws Exception {
		
		var requestFactory = new RequestFactory<>(
			new ExpiringUserItemTransformer(),
			null,
			false,
			new ProvisionedThroughput(READ_CAPACITY, WRITE_CAPACITY),
			false,
                        false, "",
			"tid",
			"123",
			false);
		
		// Create table
		Table table = dynamoDB.createTable(requestFactory.resolveCreateTableRequest());
		
		table.waitForActive();
		
		for (int i=0; i < 2000; ++i) {
			
			User user = new User("Alice " + i, "hello-" + i + "@email.com");
			
			table.putItem(requestFactory.resolveItem(new InfinispanEntry<>(i + "", user, null)));
		}
		
		var counter = new AtomicInteger(0);
		table.scan().forEach(item -> counter.incrementAndGet());
		assertEquals(2000, counter.get());
		
		var purgeTimer = new Timer();
		var deleteTimer = new Timer();
		
		var reaper = new ExpiredEntryReaper<>(new MarshalledEntryFactoryImpl<>(), table, requestFactory, PURGE_MAX_READ_CAPACITY, -1, purgeTimer, deleteTimer);
		
		List<String> deletedKeys = new LinkedList<>();
		
		// Purge expired entries (all must be marked as such)
		assertEquals(2000, reaper.purge(key -> {
			assertNotNull(key);
			deletedKeys.add(key);
		}));
		
		assertEquals(1, purgeTimer.getCount());
		assertEquals(2000, deleteTimer.getCount());
		
		// Check deleted keys pushed to listener
		for (int i=0; i < 2000;++i) {
			assertTrue(deletedKeys.contains(i + ""));
		}
		
		assertEquals(2000, deletedKeys.size());
	}
	

	@Test
	public void testExpire_withTableNamePrefix()
		throws Exception {
		
		var requestFactory = new RequestFactory<>(
			new ExpiringUserItemTransformer(),
			null,
			false,
			new ProvisionedThroughput(READ_CAPACITY, WRITE_CAPACITY),
			false,
                        false, "prefix-",
			null,
			null,
			false);
		
		// Create table
		Table table = dynamoDB.createTable(requestFactory.resolveCreateTableRequest());
		
		table.waitForActive();
		
		assertTrue(table.getTableName().startsWith("prefix-test-users-"));
		
		for (int i=0; i < 2000; ++i) {
			
			User user = new User("Alice " + i, "hello-" + i + "@email.com");
			
			table.putItem(requestFactory.resolveItem(new InfinispanEntry<>(i + "", user, null)));
		}
		
		var counter = new AtomicInteger(0);
		table.scan().forEach(item -> counter.incrementAndGet());
		assertEquals(2000, counter.get());
		
		var purgeTimer = new Timer();
		var deleteTimer = new Timer();
		
		var reaper = new ExpiredEntryReaper<>(new MarshalledEntryFactoryImpl<>(), table, requestFactory, PURGE_MAX_READ_CAPACITY, -1, purgeTimer, deleteTimer);
		
		List<String> deletedKeys = new LinkedList<>();
		
		// Purge expired entries (all must be marked as such)
		assertEquals(2000, reaper.purge(key -> {
			assertNotNull(key);
			deletedKeys.add(key);
		}));
		
		assertEquals(1, purgeTimer.getCount());
		assertEquals(2000, deleteTimer.getCount());
		
		// Check deleted keys pushed to listener
		for (int i=0; i < 2000;++i) {
			assertTrue(deletedKeys.contains(i + ""));
		}
		
		assertEquals(2000, deletedKeys.size());
	}


	@Test
	public void testSwallowUncheckedException() throws InterruptedException {

		var requestFactory = new RequestFactory<>(
			new UserItemTransformerThrowsUncheckedException(),
			null,
			false,
			new ProvisionedThroughput(READ_CAPACITY, WRITE_CAPACITY),
			false,
                        false, "",
			null,
			null,
			false);

		// Create table
		Instant startTs = Instant.now();

		Table table = dynamoDB.createTable(requestFactory.resolveCreateTableRequest());

		table.waitForActive();

		System.out.println("Created table " + table.getTableName() + ": " + Duration.between(startTs, Instant.now()));

		var purgeTimer = new Timer();
		var deleteTimer = new Timer();

		User user = new User("Alice 123", "alice-123@email.com");

		table.putItem(requestFactory.resolveItem(new InfinispanEntry<>("alice-123", user, null)));

		var reaper = new ExpiredEntryReaper<>(new MarshalledEntryFactoryImpl<>(), table, requestFactory, PURGE_MAX_READ_CAPACITY, -1L, purgeTimer, deleteTimer);

		assertEquals(0L, reaper.purgeExtended(new AdvancedCacheExpirationWriter.ExpirationPurgeListener<>() {
                        @Override
                        public void marshalledEntryPurged(MarshallableEntry<String, User> entry) {
                                fail("Must not be called");
                        }

                        @Override
                        public void entryPurged(String key) {
                                fail("Must not be called");
                        }
                }));
	}
}
