package com.nimbusds.infinispan.persistence.dynamodb;


import java.time.Instant;
import java.util.Objects;
import java.util.Set;

import net.jcip.annotations.Immutable;
import org.apache.commons.lang3.builder.ToStringBuilder;


/**
 * User POJO, test class.
 */
@Immutable
public final class User {
	
	
	private final String name;
	
	
	private final String email;
	
	
	private final Instant created;
	
	
	private final Set<String> permissions;
	
	
	public User(final String name, final String email) {
		this(name, email, null, null);
	}
	
	
	public User(final String name, final String email, final Instant created, final Set<String> permissions) {
		this.name = name;
		this.email = email;
		this.created = created;
		this.permissions = permissions;
	}
	
	
	public String getName() {
		return name;
	}
	
	
	public String getEmail() {
		return email;
	}
	
	
	public Instant getCreated() {
		return created;
	}
	
	
	public Set<String> getPermissions() {
		return permissions;
	}
	
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof User user)) return false;
		return Objects.equals(getName(), user.getName()) && Objects.equals(getEmail(), user.getEmail()) && Objects.equals(getCreated(), user.getCreated()) && Objects.equals(getPermissions(), user.getPermissions());
	}
	
	
	@Override
	public int hashCode() {
		return Objects.hash(getName(), getEmail(), getCreated(), getPermissions());
	}
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
			.append("name", name)
			.append("email", email)
			.append("created", created)
			.append("permissions", permissions)
			.toString();
	}
}
