package com.nimbusds.infinispan.persistence.dynamodb;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;

import org.infinispan.Cache;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockserver.integration.ClientAndServer;

import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfiguration;
import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfigurationBuilder;


/**
 * Tests the DynamoDB store with a programmatic config.
 */
public class DynamoDBStoreWithProgConfig_httpProxyTest extends TestWithDynamoDB {
	
	
	public static final String CACHE_NAME = "users";


	protected EmbeddedCacheManager cacheMgr;
	
	
	private ClientAndServer mockServer;
	

	@Before
	@Override
	public void setUp()
		throws Exception {
		
		super.setUp();
		
		mockServer = startClientAndServer("localhost", 8000, 8001);
		
		cacheMgr = new DefaultCacheManager();
		
		var b = new ConfigurationBuilder();
		b.persistence()
			.addStore(DynamoDBStoreConfigurationBuilder.class)
			.endpoint(endpoint)
			.itemTransformerClass(UserItemTransformer.class)
			.httpProxyHost("localhost")
			.httpProxyPort(8001)
			.segmented(false)
			.create();
		
		b.memory()
			.whenFull(EvictionStrategy.REMOVE)
			.maxCount(100)
			.create();
		
		cacheMgr.defineConfiguration(CACHE_NAME, b.build());
		
		cacheMgr.start();
	}


	@After
	@Override
	public void tearDown()
		throws Exception {
		
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		mockServer.stop();
		
		super.tearDown();
	}
	

	@Test
	public void testWithEnabledProxy() {
		
		Cache<String,User> cache = cacheMgr.getCache(CACHE_NAME);
		
		var store = (DynamoDBStore) DynamoDBStore.getInstances().get(CACHE_NAME);
		assertNotNull(store);
		
		assertEquals("localhost", ((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration(CACHE_NAME).persistence().stores().get(0)).getHTTPProxyHost());
		assertEquals(8001, ((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration(CACHE_NAME).persistence().stores().get(0)).getHTTPProxyPort());
		
		var u1 = new User("Alice Adams", "alice@wonderland.net");
		
		cache.put("u1", u1);
		
		assertEquals(u1.getEmail(), cache.get("u1").getEmail());
	}
}