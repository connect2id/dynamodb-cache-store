package com.nimbusds.infinispan.persistence.dynamodb;


import com.codahale.metrics.MetricRegistry;
import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfigurationBuilder;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;


/**
 * Tests the DynamoDB store with a programmatic config.
 */
public class DynamoDBStoreWithExternalMetricRegistryTest extends TestWithDynamoDB {
	private static final String CACHE_NAME = "users";
	private final MetricRegistry metricRegistry = new MetricRegistry();
	private EmbeddedCacheManager cacheMgr;

	@Before
	@Override
	public void setUp()
		throws Exception {
		
		super.setUp();
		
		cacheMgr = new DefaultCacheManager();

		var b = new ConfigurationBuilder();
		b.persistence()
			.addStore(DynamoDBStoreConfigurationBuilder.class)
			.metricRegistry(metricRegistry)
			.endpoint(endpoint)
			.itemTransformerClass(UserItemTransformer.class)
			.segmented(false)
			.create();

		b.memory().create();

		cacheMgr.defineConfiguration(CACHE_NAME, b.build());

		cacheMgr.start();
	}


	@After
	@Override
	public void tearDown() throws Exception {
		
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		super.tearDown();
	}
	

	@Test
	public void testThatMetricsAreAttachedToTheMetricsRegistry() {
		cacheMgr.getCache(CACHE_NAME);
		// Check timers
		assertNotNull(metricRegistry.getTimers().get("users.dynamoDB.getTimer"));
		assertNotNull(metricRegistry.getTimers().get("users.dynamoDB.putTimer"));
		assertNotNull(metricRegistry.getTimers().get("users.dynamoDB.deleteTimer"));
		assertNotNull(metricRegistry.getTimers().get("users.dynamoDB.processTimer"));
		assertNotNull(metricRegistry.getTimers().get("users.dynamoDB.purgeTimer"));
	}
}