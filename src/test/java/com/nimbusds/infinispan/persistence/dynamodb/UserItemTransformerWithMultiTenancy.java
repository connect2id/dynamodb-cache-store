package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;

import com.nimbusds.infinispan.persistence.common.InfinispanEntry;


/**
 * Transforms User POJO to / from DynamoDB item.
 */
public class UserItemTransformerWithMultiTenancy extends UserItemTransformer {
	
	
	@Override
	public String getRangeKeyAttributeName() {
		return "tid";
	}
	
	
	@Override
	public PrimaryKeyValue resolvePrimaryKey(String key) {
		String[] parts = key.split(":");
		return new PrimaryKeyValue(parts[1], parts[0]);
	}
	
	
	@Override
	public Item toItem(InfinispanEntry<String, User> infinispanEntry) {
		
		Item item = super.toItem(infinispanEntry);
		
		PrimaryKeyValue pkValue = resolvePrimaryKey(infinispanEntry.getKey());
		
		return item
			.withPrimaryKey(
				new PrimaryKey("uid", pkValue.hashKeyValue(), "tid", pkValue.rangeKeyValue())
			);
	}
	
	
	@Override
	public InfinispanEntry<String, User> toInfinispanEntry(Item item) {
		
		InfinispanEntry<String, User> en = super.toInfinispanEntry(item);
		
		return new InfinispanEntry<>(
			item.getString("tid") + ":" + en.getKey(),
			en.getValue(),
			null);
	}
}
