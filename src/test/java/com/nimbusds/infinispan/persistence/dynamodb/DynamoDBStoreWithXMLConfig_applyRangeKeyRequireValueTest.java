package com.nimbusds.infinispan.persistence.dynamodb;


import org.infinispan.commons.CacheConfigurationException;
import org.infinispan.manager.DefaultCacheManager;
import org.junit.After;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;


public class DynamoDBStoreWithXMLConfig_applyRangeKeyRequireValueTest extends TestWithDynamoDB {
	
	
	@Override
	public void setUp()
		throws Exception {
		
		super.setUp();
	}
	
	
	@After
	@Override
	public void tearDown()
		throws Exception {
		
		super.tearDown();
	}
	

	@Test
	public void testRequireRangeKeyValue()
		throws IOException {
		
		CacheConfigurationException exception = null;
		try {
			new DefaultCacheManager("test-infinispan-apply-range-key-require-value.xml");
			fail();
		} catch (CacheConfigurationException e) {
			exception = e;
		}
		
		assertEquals("A range key value must be specified", exception.getMessage());
	}
}