package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.services.dynamodbv2.document.Item;
import com.nimbusds.infinispan.persistence.common.InternalMetadataBuilder;
import org.infinispan.metadata.InternalMetadata;
import org.junit.Assert;
import org.junit.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;


public class MetadataUtilsTest {


        @Test
        public void testAddAndParse() {
		
		var item = new Item();
		
		InternalMetadata metadata = new InternalMetadataBuilder()
			.created(100L)
			.lifespan(200L)
			.maxIdle(300L)
			.lastUsed(400L)
			.build();
		
		item = MetadataUtils.addMetadata(item, metadata);
		
		Assert.assertEquals(100L, item.getLong("iat"));
		Assert.assertEquals(200L, item.getLong("max"));
		Assert.assertEquals(300L, item.getLong("idl"));
		Assert.assertEquals(400L, item.getLong("lat"));
		Assert.assertEquals(4, item.numberOfAttributes());
		
		metadata = MetadataUtils.parseMetadata(item);
		
		Assert.assertEquals(100L, metadata.created());
		Assert.assertEquals(200L, metadata.lifespan());
		Assert.assertEquals(300L, metadata.maxIdle());
		Assert.assertEquals(400L, metadata.lastUsed());
	}


        @Test
        public void testAddAndParseWithTTL_noMaxIdle_noLastUsed() {
		
		var item = new Item();
		
		Instant now = Instant.now();
		
		InternalMetadata metadata = new InternalMetadataBuilder()
			.created(now.toEpochMilli())
			.lifespan(3600_000L)
			.build();
		
		item = MetadataUtils.addMetadata(item, metadata, true);
		
		Assert.assertEquals(now.toEpochMilli(), item.getLong("iat"));
		Assert.assertEquals(metadata.lifespan(), item.getLong("max"));
		Assert.assertEquals(now.plus(3600_000L, ChronoUnit.MILLIS).getEpochSecond(), item.getLong("ttl"));
		Assert.assertEquals(3, item.numberOfAttributes());
		
		metadata = MetadataUtils.parseMetadata(item);
		
		Assert.assertEquals(now.toEpochMilli(), metadata.created());
		Assert.assertEquals(3600_000L, metadata.lifespan());
		Assert.assertEquals(-1L, metadata.maxIdle());
		Assert.assertEquals(-1L, metadata.lastUsed());
	}


        @Test
        public void testAddAndParseWithTTL_noTTL_ifCreatedMissing() {
		
		var item = new Item();
		
		InternalMetadata metadata = new InternalMetadataBuilder()
			.lifespan(3600_000L)
			.build();
		
		item = MetadataUtils.addMetadata(item, metadata, true);
		
		Assert.assertEquals(metadata.lifespan(), item.getLong("max"));
		Assert.assertEquals(1, item.numberOfAttributes());
		
		metadata = MetadataUtils.parseMetadata(item);
		
		Assert.assertEquals(-1L, metadata.created());
		Assert.assertEquals(3600_000L, metadata.lifespan());
		Assert.assertEquals(-1L, metadata.maxIdle());
		Assert.assertEquals(-1L, metadata.lastUsed());
	}


        @Test
        public void testAddAndParseWithTTL_noTTL_ifLifespanMissing() {
		
		var item = new Item();
		
		Instant now = Instant.now();
		
		InternalMetadata metadata = new InternalMetadataBuilder()
			.created(now.toEpochMilli())
			.build();
		
		item = MetadataUtils.addMetadata(item, metadata, true);
		
		Assert.assertEquals(now.toEpochMilli(), item.getLong("iat"));
		Assert.assertEquals(1, item.numberOfAttributes());
		
		metadata = MetadataUtils.parseMetadata(item);
		
		Assert.assertEquals(now.toEpochMilli(), metadata.created());
		Assert.assertEquals(-1L, metadata.lifespan());
		Assert.assertEquals(-1L, metadata.maxIdle());
		Assert.assertEquals(-1L, metadata.lastUsed());
	}


        @Test
        public void testAddAndParse_allZero() {
		
		var item = new Item();
		
		InternalMetadata metadata = new InternalMetadataBuilder()
			.created(0L)
			.lifespan(0L)
			.maxIdle(0L)
			.lastUsed(0L)
			.build();
		
		item = MetadataUtils.addMetadata(item, metadata);
		
		Assert.assertEquals(0L, item.getLong("iat"));
		Assert.assertEquals(0L, item.getLong("max"));
		Assert.assertEquals(0L, item.getLong("idl"));
		Assert.assertEquals(0L, item.getLong("lat"));
		Assert.assertEquals(4, item.numberOfAttributes());
		
		metadata = MetadataUtils.parseMetadata(item);
		
		Assert.assertEquals(0L, metadata.created());
		Assert.assertEquals(0L, metadata.lifespan());
		Assert.assertEquals(0L, metadata.maxIdle());
		Assert.assertEquals(0L, metadata.lastUsed());
	}


        @Test
        public void testAddAndParse_allNegative() {
		
		var item = new Item();
		
		InternalMetadata metadata = new InternalMetadataBuilder()
			.created(-1L)
			.lifespan(-1L)
			.maxIdle(-1L)
			.lastUsed(-1L)
			.build();
		
		item = MetadataUtils.addMetadata(item, metadata);
		
		Assert.assertEquals(0, item.numberOfAttributes());
		
		metadata = MetadataUtils.parseMetadata(item);
		
		Assert.assertEquals(-1L, metadata.created());
		Assert.assertEquals(-1L, metadata.lifespan());
		Assert.assertEquals(-1L, metadata.maxIdle());
		Assert.assertEquals(-1L, metadata.lastUsed());
	}
}
