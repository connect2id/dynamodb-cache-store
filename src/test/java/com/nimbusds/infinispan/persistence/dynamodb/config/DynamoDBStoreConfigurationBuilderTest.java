package com.nimbusds.infinispan.persistence.dynamodb.config;


import static org.junit.Assert.*;

import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.cache.PersistenceConfigurationBuilder;
import org.junit.Test;


public class DynamoDBStoreConfigurationBuilderTest {
	

	@Test
	public void testCreateDefault() {
		
		PersistenceConfigurationBuilder persistenceConfigurationBuilder = new ConfigurationBuilder().persistence();
		
		var b = new DynamoDBStoreConfigurationBuilder(persistenceConfigurationBuilder);
		
		DynamoDBStoreConfiguration config = b.create();
		
		// Infinispan
		assertFalse(config.purgeOnStartup());
		assertFalse(config.fetchPersistentState());
		assertFalse(config.ignoreModifications());
		assertFalse(config.segmented());
		assertNotNull(config.async());
		assertFalse(config.async().enabled());
		assertFalse(config.preload());
		assertFalse(config.shared());
		assertNull(config.getEndpoint());
		assertNull(config.getMetricRegistry());
		assertNull(config.getRegion());
		assertNull(config.getItemTransformerClass());
		assertNull(config.getQueryExecutorClass());
		assertEquals(1L, config.getProvisionedThroughputForCreateTable().getReadCapacityUnits().longValue());
		assertEquals(1L, config.getProvisionedThroughputForCreateTable().getWriteCapacityUnits().longValue());
		assertEquals(10.0, config.getPurgeMaxReadCapacity().value(), 0.0);
		assertEquals(Capacity.Measure.PERCENT, config.getPurgeMaxReadCapacity().measure());
		assertFalse(config.isCreateTableWithEncryptionAtRest());
		assertEquals("", config.getTablePrefix());
		assertNull(config.getRangeKeyToApply());
		assertNull(config.getRangeKeyValue());
		assertFalse(config.isCreateTableWithStream());
		assertFalse(config.isEnableContinuousBackups());
		assertFalse(config.isEnableTTL());
		assertEquals(-1, config.getPurgeLimit());
		assertNull(config.getHTTPProxyHost());
		assertEquals(-1, config.getHTTPProxyPort());
	}
}
