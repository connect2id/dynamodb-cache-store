package com.nimbusds.infinispan.persistence.dynamodb;


import org.infinispan.Cache;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


public class DynamoDBStoreWithXMLConfig_applyRangeKeyEmptyTest extends TestWithDynamoDB {
	
	
	public static final String CACHE_NAME = "users";
	
	
	protected EmbeddedCacheManager cacheMgr;
	
	
	@Override
	public void setUp()
		throws Exception {
		
		super.setUp();

		cacheMgr = new DefaultCacheManager("test-infinispan-apply-range-key-empty.xml");

		cacheMgr.start();
	}
	
	
	@After
	@Override
	public void tearDown()
		throws Exception {
		
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		super.tearDown();
	}
	

	@Test
	public void testEmptyRangeKeyTreatedAsNull() {
		
		Cache<String,User> cache = cacheMgr.getCache(CACHE_NAME);
		
		@SuppressWarnings("unchecked")
		var store = (DynamoDBStore<String,User>) DynamoDBStore.getInstances().get(CACHE_NAME);
		
		assertNull(store.getConfiguration().getRangeKeyToApply());
		assertNull(store.getConfiguration().getRangeKeyValue());
		
		User u1 = new User("Alice Adams", "alice@wonderland.net");
		
		cache.put("u1", u1);
		
		assertEquals(u1.getEmail(), cache.get("u1").getEmail());
	}
}