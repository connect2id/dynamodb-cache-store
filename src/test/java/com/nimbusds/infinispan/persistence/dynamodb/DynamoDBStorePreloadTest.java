package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfigurationBuilder;
import org.infinispan.Cache;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.metadata.impl.InternalMetadataImpl;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


/**
 * Tests preloading of DynamoDB store.
 */
public class DynamoDBStorePreloadTest extends TestWithDynamoDB {
	
	
	public static final String CACHE_NAME = "users";


	protected EmbeddedCacheManager cacheMgr;
	

	@Test
	public void testPreload()
		throws InterruptedException {
		
		var tr = new UserItemTransformer();
		
		var rf = new RequestFactory<>(
			tr,
			null,
			false,
			new ProvisionedThroughput(1L, 1L),
			false,
                        false, "",
			null,
			null,
			false);
		
		Table table = dynamoDB.createTable(rf.resolveCreateTableRequest());
		
		table.waitForActive();
		
		final int preloadCount = 3000;
		
		for (int i=0; i < preloadCount; i++) {
			
			User user = new User("Alice Adams", "alice-" + i + "@example.com");
			Item item = tr.toItem(new InfinispanEntry<>(i + "", user, new InternalMetadataImpl()));
			table.putItem(item);
		}
		
		
		cacheMgr = new DefaultCacheManager();

		var b = new ConfigurationBuilder();
		b.persistence()
			.addStore(DynamoDBStoreConfigurationBuilder.class)
			.endpoint(endpoint)
			.itemTransformerClass(UserItemTransformer.class)
			.preload(true) // preload at startup!
			.segmented(false)
			.create();
		
		b.memory()
			.whenFull(EvictionStrategy.REMOVE)
			.maxCount(100)
			.create();

		cacheMgr.defineConfiguration(CACHE_NAME, b.build());

		cacheMgr.start();
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		for (int i=0; i < preloadCount; i++) {
			
			User user = myMap.get(i + "");
			assertEquals("Alice Adams", user.getName());
			assertEquals("alice-" + i + "@example.com", user.getEmail());
		}
		
		assertEquals(preloadCount, myMap.size());
	}


	@After
	@Override
	public void tearDown()
		throws Exception {
		
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		super.tearDown();
	}
}