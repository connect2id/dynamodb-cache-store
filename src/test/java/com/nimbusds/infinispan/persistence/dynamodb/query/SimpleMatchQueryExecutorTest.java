package com.nimbusds.infinispan.persistence.dynamodb.query;


import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.nimbusds.infinispan.persistence.common.query.MultiMatchQuery;
import com.nimbusds.infinispan.persistence.common.query.SimpleMatchQuery;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;


public class SimpleMatchQueryExecutorTest {


        @Test
        public void testInitNotNull() {
		
		try {
			new SimpleMatchQueryExecutor<String, String>().init(null);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The init context must not be null", e.getMessage());
		}
	}


        @Test
        public void testConvertToSimpleMatchQuery_cast() {
		
		var query = new SimpleMatchQuery<>("key", "value");
		
		assertEquals(query, SimpleMatchQueryExecutor.toSimpleMatchQuery(query));
	}


        @Test
        public void testConvertToSimpleMatchQuery_fromMultiMatchQuery() {
		
		Map<String, String> map = new HashMap<>();
		map.put("key", "value");
		var multiMatchQuery = new MultiMatchQuery<>(map);
		
		SimpleMatchQuery<String, String> simpleMatchQuery = SimpleMatchQueryExecutor.toSimpleMatchQuery(multiMatchQuery);
		
		assertEquals("key", simpleMatchQuery.getKey());
		assertEquals("value", simpleMatchQuery.getValue());
	}


        @Test
        public void testToQuerySpec() {
		
		var query = new SimpleMatchQuery<>("key-1", "value-1");
		QuerySpec querySpec = SimpleMatchQueryExecutor.toQuerySpec(query);
		assertEquals("#k = :value", querySpec.getKeyConditionExpression());
	}


        @Test
        public void testToScanSpec() {
		
		var query = new SimpleMatchQuery<>("key-1", "value-1");
		ScanSpec scanSpec = SimpleMatchQueryExecutor.toScanSpec(query);
		assertEquals("#k = :value", scanSpec.getFilterExpression());
		assertEquals("key-1", scanSpec.getNameMap().get("#k"));
		assertEquals(1, scanSpec.getNameMap().size());
		assertEquals("value-1", scanSpec.getValueMap().get(":value"));
		assertEquals(1, scanSpec.getValueMap().size());
		assertFalse(scanSpec.isConsistentRead());
	}
}
