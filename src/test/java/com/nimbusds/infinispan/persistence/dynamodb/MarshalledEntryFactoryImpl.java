package com.nimbusds.infinispan.persistence.dynamodb;


import org.infinispan.commons.io.ByteBuffer;
import org.infinispan.metadata.Metadata;
import org.infinispan.metadata.impl.PrivateMetadata;
import org.infinispan.persistence.spi.MarshallableEntry;
import org.infinispan.persistence.spi.MarshallableEntryFactory;
import org.infinispan.persistence.spi.MarshalledValue;


class MarshalledEntryFactoryImpl<K, V> implements MarshallableEntryFactory<K, V> {
	
	
	@Override
	public MarshallableEntry<K, V> create(ByteBuffer key, ByteBuffer valueBytes) {
		return null;
	}
	
	
	@Override
	public MarshallableEntry<K, V> create(ByteBuffer key, ByteBuffer valueBytes, ByteBuffer metadataBytes, ByteBuffer internalMetadataBytes, long created, long lastUsed) {
		return null;
	}
	
	
	@Override
	public MarshallableEntry<K, V> create(Object key, ByteBuffer valueBytes, ByteBuffer metadataBytes, ByteBuffer internalMetadataBytes, long created, long lastUsed) {
		return null;
	}
	
	
	@Override
	public MarshallableEntry<K, V> create(Object key) {
		return null;
	}
	
	
	@Override
	public MarshallableEntry<K, V> create(Object key, Object value) {
		return null;
	}
	
	
	@Override
	public MarshallableEntry<K, V> create(Object key, Object value, Metadata metadata, PrivateMetadata internalMetadata, long created, long lastUsed) {
		return new MarshallableEntry<>() {
			@Override
			public ByteBuffer getKeyBytes() {
				return null;
			}
			
			
			@Override
			public ByteBuffer getValueBytes() {
				return null;
			}
			
			
			@Override
			public ByteBuffer getMetadataBytes() {
				return null;
			}
			
			
			@Override
			public ByteBuffer getInternalMetadataBytes() {
				return null;
			}
			
			
			@Override
			public K getKey() {
				return (K) key;
			}
			
			
			@Override
			public V getValue() {
				return (V) value;
			}
			
			
			@Override
			public Metadata getMetadata() {
				return metadata;
			}
			
			
			@Override
			public PrivateMetadata getInternalMetadata() {
				return internalMetadata;
			}
			
			
			@Override
			public long created() {
				return created;
			}
			
			
			@Override
			public long lastUsed() {
				return lastUsed;
			}
			
			
			@Override
			public boolean isExpired(long now) {
				return false;
			}
			
			
			@Override
			public long expiryTime() {
				return 0;
			}
			
			
			@Override
			public MarshalledValue getMarshalledValue() {
				return null;
			}
		};
	}
	
	
	@Override
	public MarshallableEntry<K, V> create(Object key, MarshalledValue value) {
		return null;
	}
	
	
	@Override
	public MarshallableEntry<K, V> getEmpty() {
		return null;
	}
	
	
	@Override
	public MarshallableEntry<K, V> cloneWithExpiration(MarshallableEntry<K, V> me, long creationTime, long lifespan) {
		return null;
	}
}
