package com.nimbusds.infinispan.persistence.dynamodb.config;


import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.assertEquals;


public class DynamoDBStoreConfigurationParserTest {


        @Test
        public void testSplit() {
	
		assertEquals(Set.of("a", "b", "c"), DynamoDBStoreConfigurationParser_3_0.parseStringSet("a, b, c"));
		assertEquals(Set.of("a", "b", "c"), DynamoDBStoreConfigurationParser_3_0.parseStringSet("a,b,c"));
		assertEquals(Set.of("a", "b", "c"), DynamoDBStoreConfigurationParser_3_0.parseStringSet("a b c"));
		assertEquals(Set.of("a", "b", "c"), DynamoDBStoreConfigurationParser_3_0.parseStringSet("a , b,  c"));
	}
}
