package com.nimbusds.infinispan.persistence.dynamodb;


import com.nimbusds.infinispan.persistence.dynamodb.config.Capacity;
import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfiguration;
import org.infinispan.Cache;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.junit.After;
import org.junit.Test;

import java.time.Instant;
import java.util.Set;

import static org.junit.Assert.*;


public class DynamoDBStoreWithXMLConfig_overrideDefaultPurgeMaxReadCapacityTest extends TestWithDynamoDB {
	
	

	protected EmbeddedCacheManager cacheMgr;
	
	
	@Override
	public void setUp()
		throws Exception {

		System.setProperty("dynamodb.purgeMaxReadCapacity", "5");
		
		super.setUp();

		cacheMgr = new DefaultCacheManager("test-infinispan-purge-max-read-capacity-sys-prop.xml");

		cacheMgr.start();
	}
	
	
	@After
	@Override
	public void tearDown()
		throws Exception {

		System.clearProperty("dynamodb.purgeMaxReadCapacity");

		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		super.tearDown();
	}


	@Test
	public void testOverride() {

		Cache<String,User> cache = cacheMgr.getCache("users");

		var store = (DynamoDBStore) DynamoDBStore.getInstances().get("users");

		assertEquals(5.0, ((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration("users").persistence().stores().get(0)).getPurgeMaxReadCapacity().value(), 0.0);
		assertEquals(Capacity.Measure.ABSOLUTE, ((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration("users").persistence().stores().get(0)).getPurgeMaxReadCapacity().measure());

		assertTrue(cache.getAdvancedCache().getExpirationManager().isEnabled());

		final int itemCount = 10;

		String[] keys = new String[itemCount];

		final Instant created = Instant.ofEpochMilli(1474102000L);

		for (int i=0; i < itemCount; i ++) {

			String key = "a" + i;
			keys[i] = key;
			User value = new User("Alice " + i, "alice-" + i + "@email.com", created, Set.of("admin", "audit"));

			assertNull(cache.putIfAbsent(key, value));
		}
		
		assertEquals("Purge task never called", 0, store.getMeters().purgeTimer.getCount());

		cache.getAdvancedCache().getExpirationManager().processExpiration();

		assertEquals("Purge task called once", 1, store.getMeters().purgeTimer.getCount());
		long purgeTaskDuration = (long)store.getMeters().purgeTimer.getSnapshot().getMean() / 1_000_000;
		System.out.println("Purge task duration (s): " + purgeTaskDuration);

		assertEquals("Deletions metered", itemCount, store.getMeters().deleteTimer.getCount());
		System.out.println("Deletions mean rate: " + (long)store.getMeters().deleteTimer.getMeanRate());

		// Clear the in-memory entries so that only items in DynamoDB may remain
		cache.getAdvancedCache().getDataContainer().clear();

		for (int i=0; i < itemCount; i ++) {
			assertNull("Confirm deletion", cache.get(keys[i]));
		}
	}
}