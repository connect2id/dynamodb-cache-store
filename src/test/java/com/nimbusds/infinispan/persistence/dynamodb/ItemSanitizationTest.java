package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.services.dynamodbv2.document.Item;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.util.*;


public class ItemSanitizationTest {


        @Test
        public void testSanitizeItem_booleanTrueOk() {
		
		Item item = new Item().withBoolean("flag", true);
		
		item = ItemSanitization.sanitize(item);
		
		Assert.assertTrue(item.getBoolean("flag"));
		Assert.assertEquals(1, item.numberOfAttributes());
	}


        @Test
        public void testSanitizeItem_booleanFalseOk() {
		
		Item item = new Item().withBoolean("flag", false);
		
		item = ItemSanitization.sanitize(item);
		
		Assert.assertFalse(item.getBoolean("flag"));
		Assert.assertEquals(1, item.numberOfAttributes());
	}


        @Test
        public void testSanitizeItem_stringOk() {
		
		Item item = new Item().withString("name", "Alice");
		
		item = ItemSanitization.sanitize(item);
		
		Assert.assertEquals("Alice", item.getString("name"));
		Assert.assertEquals(1, item.numberOfAttributes());
	}


        @Test
        public void testSanitizeItem_byteArrayOk() {
		
		Item item = new Item().withBinary("data", new byte[]{0,1,2,3});
		
		item = ItemSanitization.sanitize(item);
		
		Assert.assertArrayEquals(new byte[]{0, 1, 2, 3}, item.getBinary("data"));
		Assert.assertEquals(1, item.numberOfAttributes());
	}


        @Test
        public void testSanitizeItem_byteBufferOk() {
		
		Item item = new Item().withBinary("data", ByteBuffer.wrap(new byte[]{0,1,2,3}));
		
		item = ItemSanitization.sanitize(item);
		
		Assert.assertArrayEquals(new byte[]{0, 1, 2, 3}, item.getByteBuffer("data").array());
		Assert.assertEquals(1, item.numberOfAttributes());
	}


        @Test
        public void testSanitizeItem_stringSetOk() {
		
		Item item = new Item().withStringSet("names", new HashSet<>(Arrays.asList("Alice", "Bob")));
		
		item = ItemSanitization.sanitize(item);
		
		Assert.assertEquals(new HashSet<>(Arrays.asList("Alice", "Bob")), item.getStringSet("names"));
		Assert.assertEquals(1, item.numberOfAttributes());
	}


        @Test
        public void testSanitizeItem_mapOk() {
		
		Map<String,Object> map = new HashMap<>();
		map.put("name", "Alice");
		map.put("emails", new HashSet<>(Arrays.asList("alice@example.com", "alice@example.org")));
		
		Item item = new Item().withMap("attrs", map);
		
		Assert.assertEquals("Alice", item.getMap("attrs").get("name"));
		Assert.assertEquals(new HashSet<>(Arrays.asList("alice@example.com", "alice@example.org")), item.getMap("attrs").get("emails"));
		Assert.assertEquals(2, item.getMap("attrs").size());
		Assert.assertEquals(1, item.numberOfAttributes());
	}


        @Test
        public void testSanitizeItem_removeEmptyString() {
		
		Item item = new Item().withString("name", "");
		
		item = ItemSanitization.sanitize(item);
		
		Assert.assertEquals(0, item.numberOfAttributes());
	}


        @Test
        public void testSanitizeItem_removeEmptyByteArray() {
		
		Item item = new Item().withBinary("data", new byte[]{});
		
		item = ItemSanitization.sanitize(item);
		
		Assert.assertEquals(0, item.numberOfAttributes());
	}


        @Test
        public void testSanitizeItem_removeEmptyByteBuffer() {
		
		Item item = new Item().withBinary("data", ByteBuffer.wrap(new byte[]{}));
		
		item = ItemSanitization.sanitize(item);
		
		Assert.assertEquals(0, item.numberOfAttributes());
	}


        @Test
        public void testSanitizeItem_removeEmptyString_removeEmptySet() {
		
		Item item = new Item()
			.withString("name", "")
			.withStringSet("names", new HashSet<>());
		
		item = ItemSanitization.sanitize(item);
		
		Assert.assertEquals(0, item.numberOfAttributes());
	}


        @Test
        public void testSanitizeItem_removeEmptyString_removeEmptySet_keepInteger() {
		
		Item item = new Item()
			.withInt("age", 21)
			.withString("name", "")
			.withStringSet("names", new HashSet<>());
		
		item = ItemSanitization.sanitize(item);
		
		Assert.assertEquals(21, item.getInt("age"));
		Assert.assertEquals(1, item.numberOfAttributes());
	}


        @Test
        public void testSanitizeItem_nestedMap_removeEmptyString_removeEmptySet() {
		
		Map<String,Object> map = new HashMap<>();
		map.put("name", "Alice");
		map.put("emails", new HashSet<>(Arrays.asList("alice@example.com", "alice@example.org")));
		map.put("address", "");
		map.put("locations", new HashSet<String>());
		
		Item item = new Item().withMap("attrs", map);
		
		item = ItemSanitization.sanitize(item);
		
		Assert.assertEquals("Alice", item.getMap("attrs").get("name"));
		Assert.assertEquals(new HashSet<>(Arrays.asList("alice@example.com", "alice@example.org")), item.getMap("attrs").get("emails"));
		Assert.assertEquals(2, item.getMap("attrs").size());
		Assert.assertEquals(1, item.numberOfAttributes());
	}


        @Test
        public void testWithBooleanTrueInJSONObject() {
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("flag", true);
		
		Item item = new Item().withJSON("json", jsonObject.toJSONString());
		
		Assert.assertEquals("{\"flag\":true}", item.getJSON("json"));
		
	}


        @Test
        public void testWithBooleanFalseInJSONObject() {
		
		var jsonObject = new JSONObject();
		jsonObject.put("flag", false);
		
		Item item = new Item().withJSON("json", jsonObject.toJSONString());
		
		item = ItemSanitization.sanitize(item);
		
		Assert.assertEquals("{\"flag\":false}", item.getJSON("json"));
	}


        @Test
        public void testFromJSONObject_sanitize() {
		String json = """
                        {
                         "name" : "",
                         "locations" : [],
                         "empty_data" : {},
                         "nested_data" : { "key-1" : "", "key-2" : [] }
                        }""";
		Item item = Item.fromJSON(json);
		item = ItemSanitization.sanitize(item);
		Assert.assertTrue(item.getMap("empty_data").isEmpty());
		Assert.assertTrue(item.getMap("nested_data").isEmpty());
		Assert.assertEquals(2, item.numberOfAttributes());
	}
	
	
	// Some c2id server use case
        @Test
        public void testRevocationEvent_wildCard() {
		
		var jsonArray = new JSONArray();
		jsonArray.add("*");
		jsonArray.add("*");
		jsonArray.add("*");
		Assert.assertEquals(3, jsonArray.size());
		
		Item in = new Item()
			.withPrimaryKey("r", jsonArray.toJSONString())
			.withLong("ts", new Date().getTime() / 1000);
		
		Item out = ItemSanitization.sanitize(in);
		
		Assert.assertEquals(out, in);
	}
	
	
	// Some c2id server use case
        @Test
        public void testRevocationEvent_emptyString() {
		
		var jsonArray = new JSONArray();
		jsonArray.add("");
		jsonArray.add("");
		jsonArray.add("");
		Assert.assertEquals(3, jsonArray.size());
		
		Item in = new Item()
			.withPrimaryKey("r", jsonArray.toJSONString())
			.withLong("ts", new Date().getTime() / 1000);
		
		Item out = ItemSanitization.sanitize(in);
		
		Assert.assertEquals(out, in);
	}
}
