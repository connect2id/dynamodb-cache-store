package com.nimbusds.infinispan.persistence.dynamodb;


import com.codahale.metrics.Metric;
import com.nimbusds.common.monitor.MonitorRegistries;
import com.nimbusds.infinispan.persistence.dynamodb.config.Capacity;
import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfiguration;
import org.infinispan.Cache;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.junit.After;
import org.junit.Test;

import java.time.Instant;
import java.util.Set;

import static org.junit.Assert.*;


public class DynamoDBStoreWithXMLConfig_purgeMaxReadCapacityPercentageTest extends TestWithDynamoDB {
	
	

	protected EmbeddedCacheManager cacheMgr;
	
	
	@Override
	public void setUp()
		throws Exception {
		
		super.setUp();

		cacheMgr = new DefaultCacheManager("test-infinispan-purge-max-read-capacity-percentage.xml");

		cacheMgr.start();
	}
	
	
	@After
	@Override
	public void tearDown()
		throws Exception {
		
		if (cacheMgr != null) {
			cacheMgr.stop();
		}

		MonitorRegistries.register("users.dynamoDB.getTimer", (Metric)null);
		MonitorRegistries.register("users.dynamoDB.putTimer", (Metric)null);
		MonitorRegistries.register("users.dynamoDB.deleteTimer", (Metric)null);
		MonitorRegistries.register("users.dynamoDB.processTimer", (Metric)null);
		MonitorRegistries.register("users.dynamoDB.purgeTimer", (Metric)null);
		
		super.tearDown();
	}

	@Test
	public void testWithCustomPurgeMaxReadCapacity() {

		Cache<String,User> cacheWithCap6 = cacheMgr.getCache("users");

		var store = (DynamoDBStore) DynamoDBStore.getInstances().get("users");

		assertEquals(50.0, ((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration("users").persistence().stores().get(0)).getPurgeMaxReadCapacity().value(), 0.0);
		assertEquals(Capacity.Measure.PERCENT, ((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration("users").persistence().stores().get(0)).getPurgeMaxReadCapacity().measure());

		assertTrue(cacheWithCap6.getAdvancedCache().getExpirationManager().isEnabled());

		final int itemCount = 100;

		String[] keys = new String[itemCount];

		final Instant created = Instant.ofEpochMilli(1474102000L);

		for (int i=0; i < itemCount; i ++) {

			String key = "a" + i;
			keys[i] = key;
			User value = new User("Alice " + i, "alice-" + i + "@email.com", created, Set.of("admin", "audit"));

			assertNull(cacheWithCap6.putIfAbsent(key, value));
		}
		
		assertEquals("Purge task never called", 0, store.getMeters().purgeTimer.getCount());

		cacheWithCap6.getAdvancedCache().getExpirationManager().processExpiration();

		assertEquals("Purge task called once", 1, store.getMeters().purgeTimer.getCount());
		long purgeTaskDuration = (long)store.getMeters().purgeTimer.getSnapshot().getMean() / 1_000_000;
		System.out.println("Purge task duration (s): " + purgeTaskDuration);
		assertTrue(1_500L < purgeTaskDuration && purgeTaskDuration < 2_500L);

		assertEquals("Deletions metered", itemCount, store.getMeters().deleteTimer.getCount());
		System.out.println("Deletions mean rate: " + (long)store.getMeters().deleteTimer.getMeanRate());

		// Clear the in-memory entries so that only items in DynamoDB may remain
		cacheWithCap6.getAdvancedCache().getDataContainer().clear();

		for (int i=0; i < itemCount; i ++) {
			assertNull("Confirm deletion", cacheWithCap6.get(keys[i]));
		}
	}
}