package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.services.dynamodbv2.model.*;
import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfiguration;
import org.infinispan.Cache;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.junit.After;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class DynamoDBStoreWithXMLConfig_continuousBackupsTest extends TestWithDynamoDB {
	
	
	public static final String CACHE_NAME = "users";
	
	
	protected EmbeddedCacheManager cacheMgr;
	
	
	@Override
	public void setUp()
		throws Exception {

		// Use AWS DynamoDB
		System.setProperty("dynamoDB.endpoint", "");
		
		super.setUp();

		cacheMgr = new DefaultCacheManager("test-infinispan-continuous-backups.xml");

		cacheMgr.start();
	}
	
	
	@After
	@Override
	public void tearDown()
		throws Exception {
		
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		super.tearDown();
	}
	

	@Test
	public void testWithContinuousBackups() {
		
		assertTrue(((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration("users").persistence().stores().get(0)).isEnableContinuousBackups());

		// Get cache
		Cache<String,User> cache = cacheMgr.getCache(CACHE_NAME);

		// Perform some op
		var u1 = new User("Alice Adams", "alice@wonderland.net");
		cache.put("u1", u1);
		assertEquals(u1.getEmail(), cache.get("u1").getEmail());

		// Verify PITR enabled for table
		assertEquals(
			ContinuousBackupsStatus.ENABLED,
			ContinuousBackupsStatus.fromValue(client.describeContinuousBackups(new DescribeContinuousBackupsRequest()
					.withTableName(new UserItemTransformer().getTableName()))
				.getContinuousBackupsDescription()
				.getContinuousBackupsStatus()
			)
		);
		assertEquals(
			PointInTimeRecoveryStatus.ENABLED,
			PointInTimeRecoveryStatus.fromValue(client.describeContinuousBackups(new DescribeContinuousBackupsRequest()
					.withTableName(new UserItemTransformer().getTableName()))
				.getContinuousBackupsDescription()
				.getPointInTimeRecoveryDescription()
				.getPointInTimeRecoveryStatus()
			)
		);
	}
}