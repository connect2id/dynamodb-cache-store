package com.nimbusds.infinispan.persistence.dynamodb;


import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfiguration;
import org.infinispan.Cache;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.*;


public class DynamoDBStoreWithXMLConfig_consistentReadsTest extends TestWithDynamoDB {
	
	
	public static final String CACHE_NAME = "users";
	
	
	protected EmbeddedCacheManager cacheMgr;
	
	
	@Override
	public void setUp()
		throws Exception {
		
		super.setUp();

		cacheMgr = new DefaultCacheManager("test-infinispan-consistent-reads.xml");

		cacheMgr.start();
	}
	
	
	@After
	@Override
	public void tearDown()
		throws Exception {
		
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		super.tearDown();
	}
	

	@Test
	public void testWithEnabledConsistentReads() {
		
		Cache<String,User> cache = cacheMgr.getCache(CACHE_NAME);
		
		var store = (DynamoDBStore) DynamoDBStore.getInstances().get(CACHE_NAME);
		assertNotNull(store);
		
		assertTrue(((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration(CACHE_NAME).persistence().stores().get(0)).useConsistentReads());
		
		var u1 = new User("Alice Adams", "alice@wonderland.net");
		
		cache.put("u1", u1);
		
		assertEquals(u1.getEmail(), cache.get("u1").getEmail());
	}
}