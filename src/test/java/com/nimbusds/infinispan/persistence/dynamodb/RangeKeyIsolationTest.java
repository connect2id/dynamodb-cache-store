package com.nimbusds.infinispan.persistence.dynamodb;


import java.util.Collection;
import java.util.LinkedList;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;
import com.amazonaws.services.dynamodbv2.model.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class RangeKeyIsolationTest extends TestWithDynamoDB {
	
	
	private static final String TABLE_NAME = "test_user";


	@Test
	public void testItemIsolation_identicalHashKey_differentRangeKey() {
		
		Collection<KeySchemaElement> keyAttrs = new LinkedList<>();
		keyAttrs.add(new KeySchemaElement("uid", KeyType.HASH));
		keyAttrs.add(new KeySchemaElement("tid", KeyType.RANGE));
		
		Collection<AttributeDefinition> attrs = new LinkedList<>();
		attrs.add(new AttributeDefinition("uid", ScalarAttributeType.S));
		attrs.add(new AttributeDefinition("tid", ScalarAttributeType.S));
		
		CreateTableRequest createTableRequest = new CreateTableRequest()
			.withTableName(TABLE_NAME)
			.withKeySchema(keyAttrs)
			.withAttributeDefinitions(attrs)
			.withProvisionedThroughput(new ProvisionedThroughput(1L, 1L));
		
		
		Table table = dynamoDB.createTable(createTableRequest);
		
		table.putItem(
			new Item()
			.withPrimaryKey("uid", "alice", "tid", "t1")
			.withString("name", "Alice Adams")
			.withString("context", "tenant-1")
		);
		
		table.putItem(
			new Item()
			.withPrimaryKey("uid", "alice", "tid", "t2")
			.withString("name", "Alice Adams")
			.withString("context", "tenant-2")
		);
		
		Item item = table.getItem(new GetItemSpec()
			.withPrimaryKey("uid", "alice", "tid", "t1")
		);
		assertEquals("Alice Adams", item.getString("name"));
		assertEquals("tenant-1", item.getString("context"));
		
		item = table.getItem(new GetItemSpec()
			.withPrimaryKey("uid", "alice", "tid", "t2")
		);
		assertEquals("Alice Adams", item.getString("name"));
		assertEquals("tenant-2", item.getString("context"));
	}
}
