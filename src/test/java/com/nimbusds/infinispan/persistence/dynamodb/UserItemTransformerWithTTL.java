package com.nimbusds.infinispan.persistence.dynamodb;


/**
 * Transforms User POJO to / from DynamoDB item.
 */
public class UserItemTransformerWithTTL extends UserItemTransformer {
	
	
	private boolean ttlEnabled = false;
	
	
	@Override
	public void init(final InitContext initContext) {
		ttlEnabled = initContext.isEnableTTL();
		System.out.println(getClass() + ": enable TTL: " + ttlEnabled);
	}
	
	
	@Override
	public String getTTLAttributeName() {
		return ttlEnabled ? "ttl" : null;
	}
}
