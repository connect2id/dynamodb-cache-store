package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.regions.Regions;
import com.nimbusds.infinispan.persistence.dynamodb.config.Capacity;
import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfiguration;

import org.infinispan.manager.DefaultCacheManager;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;


/**
 * Tests the DynamoDB store with an XML-based config.
 */
public class DynamoDBStoreWithXMLConfigTest extends DynamoDBStoreWithProgConfigTest {
	
	
	@Override
	public void setUp()
		throws Exception {
		
		super.setUp();

		cacheMgr = new DefaultCacheManager("test-infinispan.xml");

		cacheMgr.start();
		
		assertFalse(((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration("users").persistence().stores().get(0)).isCreateTableWithEncryptionAtRest());
	}
	
	
	// TODO disabled with localhost only testing
	@Test @Ignore
	public void _testWithRegionSpec()
		throws Exception {
		
		cacheMgr.stop();
		
		cacheMgr = new DefaultCacheManager("test-infinispan-with-region-spec.xml");
		
		cacheMgr.start();
		
		assertEquals(Regions.EU_CENTRAL_1, ((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration("users").persistence().stores().get(0)).getRegion());
	}
	

	@Test
	public void testWithEmptyRegionSpec()
		throws Exception {
		
		cacheMgr.stop();
		
		cacheMgr = new DefaultCacheManager("test-infinispan-with-empty-region-spec.xml");
		
		cacheMgr.start();
		
		assertNull(((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration("users").persistence().stores().get(0)).getRegion());
	}
	

	@Test
	public void testWithTablePrefix()
		throws Exception {
		
		cacheMgr.stop();
		
		cacheMgr = new DefaultCacheManager("test-infinispan-table-prefix.xml");
		
		cacheMgr.start();
		
		assertEquals("myapp_", ((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration("users").persistence().stores().get(0)).getTablePrefix());
	}
	

	@Test
	public void testWithEncryptionAtRest()
		throws Exception {
		
		cacheMgr.stop();
		
		cacheMgr = new DefaultCacheManager("test-infinispan-encryption-at-rest.xml");
		
		cacheMgr.start();
		
		assertTrue(((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration("users").persistence().stores().get(0)).isCreateTableWithEncryptionAtRest());
	}


	@Test
	public void testDefaultPurgeMaxReadCapacity()
		throws Exception {

		cacheMgr.stop();

		cacheMgr = new DefaultCacheManager("test-infinispan.xml");

		cacheMgr.start();

		assertEquals(10.0, ((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration("users").persistence().stores().get(0)).getPurgeMaxReadCapacity().value(), 0.0);
		assertEquals(Capacity.Measure.PERCENT, ((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration("users").persistence().stores().get(0)).getPurgeMaxReadCapacity().measure());
	}
}