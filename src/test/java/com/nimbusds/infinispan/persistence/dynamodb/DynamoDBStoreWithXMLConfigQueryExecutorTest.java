package com.nimbusds.infinispan.persistence.dynamodb;


import org.infinispan.manager.DefaultCacheManager;


public class DynamoDBStoreWithXMLConfigQueryExecutorTest extends DynamoDBStoreWithProgConfigQueryExecutorTest {
	
	
	@Override
	public void setUp()
		throws Exception {
		
		setUpLocalClient();
		
		cacheMgr = new DefaultCacheManager("test-infinispan-query-enabled.xml");
		
		cacheMgr.start();
	}
}
