package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.nimbusds.common.monitor.MonitorRegistries;
import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.common.util.InfinispanUtils;
import com.nimbusds.infinispan.persistence.dynamodb.config.Capacity;
import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfiguration;
import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfigurationBuilder;
import org.infinispan.Cache;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.context.Flag;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.persistence.spi.AdvancedCacheExpirationWriter;
import org.infinispan.persistence.spi.MarshallableEntry;
import org.infinispan.persistence.spi.PersistenceException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.time.Instant;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static org.junit.Assert.*;


/**
 * Tests the DynamoDB store with a programmatic config.
 */
public class DynamoDBStoreWithProgConfigTest extends TestWithDynamoDB {
	
	
	public static final String CACHE_NAME = "users";


	protected EmbeddedCacheManager cacheMgr;
	

	@Before
	@Override
	public void setUp()
		throws Exception {
		
		super.setUp();
		
		cacheMgr = new DefaultCacheManager();

		var b = new ConfigurationBuilder();
		b.persistence()
			.addStore(DynamoDBStoreConfigurationBuilder.class)
			.endpoint(endpoint)
			.itemTransformerClass(UserItemTransformer.class)
			.segmented(false)
			.create();
		
		b.memory()
			.whenFull(EvictionStrategy.REMOVE)
			.maxCount(100)
			.create();

		cacheMgr.defineConfiguration(CACHE_NAME, b.build());

		cacheMgr.start();
	}


	@After
	@Override
	public void tearDown()
		throws Exception {
		
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		super.tearDown();
	}


	@Test
	public void testConfiguration() {

		cacheMgr.getCache(CACHE_NAME);

		var store = (DynamoDBStore) DynamoDBStore.getInstances().get(CACHE_NAME);
		assertNotNull(store);

		assertEquals(60_000, cacheMgr.getCacheConfiguration(CACHE_NAME).expiration().wakeUpInterval());
		assertTrue(cacheMgr.getCacheConfiguration(CACHE_NAME).expiration().reaperEnabled());

		var config = (DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration(CACHE_NAME).persistence().stores().get(0);

		assertEquals("http://localhost:8000", config.getEndpoint());
		assertNull(config.getRegion());
		assertEquals(UserItemTransformer.class, config.getItemTransformerClass());
		assertNull(config.getQueryExecutorClass());
		assertNull(config.getIndexAttributes());
		assertFalse(config.useConsistentReads());
		assertEquals(1L, config.getProvisionedThroughputForCreateTable().getReadCapacityUnits().longValue());
		assertEquals(1L, config.getProvisionedThroughputForCreateTable().getWriteCapacityUnits().longValue());
		assertEquals(10.0, config.getPurgeMaxReadCapacity().value(), 0.0);
		assertEquals(Capacity.Measure.PERCENT, config.getPurgeMaxReadCapacity().measure());
		assertFalse(config.isCreateTableWithEncryptionAtRest());
		assertEquals("", config.getTablePrefix());
		assertNull(config.getMetricRegistry());
		assertNull(config.getRangeKeyToApply());
		assertFalse(config.isCreateTableWithStream());
		assertFalse(config.isEnableContinuousBackups());
		assertFalse(config.isEnableTTL());
		assertEquals(-1L, config.getPurgeLimit());
		assertNull(config.getHTTPProxyHost());
		assertEquals(-1, config.getHTTPProxyPort());
		assertNull(config.getHMACSHA256Key());
	}
	

	@Test
	public void testGetTable() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		@SuppressWarnings("unchecked")
		DynamoDBStore<String,User> dynamoDBStore = (DynamoDBStore<String,User>)DynamoDBStore.getInstances().get(CACHE_NAME);
		Table table = dynamoDBStore.getTable();
		assertEquals("test-users", table.getTableName());
		
		Instant created = Instant.ofEpochMilli(1474012953000L);
		User u1 = new User("Alice Adams", "alice@wonderland.net", created, new HashSet<>(Arrays.asList("admin", "audit")));
		
		Item item = dynamoDBStore.getItemTransformer().toItem(new InfinispanEntry<>("u1", u1, null));
		
		table.putItem(item);
		
		assertEquals(u1.getEmail(), myMap.get("u1").getEmail());
	}


	@Test
	public void testSimpleObjectLifeCycle() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		assertNotNull(DynamoDBStore.getInstances().get(CACHE_NAME));
		assertEquals(1, DynamoDBStore.getInstances().size());
		
		// Initial size
		assertEquals(0, myMap.size());
		
		// Get non-existing object
		assertNull(myMap.get("invalid-key"));
		
		// Store new object with all fields defined
		Instant created = Instant.ofEpochMilli(1474012953000L);
		User u1 = new User("Alice Adams", "alice@wonderland.net", created, new HashSet<>(Arrays.asList("admin", "audit")));
		assertNull(myMap.putIfAbsent("u1", u1));
		
		// Check presence
		assertTrue(myMap.containsKey("u1"));
		
		
		// Confirm put
		Item item = dynamoDB.getTable("test-users").getItem("uid", "u1");
		assertEquals("u1", item.get("uid"));
		assertEquals("Alice",  item.get("given_name"));
		assertEquals("Adams",  item.get("surname"));
		assertEquals("alice@wonderland.net",  item.get("email"));
		assertEquals(created, Instant.ofEpochMilli(item.getLong("created")));
		assertEquals(u1.getPermissions(), item.getStringSet("permissions"));
		assertEquals(6, item.numberOfAttributes());
	
		
		// Get new count
		assertEquals(1, myMap.size());
		
		// Get object
		User out = myMap.get("u1");
		assertEquals(u1.getName(), out.getName());
		assertEquals(u1.getEmail(), out.getEmail());
		assertEquals(u1.getCreated(), out.getCreated());
		assertEquals(u1.getPermissions(), out.getPermissions());
		
		// Update object
		User u2 = new User("Bob Brown", "bob@wonderland.net", created, new HashSet<>(Arrays.asList("browse", "pay")));
		assertNotNull(myMap.replace("u1", u2));
		
		// Confirm put
		item = dynamoDB.getTable("test-users").getItem("uid", "u1");
		assertEquals("u1", item.get("uid"));
		assertEquals("Bob",  item.get("given_name"));
		assertEquals("Brown",  item.get("surname"));
		assertEquals(u2.getEmail(),  item.get("email"));
		assertEquals(created, Instant.ofEpochMilli(item.getLong("created")));
		assertEquals(u2.getPermissions(), item.getStringSet("permissions"));
		assertEquals(6, item.numberOfAttributes());
		
		// Get object
		out = myMap.get("u1");
		assertEquals(u2.getName(), out.getName());
		assertEquals(u2.getEmail(), out.getEmail());
		assertEquals(u2.getCreated(), out.getCreated());
		assertEquals(u2.getPermissions(), out.getPermissions());
		
		// Update object with fewer defined fields
		User u3 = new User("Claire Cox", "claire@wonderland.net");
		assertNotNull(myMap.replace("u1", u3));
		
		// Confirm DynamoDB update
		item = dynamoDB.getTable("test-users").getItem("uid", "u1");
		assertEquals("u1", item.get("uid"));
		assertEquals("Claire",  item.get("given_name"));
		assertEquals("Cox",  item.get("surname"));
		assertEquals("claire@wonderland.net",  item.get("email"));
		assertNull(item.get("created")); // Timestamp
		assertNull(item.get("permissions"));
		assertEquals(4, item.numberOfAttributes());
		
		// Get object
		out = myMap.get("u1");
		assertEquals(u3.getName(), out.getName());
		assertEquals(u3.getEmail(), out.getEmail());
		assertNull(out.getCreated());
		assertNull(out.getPermissions());
		
		
		// Remove object
		out = myMap.remove("u1");
		assertEquals(u3.getName(), out.getName());
		assertEquals(u3.getEmail(), out.getEmail());
		assertNull(out.getCreated());
		assertNull(out.getPermissions());
		
		// Confirm removal
		assertNull(myMap.get("u1"));
		assertFalse(myMap.containsKey("u1"));
		
		// Confirm delete
		assertNull(dynamoDB.getTable("test-users").getItem("uid", "u1"));
		
		// Zero count
		assertEquals(0, myMap.size());
		assertEquals(0, myMap.getAdvancedCache().size());
		assertEquals(0, myMap.getAdvancedCache().getDataContainer().size());
		
		// Check timers
		assertEquals(4L, MonitorRegistries.getMetricRegistry().getTimers().get("users.dynamoDB.getTimer").getCount());
		assertEquals(3L, MonitorRegistries.getMetricRegistry().getTimers().get("users.dynamoDB.putTimer").getCount());
		assertEquals(1L, MonitorRegistries.getMetricRegistry().getTimers().get("users.dynamoDB.deleteTimer").getCount());
//		assertEquals(4L, MonitorRegistries.getMetricRegistry().getTimers().get("users.dynamoDB.processTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("users.dynamoDB.purgeTimer").getCount());
	}
	

	@Test
	public void testMultiplePutIfAbsentWithEviction() {
		
		Cache<String, User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		assertEquals(100, myMap.getCacheConfiguration().memory().maxCount());
		assertEquals(EvictionStrategy.REMOVE, myMap.getCacheConfiguration().memory().whenFull());
		assertEquals(60000L, myMap.getCacheConfiguration().expiration().wakeUpInterval());
		
		String[] keys = new String[200];
		
		final Instant created = Instant.ofEpochMilli(1474102000L);
		
		for (int i=0; i < 200; i ++) {
			
			String key = "a" + i;
			keys[i] = key;
			User value = new User("Alice Adams-" + i, "alice-" + i + "@email.com", created, new HashSet<>(Arrays.asList("admin", "audit")));
			
			assertNull(myMap.putIfAbsent(key, value));
		}
		
		// Confirm put
		for (int i=0; i < 200; i ++) {
			
			Item item = dynamoDB.getTable("test-users").getItem("uid", keys[i]);
			assertEquals(keys[i], item.get("uid"));
			assertEquals("Alice",  item.get("given_name"));
			assertEquals("Adams-" + i,  item.get("surname"));
			assertEquals("alice-"+i+"@email.com",  item.get("email"));
			assertEquals(created.toEpochMilli(), item.getLong("created"));
			assertEquals(new HashSet<>(Arrays.asList("admin", "audit")), item.getStringSet("permissions"));
			assertEquals(6, item.numberOfAttributes());
		}
		
		// Calls AdvancedCacheLoader.process, result matches eviction size
		assertEquals(100, myMap.getAdvancedCache().getDataContainer().size());
		
		// Calls AdvancedCacheLoader.process, result equals total persisted
		assertEquals(200, myMap.size());
		
		// Check presence
		for (int i=0; i < 200; i ++) {
			assertTrue(myMap.containsKey(keys[i]));
		}
		
		assertEquals(200, myMap.size());
		
		// Evict entries from memory, store unaffected
		for (String uuid: keys) {
			myMap.evict(uuid);
		}
		
		assertEquals(200, myMap.size());
		
		// Recheck presence, loads data from store
		for (int i=0; i < 200; i ++) {
			assertTrue(myMap.containsKey(keys[i]));
		}
		
		// Confirm presence
		for (int i=0; i < 200; i ++) {
			
			Item item = dynamoDB.getTable("test-users").getItem("uid", keys[i]);
			assertEquals(keys[i], item.get("uid"));
			assertEquals("Alice",  item.get("given_name"));
			assertEquals("Adams-" + i,  item.get("surname"));
			assertEquals("alice-"+i+"@email.com",  item.get("email"));
			assertEquals(created.toEpochMilli(), item.getLong("created"));
			assertEquals(new HashSet<>(Arrays.asList("admin", "audit")), item.getStringSet("permissions"));
			assertEquals(6, item.numberOfAttributes());
		}
		
		// Evict entries from memory, DynamoDB store unaffected
		for (String uuid: keys) {
			myMap.evict(uuid);
		}
		
		assertEquals(200, myMap.size());
		
		// Iterate and count, calls AdvancedCacheLoader.process
		AtomicInteger counter = new AtomicInteger();
		myMap.keySet().iterator().forEachRemaining(uuid -> counter.incrementAndGet());
		assertEquals(200, counter.get());
		
		// Remove from cache and store
		for (int i=0; i < 200; i ++) {
			assertNotNull(myMap.remove(keys[i]));
		}
		
		assertEquals(0, myMap.size());
		
		// Confirm DynamoDB deletion
		for (int i=0; i < 200; i ++) {
			
			assertNull(dynamoDB.getTable("test-users").getItem("uid", keys[i]));
		}
		
		// Confirm deletion
		for (int i=0; i < 200; i ++) {
			
			assertNull(myMap.get(keys[0]));
		}
	}
	

	@Test
	public void testPutWithMissingTable() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		client.deleteTable("test-users");
		
		try {
			myMap.put("alice", new User("Alice Adams", "alice@name.com"));
			fail();
		} catch (PersistenceException e) {
			assertTrue(e.getCause() instanceof ResourceNotFoundException);
			assertTrue(e.getMessage().startsWith("Cannot do operations on a non-existent table"));
		}
	}
	

	@Test
	public void testPutAsyncWithMissingTable()
		throws Exception {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		client.deleteTable("test-users");
		
		// No exception thrown here, instead in future
		CompletableFuture<User> future = myMap.putAsync("alice", new User("Alice Adams", "alice@name.com"));
		
		while (! future.isDone()) {
			TimeUnit.MILLISECONDS.sleep(10);
		}
		
		try {
			future.get();
			fail();
		} catch (ExecutionException e) {
			assertTrue(e.getCause() instanceof PersistenceException);
			assertTrue(e.getCause().getCause() instanceof ResourceNotFoundException);
			assertTrue(e.getMessage().contains("Cannot do operations on a non-existent table"));
		}
		
		assertEquals(0, myMap.getAdvancedCache().withFlags(Flag.SKIP_CACHE_LOAD).size());
	}
	

	@Test
	public void testKeyFilterProcess() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		// Initial state empty
		Map<String,User> filteredMap = myMap
			.entrySet()
			.stream()
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		
		assertTrue(filteredMap.isEmpty());
		
		// Put entry
		String k1 = "alice";
		User u1 = new User("Alice Adams", "alice@name.com");
		myMap.put(k1, u1);
		
		// Confirm put
		assertEquals(u1.getName(), myMap.get(k1).getName());
		assertEquals(u1.getEmail(), myMap.get(k1).getEmail());
		
		// Filter all
		filteredMap = myMap
			.entrySet()
			.stream()
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		
		assertEquals(u1.getName(), filteredMap.get(k1).getName());
		assertEquals(u1.getEmail(), filteredMap.get(k1).getEmail());
		assertEquals(1, filteredMap.size());
		
		// Put another entry
		String k2 = "bob";
		User u2 = new User("Bob Brown", "bob@name.com");
		myMap.put(k2, u2);
		
		// Filter on name
		filteredMap = myMap
			.entrySet()
			.stream()
			.filter(uuidUserEntry -> uuidUserEntry.getValue().getName().equals("Bob Brown"))
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		
		assertEquals(u2.getName(), filteredMap.get(k2).getName());
		assertEquals(u2.getEmail(), filteredMap.get(k2).getEmail());
		assertEquals(1, filteredMap.size());
		
		// Filter to include all
		filteredMap = myMap
			.entrySet()
			.stream()
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		
		assertEquals(u1.getName(), filteredMap.get(k1).getName());
		assertEquals(u1.getEmail(), filteredMap.get(k1).getEmail());
		assertEquals(u2.getName(), filteredMap.get(k2).getName());
		assertEquals(u2.getEmail(), filteredMap.get(k2).getEmail());
		assertEquals(2, filteredMap.size());
		
		// Check timers
		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get("users.dynamoDB.getTimer").getCount());
		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get("users.dynamoDB.putTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("users.dynamoDB.deleteTimer").getCount());
		assertEquals(4L, MonitorRegistries.getMetricRegistry().getTimers().get("users.dynamoDB.processTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("users.dynamoDB.purgeTimer").getCount());
	}
	

	@Test
	public void testRepeatPut() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		String k1 = "alice";
		User u1 = new User("Alice Adams", "alice@name.com");
		
		// First put
		myMap.put(k1, u1);
		
		assertEquals(u1.getName(), myMap.get(k1).getName());
		assertEquals(u1.getEmail(), myMap.get(k1).getEmail());
		
		// Repeat put
		myMap.put(k1, u1);
		
		assertEquals(u1.getName(), myMap.get(k1).getName());
		assertEquals(u1.getEmail(), myMap.get(k1).getEmail());
	}
	

	@Test
	public void testPutExpiring() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		String k1 = "alice";
		User u1 = new User("Alice Adams", "alice@name.com");
		
		myMap.put(k1, u1, 60L, TimeUnit.HOURS);
		
		assertEquals(u1.getName(), myMap.get(k1).getName());
		assertEquals(u1.getEmail(), myMap.get(k1).getEmail());
		
		myMap.put(k1, u1, 60L, TimeUnit.HOURS);
	}
	
	
	public void testExists() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		String k1 = "alice";
		User u1 = new User("Alice Adams", "alice@name.com");
		
		myMap.put(k1, u1);
		
		assertTrue(DynamoDBStore.getInstances().get(CACHE_NAME).contains(k1));
		
		assertFalse(DynamoDBStore.getInstances().get(CACHE_NAME).contains("no-such-key"));
	}
	

	@Test
	public void testPurgeNonExpiring() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		String[] keys = new String[200];
		
		for (int i=0; i < 200; i ++) {
			
			String key = "a" + i;
			keys[i] = key;
			User value = new User("Alice Adams-" + i, "alice-" + i + "@email.com");
			
			assertNull(myMap.putIfAbsent(key, value));
		}
		
		// Nothing to parse with regular user records
		
		DynamoDBStore.getInstances().get(CACHE_NAME).purge(
			Executors.newSingleThreadExecutor(),
			new AdvancedCacheExpirationWriter.ExpirationPurgeListener() {
				@Override
				public void marshalledEntryPurged(MarshallableEntry entry) {
				
				}
				
				
				@Override
				public void entryPurged(Object o) {
					fail();
				}
		});
		
		assertEquals(200, myMap.size());
	}
	

	@Test
	public void testSize() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		assertEquals(0, DynamoDBStore.getInstances().get(CACHE_NAME).size());
		
		String[] keys = new String[200];
		
		for (int i=0; i < 200; i ++) {
			
			String key = "a" + i;
			keys[i] = key;
			User value = new User("Alice Adams-" + i, "alice-" + i + "@email.com");
			
			assertNull(myMap.putIfAbsent(key, value));
		}
		
		assertEquals(200, DynamoDBStore.getInstances().get(CACHE_NAME).size());
		
		myMap.clear();
		
		assertEquals(0, DynamoDBStore.getInstances().get(CACHE_NAME).size());
	}
	

	@Test
	public void testMetrics() {
		
		cacheMgr.getCache(CACHE_NAME);
		
		MetricRegistry metricRegistry = MonitorRegistries.getMetricRegistry();
		
		Map<String,Timer> timers = metricRegistry.getTimers();
		assertTrue(timers.containsKey(CACHE_NAME+".dynamoDB.getTimer"));
		assertTrue(timers.containsKey(CACHE_NAME+".dynamoDB.putTimer"));
		assertTrue(timers.containsKey(CACHE_NAME+".dynamoDB.deleteTimer"));
		assertTrue(timers.containsKey(CACHE_NAME+".dynamoDB.processTimer"));
		assertTrue(timers.containsKey(CACHE_NAME+".dynamoDB.purgeTimer"));
		assertEquals(5, timers.size());
	}
	

	@Test
	public void testExpirationNone()
		throws Exception {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		assertEquals(60_000L, myMap.getCacheConfiguration().expiration().wakeUpInterval());
		
		myMap.getAdvancedCache().getExpirationManager().processExpiration();
		
		Thread.sleep(500);
	}
	

	@Test
	public void testDirectWriteWithNullMetadata() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		@SuppressWarnings("unchecked")
		DynamoDBStore<String,User> dynamoDBStore = (DynamoDBStore<String,User>)DynamoDBStore.getInstances().get(CACHE_NAME);
		
		var key = "u1";
		var u1 = new User("Alice Adams", "alice@wonderland.net");
		
		dynamoDBStore.write(InfinispanUtils.toMarshallableEntry(key, u1));
		
		assertEquals(u1, myMap.get(key));
		
		MarshallableEntry<String, User> entry = dynamoDBStore.loadEntry(key);
		
		assertEquals(key, entry.getKey());
		assertEquals(u1, entry.getValue());
		assertNull(entry.getMetadata());
	}
}