package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.services.dynamodbv2.model.DescribeTimeToLiveRequest;
import com.amazonaws.services.dynamodbv2.model.TimeToLiveStatus;
import org.infinispan.Cache;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.junit.After;

import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfiguration;
import org.junit.Ignore;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;


public class DynamoDBStoreWithXMLConfig_ttlTest extends TestWithDynamoDB {
	
	
	public static final String CACHE_NAME = "users";
	
	
	protected EmbeddedCacheManager cacheMgr;
	
	
	@Override
	public void setUp()
		throws Exception {

		// Use AWS DynamoDB
		System.setProperty("dynamoDB.endpoint", "");
		
		super.setUp();

		cacheMgr = new DefaultCacheManager("test-infinispan-ttl.xml");

		cacheMgr.start();
	}
	
	
	@After
	@Override
	public void tearDown()
		throws Exception {
		
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		super.tearDown();
	}
	
	
	@Test
	public void testWithEnabledTTL() throws InterruptedException {
		
		Cache<String,User> cache = cacheMgr.getCache(CACHE_NAME);
		
		var store = (DynamoDBStore) DynamoDBStore.getInstances().get(CACHE_NAME);
		assertNotNull(store);

		assertEquals(
			TimeToLiveStatus.ENABLED,
			TimeToLiveStatus.fromValue(client.describeTimeToLive(
					new DescribeTimeToLiveRequest().withTableName(new UserItemTransformer().getTableName()))
				.getTimeToLiveDescription()
				.getTimeToLiveStatus()
			)
		);
		
		assertTrue(((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration(CACHE_NAME).persistence().stores().get(0)).isEnableTTL());
		
		assertEquals(0L, cacheMgr.getCacheConfiguration(CACHE_NAME).expiration().wakeUpInterval());
		assertTrue(cacheMgr.getCacheConfiguration(CACHE_NAME).expiration().reaperEnabled());
		
		assertEquals(100, cacheMgr.getCacheConfiguration(CACHE_NAME).memory().maxCount());
		assertEquals(EvictionStrategy.REMOVE, cacheMgr.getCacheConfiguration(CACHE_NAME).memory().whenFull());
		
		assertFalse(cache.getAdvancedCache().getExpirationManager().isEnabled());
		
		cache.put("u1", new User("Alice Adams", "alice@wonderland.net"));

		TimeUnit.MINUTES.sleep(10);
		
		assertEquals("Purge task not called", 0, store.getMeters().purgeTimer.getCount());
		
		assertEquals("alice@wonderland.net", cache.get("u1").getEmail());
	}
}