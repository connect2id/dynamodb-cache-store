package com.nimbusds.infinispan.persistence.dynamodb;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.model.ConsumedCapacity;
import org.junit.Test;

import java.util.Iterator;

import static org.junit.Assert.assertEquals;

public class PageOfItemsTest {


        @Test
        public void testConstructor() {

                var it = new Iterator<Item>() {
                        @Override
                        public boolean hasNext() {
                                return false;
                        }

                        @Override
                        public Item next() {
                                return null;
                        }
                };

                var capacity = new ConsumedCapacity();

                var page = new PageOfItems(it, capacity);

                assertEquals(it, page.itemIterator());
                assertEquals(capacity, page.consumedCapacity());
        }
}
