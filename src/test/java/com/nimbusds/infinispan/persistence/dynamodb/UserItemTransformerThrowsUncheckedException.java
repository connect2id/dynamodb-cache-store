package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.services.dynamodbv2.document.Item;
import com.nimbusds.infinispan.persistence.common.InfinispanEntry;


public class UserItemTransformerThrowsUncheckedException extends UserItemTransformer {

	@Override
	public InfinispanEntry<String,User> toInfinispanEntry(final Item item) {
		
		throw new RuntimeException("Unchecked exception");
	}
}