package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;


/**
 * The base class for tests that require a DynamoDB database.
 */
public class TestWithDynamoDB {
	
	
	protected String endpoint;
	protected AmazonDynamoDB client;
	protected DynamoDB dynamoDB;
	
	
	protected void setUpLocalClient() {

		try {
			var testProps = new Properties();
			testProps.load(new FileInputStream("test.properties"));

			testProps.putAll(System.getProperties());

			endpoint = testProps.getProperty("dynamoDB.endpoint");

		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		// Create a DynamoDB client
		AmazonDynamoDBClientBuilder builder = AmazonDynamoDBClientBuilder
			.standard()
			.withCredentials(DefaultAWSCredentialsProviderChain.getInstance());

		if (StringUtils.isNotBlank(endpoint)) {
			builder = builder.withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endpoint, null));
		}

		client = builder.build();

		dynamoDB = new DynamoDB(client);
	}
	
	
	@Before
	public void setUp()
		throws Exception {
		
		setUpLocalClient();
	}
	
	
	@After
	public void tearDown()
		throws Exception {

		System.clearProperty("dynamoDB.endpoint");
		
		if (dynamoDB != null) {
			
			List<String> tableNames = client.listTables().getTableNames();
			
			for (String tableName: tableNames) {
				
				if (! tableName.contains("test")) {
					return;
				}
				
				Table table = dynamoDB.getTable(tableName);
				table.delete();
				table.waitForDelete();
			}
			
			dynamoDB.shutdown();
		}
	}
}
