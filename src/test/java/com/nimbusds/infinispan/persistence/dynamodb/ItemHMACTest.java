package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.services.dynamodbv2.document.Item;
import org.junit.Assert;
import org.junit.Test;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

import static org.junit.Assert.*;


public class ItemHMACTest {
	
	
	private static final byte[] HMAC_SHA256_KEY_BYTES = new byte[256 / 8];
	
	static {
		new SecureRandom().nextBytes(HMAC_SHA256_KEY_BYTES);
	}
	
	
	private static final String HMAC_SHA256_KEY_BASE64_ENCODED = Base64.getEncoder().encodeToString(HMAC_SHA256_KEY_BYTES);
	
	
	private static final Item SAMPLE_ITEM = new Item()
		.withString("key", "9cea8d2d-197f-41d6-a0c3-643b64746915")
		.withString("name", "Alice Adams")
		.withNumber("age", 21);


        @Test
        public void testLifeCycle_disabled()
		throws InvalidKeyException, NoSuchAlgorithmException {
		
		var itemHMAC = new ItemHMAC((String) null);
		
		assertEquals(SAMPLE_ITEM, itemHMAC.apply(SAMPLE_ITEM));
	}


        @Test
        public void testKeyShorterThan256Bits() {
		
		byte[] keyBytes = new byte[(256 / 8) - 1];
		new SecureRandom().nextBytes(keyBytes);
		
		try {
			new ItemHMAC(keyBytes);
			fail();
		} catch (InvalidKeyException e) {
			assertEquals("The HMAC SHA-256 key must be at least 256 bits long", e.getMessage());
		}
	}


        @Test
        public void testLifeCycle_enabled()
		throws InvalidKeyException, NoSuchAlgorithmException, InvalidHMACException {
	
		System.out.println("HMAC SHA-256 key: " + HMAC_SHA256_KEY_BASE64_ENCODED);
		
		var itemHMAC = new ItemHMAC(HMAC_SHA256_KEY_BASE64_ENCODED);
		
		Item out = itemHMAC.apply(SAMPLE_ITEM);
		
		assertTrue(out.hasAttribute(ItemHMAC.ATTRIBUTE_NAME));
		
		itemHMAC.verify(out);
	}


        @Test
        public void testLifeCycle_enabled_missingHMACAttribute()
		throws InvalidKeyException, NoSuchAlgorithmException {
	
		var itemHMAC = new ItemHMAC(HMAC_SHA256_KEY_BASE64_ENCODED);
		
		try {
			itemHMAC.verify(SAMPLE_ITEM);
			fail();
		} catch (InvalidHMACException e) {
			assertEquals("Missing item HMAC attribute: _hmac#s256", e.getMessage());
		}
	}


        @Test
        public void testLifeCycle_enabled_invalidHMACException()
		throws InvalidKeyException, NoSuchAlgorithmException {
	
		var itemHMAC = new ItemHMAC(HMAC_SHA256_KEY_BASE64_ENCODED);
		
		Item out = itemHMAC.apply(SAMPLE_ITEM);
		
		assertTrue(out.hasAttribute(ItemHMAC.ATTRIBUTE_NAME));
		
		byte[] badHMAC = new byte[256 / 8];
		new SecureRandom().nextBytes(badHMAC);
		
		out = out.removeAttribute(ItemHMAC.ATTRIBUTE_NAME);
		out = out.withBinary(ItemHMAC.ATTRIBUTE_NAME, badHMAC);
		
		try {
			itemHMAC.verify(out);
			fail();
		} catch (InvalidHMACException e) {
			assertTrue(e.getMessage().startsWith("Invalid item HMAC: "));
		}
	}
}
