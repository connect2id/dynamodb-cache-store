package com.nimbusds.infinispan.persistence.dynamodb;


import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import org.infinispan.Cache;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.context.Flag;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.junit.After;
import org.junit.Before;

import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfigurationBuilder;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


public class DynamoDBStorePurgeLimitTest extends TestWithDynamoDB {
	
	
	
	public static final String CACHE_NAME = "users";
	
	
	private static final int PURGE_LIMIT = 150;
	
	
	protected EmbeddedCacheManager cacheMgr;
	
	
	@Before
	@Override
	public void setUp()
		throws Exception {
		
		super.setUp();
		
		cacheMgr = new DefaultCacheManager();
		
		var b = new ConfigurationBuilder();
		b.persistence()
			.addStore(DynamoDBStoreConfigurationBuilder.class)
			.endpoint(endpoint)
			.itemTransformerClass(ExpiringUserItemTransformer.class)
			.purgeLimit(PURGE_LIMIT)
			.segmented(false)
			.create();
		
		b.memory()
			.whenFull(EvictionStrategy.REMOVE)
			.maxCount(10)
			.create();
		
		cacheMgr.defineConfiguration(CACHE_NAME, b.build());
		
		cacheMgr.start();
	}
	
	
	@After
	@Override
	public void tearDown()
		throws Exception {
		
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		super.tearDown();
	}
	

	@Test
	public void testExpirationWithLimit150()
		throws Exception {
		
		Cache<String, User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		assertEquals(10, myMap.getCacheConfiguration().memory().maxCount());
		assertEquals(EvictionStrategy.REMOVE, myMap.getCacheConfiguration().memory().whenFull());
		assertEquals(60000L, myMap.getCacheConfiguration().expiration().wakeUpInterval());
		assertEquals(150, ((DynamoDBStore)DynamoDBStore.getInstances().get(myMap.getName())).getConfiguration().getPurgeLimit());
		
		String[] keys = new String[300];
		
		Instant created = Instant.now().minus(1L, ChronoUnit.DAYS);
		
		for (int i=0; i < keys.length; i ++) {
			
			String key = "a" + i;
			keys[i] = key;
			User value = new User("Alice Adams-" + i, "alice-" + i + "@email.com", created, new HashSet<>(Arrays.asList("admin", "audit")));
			
			assertNull(myMap.putIfAbsent(key, value));
		}
		
		assertEquals(10, myMap.getAdvancedCache().withFlags(Flag.SKIP_CACHE_LOAD).size());
		assertEquals(10, myMap.size());
	
		// reap expired entries
		myMap.getAdvancedCache().getExpirationManager().processExpiration();
		
		Thread.sleep(2000);
		
		Table table = ((DynamoDBStore)DynamoDBStore.getInstances().get(myMap.getName())).getTable();
		
		Set<String> keysFoundAfterExpiration = new HashSet<>();
		table.scan(new ScanSpec()).forEach(
			item -> keysFoundAfterExpiration.add(item.getString("uid"))
		);

		// Actual purge limit is modulo page size
		assertEquals(100, keysFoundAfterExpiration.size());
		assertEquals(10, myMap.size()); // only in-memory entries remain
	}
}
