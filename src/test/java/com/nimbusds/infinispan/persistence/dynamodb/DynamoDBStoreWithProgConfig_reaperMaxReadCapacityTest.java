package com.nimbusds.infinispan.persistence.dynamodb;


import com.nimbusds.infinispan.persistence.dynamodb.config.Capacity;
import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfiguration;
import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfigurationBuilder;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


/**
 * Tests the DynamoDB store with a programmatic config.
 */
public class DynamoDBStoreWithProgConfig_reaperMaxReadCapacityTest extends TestWithDynamoDB {
	
	
	public static final String CACHE_NAME = "users";


	protected EmbeddedCacheManager cacheMgr;
	

	@Before
	@Override
	public void setUp()
		throws Exception {
		
		super.setUp();
		
		cacheMgr = new DefaultCacheManager();

		var b = new ConfigurationBuilder();
		
		b.persistence()
			.addStore(DynamoDBStoreConfigurationBuilder.class)
			.endpoint(endpoint)
			.itemTransformerClass(UserItemTransformer.class)
			.purgeMaxReadCapacity(new Capacity(5.0, Capacity.Measure.ABSOLUTE))
			.segmented(false)
			.create();
		
		b.memory()
			.whenFull(EvictionStrategy.REMOVE)
			.maxCount(100)
			.create();

		cacheMgr.defineConfiguration(CACHE_NAME, b.build());

		cacheMgr.start();
	}


	@After
	@Override
	public void tearDown()
		throws Exception {
		
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		super.tearDown();
	}
	

	@Test
	public void testPurgeMaxReadCapacitySetup() throws InterruptedException {

		cacheMgr.getCache(CACHE_NAME);
		
		var store = (DynamoDBStore) DynamoDBStore.getInstances().get(CACHE_NAME);
		assertNotNull(store);

		assertEquals(60_000, cacheMgr.getCacheConfiguration(CACHE_NAME).expiration().wakeUpInterval());
		assertTrue(cacheMgr.getCacheConfiguration(CACHE_NAME).expiration().reaperEnabled());

		assertEquals(5.0, ((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration(CACHE_NAME).persistence().stores().get(0)).getPurgeMaxReadCapacity().value(), 0.0);
		assertEquals(Capacity.Measure.ABSOLUTE, ((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration(CACHE_NAME).persistence().stores().get(0)).getPurgeMaxReadCapacity().measure());

		assertEquals(100, cacheMgr.getCacheConfiguration(CACHE_NAME).memory().size());
		assertEquals(EvictionStrategy.REMOVE, cacheMgr.getCacheConfiguration(CACHE_NAME).memory().evictionStrategy());
		
		assertTrue(cacheMgr.getCache(CACHE_NAME).getAdvancedCache().getExpirationManager().isEnabled());
		
		// Purge explicitly, async
		cacheMgr.getCache(CACHE_NAME).getAdvancedCache().getExpirationManager().processExpiration();
		Thread.sleep(500);
		assertEquals(1, store.getMeters().purgeTimer.getCount());
	}
}