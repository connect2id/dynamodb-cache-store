package com.nimbusds.infinispan.persistence.dynamodb;


import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.UpdateTableRequest;
import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfiguration;
import org.infinispan.Cache;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.*;


public class DynamoDBStoreWithXMLConfig_deletionProtectionTest extends TestWithDynamoDB {
	
	
	public static final String CACHE_NAME = "users";
	
	
	protected EmbeddedCacheManager cacheMgr;
	
	
	@Override
	public void setUp()
		throws Exception {

		// Use AWS DynamoDB
		System.setProperty("dynamoDB.endpoint", "");
		
		super.setUp();
	}
	
	
	@After
	@Override
	public void tearDown()
		throws Exception {
		
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		super.tearDown();
	}
	

	@Test
	public void testEnableDeletionProtectionForNewTable() throws Exception {

		cacheMgr = new DefaultCacheManager("test-infinispan-deletion-protection.xml");

		cacheMgr.start();

		assertTrue(((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration("users").persistence().stores().get(0)).isEnableDeletionProtection());

		final var tableName = "deletion-protected-" + new UserItemTransformer().getTableName();
		Table table = dynamoDB.getTable(tableName);

		assertTrue(table.describe().getDeletionProtectionEnabled());

		// Get cache
		Cache<String,User> cache = cacheMgr.getCache(CACHE_NAME);

		// Perform some op
		var u1 = new User("Alice Adams", "alice@wonderland.net");
		cache.put("u1", u1);
		assertEquals(u1.getEmail(), cache.get("u1").getEmail());

		// Try to delete
		try {
			table.delete();
			fail();
		} catch (AmazonServiceException ase) {
			assertNotNull(ase.getMessage());
		}

		// Lift deletion protection
		client.updateTable(
			new UpdateTableRequest()
				.withTableName(table.getTableName())
				.withDeletionProtectionEnabled(false)
		);

		assertFalse(table.describe().getDeletionProtectionEnabled());

		// Delete success
		table.delete();
		table.waitForDelete();
	}


	@Test
	public void testEnableDeletionProtectionForExistingTable() throws Exception {

		cacheMgr = new DefaultCacheManager("test-infinispan-deletion-protection-not-enabled.xml");

		cacheMgr.start();

		assertFalse(((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration("users").persistence().stores().get(0)).isEnableDeletionProtection());

		final var tableName = "deletion-protected-" + new UserItemTransformer().getTableName();
		Table table = dynamoDB.getTable(tableName);

		assertFalse(table.describe().getDeletionProtectionEnabled());

		// Get cache
		Cache<String,User> cache = cacheMgr.getCache(CACHE_NAME);

		// Perform some op
		var u1 = new User("Alice Adams", "alice@wonderland.net");
		cache.put("u1", u1);
		assertEquals(u1.getEmail(), cache.get("u1").getEmail());

		// Restart
		cacheMgr.stop();

		cacheMgr = new DefaultCacheManager("test-infinispan-deletion-protection.xml");

		cacheMgr.start();

		assertTrue(((DynamoDBStoreConfiguration)cacheMgr.getCacheConfiguration("users").persistence().stores().get(0)).isEnableDeletionProtection());

		table = dynamoDB.getTable(tableName);

		assertTrue(table.describe().getDeletionProtectionEnabled());

		// Get cache
		cache = cacheMgr.getCache(CACHE_NAME);

		// Perform some op
		var u2 = new User("Bob Brown", "bob@wonderland.net");
		cache.put("u2", u2);
		assertEquals(u2.getEmail(), cache.get("u2").getEmail());

		// Try to delete
		try {
			table.delete();
			fail();
		} catch (AmazonServiceException ase) {
			assertNotNull(ase.getMessage());
		}

		// Lift deletion protection
		client.updateTable(
			new UpdateTableRequest()
				.withTableName(table.getTableName())
				.withDeletionProtectionEnabled(false)
		);

		// Delete success
		table.delete();
		table.waitForDelete();
	}
}