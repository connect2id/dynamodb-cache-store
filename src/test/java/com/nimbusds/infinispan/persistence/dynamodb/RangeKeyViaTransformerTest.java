package com.nimbusds.infinispan.persistence.dynamodb;


import java.time.Instant;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.amazonaws.services.dynamodbv2.document.Item;
import org.infinispan.Cache;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.eviction.EvictionStrategy;
import org.infinispan.eviction.EvictionType;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.persistence.spi.AdvancedCacheExpirationWriter;
import org.infinispan.persistence.spi.MarshallableEntry;
import org.junit.After;
import org.junit.Before;

import com.nimbusds.common.monitor.MonitorRegistries;
import com.nimbusds.infinispan.persistence.common.InfinispanEntry;
import com.nimbusds.infinispan.persistence.common.query.QueryExecutor;
import com.nimbusds.infinispan.persistence.common.query.SimpleMatchQuery;
import com.nimbusds.infinispan.persistence.dynamodb.config.DynamoDBStoreConfigurationBuilder;
import com.nimbusds.infinispan.persistence.dynamodb.query.SimpleMatchQueryExecutor;
import org.junit.Test;

import static org.junit.Assert.*;


public class RangeKeyViaTransformerTest extends TestWithDynamoDB {
	
	
	public static final String CACHE_NAME = "users";
	
	
	protected EmbeddedCacheManager cacheMgr;
	
	
	@Before
	@Override
	public void setUp()
		throws Exception {
		
		super.setUp();
		
		cacheMgr = new DefaultCacheManager();
		
		var b = new ConfigurationBuilder();
		b.persistence()
			.addStore(DynamoDBStoreConfigurationBuilder.class)
			.endpoint(endpoint)
			.itemTransformerClass(UserItemTransformerWithMultiTenancy.class)
			.queryExecutorClass(SimpleMatchQueryExecutor.class)
			.indexAttributes(Collections.singleton("email"))
			.applyRangeKey("range-key-overridden") // overridden by transformer
			.rangeKeyValue("123") // overridden by transformer
			.segmented(false)
			.create();
		
		b.memory()
			.evictionStrategy(EvictionStrategy.REMOVE)
			.size(100)
			.evictionType(EvictionType.COUNT)
			.create();
		
		cacheMgr.defineConfiguration(CACHE_NAME, b.build());
		
		cacheMgr.start();
	}
	
	
	@After
	@Override
	public void tearDown()
		throws Exception {
		
		if (cacheMgr != null) {
			cacheMgr.stop();
		}
		
		super.tearDown();
	}
	

	@Test
	public void testSimpleObjectLifeCycleWithHashKeyIsolation() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		assertNotNull(DynamoDBStore.getInstances().get(CACHE_NAME));
		assertEquals(1, DynamoDBStore.getInstances().size());
		
		List<String> tenants = Arrays.asList("t1", "t2");
		
		// Initial size
		assertEquals(0, myMap.size());
		
		// Get non-existing object
		for (String t: tenants) {
			assertNull(myMap.get(t + ":invalid-key"));
			assertNull(myMap.get(t + ":invalid-key"));
		}
		
		// Store new object with all fields defined
		Instant created = Instant.ofEpochMilli(1474012953000L);
		Set<String> permissions = new HashSet<>(Arrays.asList("admin", "audit"));
		Map<String,User> tenantUsers = new HashMap<>();
		for (String t: tenants) {
			User user = new User("Alice Adams", "alice@" + t + ".net", created, permissions);
			tenantUsers.put(t, user);
			assertNull(myMap.putIfAbsent(t + ":u1", user));
		}
		
		// Check presence
		for (String t: tenants) {
			assertTrue(myMap.containsKey(t + ":u1"));
		}
		
		
		// Confirm put
		for (String t: tenants) {
			Item item = dynamoDB.getTable("test-users").getItem("uid", "u1", "tid", t);
			assertEquals("u1", item.get("uid"));
			assertEquals(t, item.get("tid"));
			assertEquals("Alice", item.get("given_name"));
			assertEquals("Adams", item.get("surname"));
			assertEquals(tenantUsers.get(t).getEmail(), item.get("email"));
			assertEquals(created, Instant.ofEpochMilli(item.getLong("created")));
			assertEquals(permissions, item.getStringSet("permissions"));
			assertEquals(7, item.numberOfAttributes());
		}
		
		// Query
		final List<InfinispanEntry<String,User>> queryResult = new LinkedList<>();
		
		QueryExecutor<String,User> queryExecutor = DynamoDBStore.getInstances().get(CACHE_NAME).getQueryExecutor();
		
		
		for (String t: tenants) {
			queryExecutor.executeQuery(new SimpleMatchQuery<>("email", tenantUsers.get(t).getEmail()), queryResult::add);
			
			assertEquals(t + ":u1", queryResult.get(0).getKey());
			assertEquals(tenantUsers.get(t).getName(), queryResult.get(0).getValue().getName());
			assertEquals(tenantUsers.get(t).getEmail(), queryResult.get(0).getValue().getEmail());
			assertEquals(tenantUsers.get(t).getCreated(), queryResult.get(0).getValue().getCreated());
			assertEquals(tenantUsers.get(t).getPermissions(), queryResult.get(0).getValue().getPermissions());
			assertEquals(1, queryResult.size());
			
			queryResult.clear();
		}
		
		
		// Get new count
		assertEquals(2, myMap.size());
		
		// Get object
		for (String t: tenants) {
			User out = myMap.get(t + ":u1");
			assertEquals(tenantUsers.get(t).getName(), out.getName());
			assertEquals(tenantUsers.get(t).getEmail(), out.getEmail());
			assertEquals(tenantUsers.get(t).getCreated(), out.getCreated());
			assertEquals(tenantUsers.get(t).getPermissions(), out.getPermissions());
		}
		
		// Update object
		permissions = new HashSet<>(Arrays.asList("browse", "pay"));
		for (String t: tenants) {
			User user = new User("Bob Brown", "bob@" + t + ".net", created, permissions);
			tenantUsers.put(t, user);
			assertNotNull(myMap.replace(t + ":u1", user));
		}
		
		// Confirm put
		for (String t: tenants) {
			Item item = dynamoDB.getTable("test-users").getItem("uid", "u1", "tid", t);
			assertEquals("u1", item.get("uid"));
			assertEquals(t, item.get("tid"));
			assertEquals("Bob", item.get("given_name"));
			assertEquals("Brown", item.get("surname"));
			assertEquals(tenantUsers.get(t).getEmail(), item.get("email"));
			assertEquals(created, Instant.ofEpochMilli(item.getLong("created")));
			assertEquals(permissions, item.getStringSet("permissions"));
			assertEquals(7, item.numberOfAttributes());
		}
		
		// Get object
		for (String t: tenants) {
			User out = myMap.get(t + ":u1");
			assertEquals(tenantUsers.get(t).getName(), out.getName());
			assertEquals(tenantUsers.get(t).getEmail(), out.getEmail());
			assertEquals(tenantUsers.get(t).getCreated(), out.getCreated());
			assertEquals(tenantUsers.get(t).getPermissions(), out.getPermissions());
		}
		
		// Query
		for (String t: tenants) {
			queryExecutor.executeQuery(new SimpleMatchQuery<>("email", "bob@" + t + ".net"), queryResult::add);
			
			assertEquals(t + ":u1", queryResult.get(0).getKey());
			assertEquals(tenantUsers.get(t).getName(), queryResult.get(0).getValue().getName());
			assertEquals(tenantUsers.get(t).getEmail(), queryResult.get(0).getValue().getEmail());
			assertEquals(tenantUsers.get(t).getCreated(), queryResult.get(0).getValue().getCreated());
			assertEquals(tenantUsers.get(t).getPermissions(), queryResult.get(0).getValue().getPermissions());
			assertEquals(1, queryResult.size());
			
			queryResult.clear();
		}
		
		// Update object with fewer defined fields
		for (String t: tenants) {
			User user = new User("Claire Cox", "claire@" + t + ".net");
			tenantUsers.put(t, user);
			assertNotNull(myMap.replace(t + ":u1", user));
		}
		
		// Confirm DynamoDB update
		for (String t: tenants) {
			Item item = dynamoDB.getTable("test-users").getItem("uid", "u1", "tid", t);
			assertEquals("u1", item.get("uid"));
			assertEquals(t, item.get("tid"));
			assertEquals("Claire", item.get("given_name"));
			assertEquals("Cox", item.get("surname"));
			assertEquals(tenantUsers.get(t).getEmail(), item.get("email"));
			assertNull(item.get("created")); // Timestamp
			assertNull(item.get("permissions"));
			assertEquals(5, item.numberOfAttributes());
		}
		
		// Get object
		for (String t: tenants) {
			User out = myMap.get(t + ":u1");
			assertEquals(tenantUsers.get(t).getName(), out.getName());
			assertEquals(tenantUsers.get(t).getEmail(), out.getEmail());
			assertNull(out.getCreated());
			assertNull(out.getPermissions());
		}
		
		
		// Remove object
		for (String t: tenants) {
			User out = myMap.remove(t + ":u1");
			assertEquals(tenantUsers.get(t).getName(), out.getName());
			assertEquals(tenantUsers.get(t).getEmail(), out.getEmail());
			assertNull(out.getCreated());
			assertNull(out.getPermissions());
		}
		
		// Confirm removal
		for (String t: tenants) {
			assertNull(myMap.get(t + ":u1"));
			assertFalse(myMap.containsKey(t + ":u1"));
		}
		
		// Confirm delete
		for (String t: tenants) {
			assertNull(dynamoDB.getTable("test-users").getItem("uid", "u1", "tid", t));
		}
		
		// Query
		queryExecutor.executeQuery(new SimpleMatchQuery<>("email", "alice@wonderland.net"), queryResult::add);
		queryExecutor.executeQuery(new SimpleMatchQuery<>("email", "bob@wonderland.net"), queryResult::add);
		queryExecutor.executeQuery(new SimpleMatchQuery<>("email", "claire@wonderland.net"), queryResult::add);
		assertEquals(0, queryResult.size());
		
		// Zero count
		assertEquals(0, myMap.size());
		assertEquals(0, myMap.getAdvancedCache().size());
		assertEquals(0, myMap.getAdvancedCache().getDataContainer().size());
		
		// Check timers
		assertEquals(10L, MonitorRegistries.getMetricRegistry().getTimers().get("users.dynamoDB.getTimer").getCount());
		assertEquals(6L, MonitorRegistries.getMetricRegistry().getTimers().get("users.dynamoDB.putTimer").getCount());
		assertEquals(2L, MonitorRegistries.getMetricRegistry().getTimers().get("users.dynamoDB.deleteTimer").getCount());
		assertEquals(4L, MonitorRegistries.getMetricRegistry().getTimers().get("users.dynamoDB.processTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("users.dynamoDB.purgeTimer").getCount());
	}
	

	@Test
	public void testMultiplePutIfAbsentWithEviction() {
		
		Cache<String, User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		assertEquals(100, myMap.getCacheConfiguration().memory().size());
		assertEquals(EvictionType.COUNT, myMap.getCacheConfiguration().memory().evictionType());
		assertEquals(60000L, myMap.getCacheConfiguration().expiration().wakeUpInterval());
		
		String[] keys = new String[200];
		
		final Instant created = Instant.ofEpochMilli(1474102000L);
		
		for (int i=0; i < 200; i ++) {
			
			String key = "t1:a" + i;
			keys[i] = key;
			User value = new User("Alice Adams-" + i, "alice-" + i + "@email.com", created, new HashSet<>(Arrays.asList("admin", "audit")));
			
			assertNull(myMap.putIfAbsent(key, value));
		}
		
		// Confirm put
		for (int i=0; i < 200; i ++) {
			
			Item item = dynamoDB.getTable("test-users").getItem("uid", "a"+i, "tid", "t1");
			assertEquals("a" + i, item.get("uid"));
			assertEquals("t1", item.get("tid"));
			assertEquals("Alice",  item.get("given_name"));
			assertEquals("Adams-" + i,  item.get("surname"));
			assertEquals("alice-"+i+"@email.com",  item.get("email"));
			assertEquals(created.toEpochMilli(), item.getLong("created"));
			assertEquals(new HashSet<>(Arrays.asList("admin", "audit")), item.getStringSet("permissions"));
			assertEquals(7, item.numberOfAttributes());
		}
		
		// Calls AdvancedCacheLoader.process, result matches eviction size
		assertEquals(100, myMap.getAdvancedCache().getDataContainer().size());
		
		// Calls AdvancedCacheLoader.process, result equals total persisted
		assertEquals(200, myMap.size());
		
		// Check presence
		for (int i=0; i < 200; i ++) {
			assertTrue(myMap.containsKey(keys[i]));
		}
		
		assertEquals(200, myMap.size());
		
		// Evict entries from memory, store unaffected
		for (String uuid: keys) {
			myMap.evict(uuid);
		}
		
		assertEquals(200, myMap.size());
		
		// Recheck presence, loads data from store
		for (int i=0; i < 200; i ++) {
			assertTrue(myMap.containsKey(keys[i]));
		}
		
		// Confirm presence
		for (int i=0; i < 200; i ++) {
			
			Item item = dynamoDB.getTable("test-users").getItem("uid", "a" + i, "tid", "t1");
			assertEquals("a" + i, item.get("uid"));
			assertEquals("t1", item.get("tid"));
			assertEquals("Alice",  item.get("given_name"));
			assertEquals("Adams-" + i,  item.get("surname"));
			assertEquals("alice-"+i+"@email.com",  item.get("email"));
			assertEquals(created.toEpochMilli(), item.getLong("created"));
			assertEquals(new HashSet<>(Arrays.asList("admin", "audit")), item.getStringSet("permissions"));
			assertEquals(7, item.numberOfAttributes());
		}
		
		// Evict entries from memory, DynamoDB store unaffected
		for (String uuid: keys) {
			myMap.evict(uuid);
		}
		
		assertEquals(200, myMap.size());
		
		// Iterate and count, calls AdvancedCacheLoader.process
		AtomicInteger counter = new AtomicInteger();
		myMap.keySet().iterator().forEachRemaining(uuid -> counter.incrementAndGet());
		assertEquals(200, counter.get());
		
		// Remove from cache and store
		for (int i=0; i < 200; i ++) {
			assertNotNull(myMap.remove(keys[i]));
		}
		
		assertEquals(0, myMap.size());
		
		// Confirm DynamoDB deletion
		for (int i=0; i < 200; i ++) {
			
			assertNull(dynamoDB.getTable("test-users").getItem("uid", "a" + i, "tid", "t1"));
		}
		
		// Confirm deletion
		for (int i=0; i < 200; i ++) {
			
			assertNull(myMap.get(keys[0]));
		}
	}
	

	@Test
	public void testKeyFilterProcess() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		// Initial state empty
		Map<String,User> filteredMap = myMap
			.entrySet()
			.stream()
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		
		assertTrue(filteredMap.isEmpty());
		
		// Put entry
		String k1 = "t1:alice";
		User u1 = new User("Alice Adams", "alice@name.com");
		myMap.put(k1, u1);
		
		// Confirm put
		assertEquals(u1.getName(), myMap.get(k1).getName());
		assertEquals(u1.getEmail(), myMap.get(k1).getEmail());
		
		// Filter all
		filteredMap = myMap
			.entrySet()
			.stream()
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		
		assertEquals(u1.getName(), filteredMap.get(k1).getName());
		assertEquals(u1.getEmail(), filteredMap.get(k1).getEmail());
		assertEquals(1, filteredMap.size());
		
		// Put another entry
		String k2 = "t1:bob";
		User u2 = new User("Bob Brown", "bob@name.com");
		myMap.put(k2, u2);
		
		// Put entry with matching hash key, different range key, to test isolation
		String k3 = "t2:bob";
		User u3 = new User("Bob Blake", "bella@name.com");
		myMap.put(k3, u3);
		
		// Filter on name
		filteredMap = myMap
			.entrySet()
			.stream()
			.filter(uuidUserEntry -> uuidUserEntry.getValue().getName().equals("Bob Brown"))
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		
		assertEquals(u2.getName(), filteredMap.get(k2).getName());
		assertEquals(u2.getEmail(), filteredMap.get(k2).getEmail());
		assertEquals(1, filteredMap.size());
		
		// Filter to include all
		filteredMap = myMap
			.entrySet()
			.stream()
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		
		assertEquals(u1.getName(), filteredMap.get(k1).getName());
		assertEquals(u1.getEmail(), filteredMap.get(k1).getEmail());
		assertEquals(u2.getName(), filteredMap.get(k2).getName());
		assertEquals(u2.getEmail(), filteredMap.get(k2).getEmail());
		assertEquals(u3.getName(), filteredMap.get(k3).getName());
		assertEquals(u3.getEmail(), filteredMap.get(k3).getEmail());
		assertEquals(3, filteredMap.size());
		
		// Check timers
		assertEquals(3L, MonitorRegistries.getMetricRegistry().getTimers().get("users.dynamoDB.getTimer").getCount());
		assertEquals(3L, MonitorRegistries.getMetricRegistry().getTimers().get("users.dynamoDB.putTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("users.dynamoDB.deleteTimer").getCount());
		assertEquals(4L, MonitorRegistries.getMetricRegistry().getTimers().get("users.dynamoDB.processTimer").getCount());
		assertEquals(0L, MonitorRegistries.getMetricRegistry().getTimers().get("users.dynamoDB.purgeTimer").getCount());
	}
	

	@Test
	public void testRepeatPutIsolation() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		String k1t1 = "t1:alice";
		String k1t2 = "t1:alice";
		User user = new User("Alice Adams", "alice@name.com");
		
		// First put
		myMap.put(k1t1, user);
		myMap.put(k1t2, user);
		
		assertEquals(user, myMap.get(k1t1));
		assertEquals(user, myMap.get(k1t2));
		
		// Repeat put
		myMap.put(k1t1, user);
		myMap.put(k1t2, user);
		
		assertEquals(user, myMap.get(k1t1));
		assertEquals(user, myMap.get(k1t2));
	}


	@Test
	public void testPutExpiring() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		String k1 = "t1:alice";
		User user = new User("Alice Adams", "alice@name.com");
		
		myMap.put(k1, user, 60L, TimeUnit.HOURS);
		
		assertEquals(user, myMap.get(k1));
		
		myMap.put(k1, user, 60L, TimeUnit.HOURS);
	}
	

	@Test
	public void testExists() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		String k1 = "t1:alice";
		User u1 = new User("Alice Adams", "alice@name.com");
		
		myMap.put(k1, u1);
		
		assertTrue(DynamoDBStore.getInstances().get(CACHE_NAME).contains(k1));
		
		assertFalse(DynamoDBStore.getInstances().get(CACHE_NAME).contains("t1:no-such-key"));
	}
	

	@Test
	public void testPurgeNonExpiring() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		for (int i=0; i < 200; i ++) {
			
			String key = "t1:a" + i;
			User value = new User("Alice Adams-" + i, "alice-" + i + "@email.com");
			
			assertNull(myMap.putIfAbsent(key, value));
		}
		
		// Nothing to purge with regular user records
		
		DynamoDBStore.getInstances().get(CACHE_NAME).purge(
			Executors.newSingleThreadExecutor(),
			new AdvancedCacheExpirationWriter.ExpirationPurgeListener() {
			
				@Override
				public void marshalledEntryPurged(MarshallableEntry entry) {
					fail();
				}
				
				
				@Override
				public void entryPurged(Object o) {
					fail();
				}
		});
		
		assertEquals(200, myMap.size());
	}
	

	@Test
	public void testQueryMultiple() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		for (int i=0; i < 100; i++) {
			
			assertNull(myMap.putIfAbsent("t1:u" + i, new User("Alice Adams", "alice@email.com")));
		}
		for (int i=100; i < 200; i++) {
			
			assertNull(myMap.putIfAbsent("t1:u" + i, new User("Bob Brown", "bob@email.com")));
		}
		
		QueryExecutor<String, User> queryExecutor = DynamoDBStore.getInstances().get(CACHE_NAME).getQueryExecutor();
		
		List<InfinispanEntry<String, User>> matches = new LinkedList<>();
		queryExecutor.executeQuery(new SimpleMatchQuery<>("email", "alice@email.com"), matches::add);

		matches.forEach(en -> assertEquals("alice@email.com", en.getValue().getEmail()));
		assertEquals(100, matches.size());
	}
	

	@Test
	public void testQueryMultipleHashKeyIsolation() {
		
		Cache<String,User> myMap = cacheMgr.getCache(CACHE_NAME);
		
		for (int i=0; i < 100; i++) {
			
			assertNull(myMap.putIfAbsent("t1:u" + i, new User("Alice Adams", "alice@email.com")));
		}
		for (int i=0; i < 100; i++) {
			
			assertNull(myMap.putIfAbsent("t2:u" + i, new User("Bob Brown", "bob@email.com")));
		}
		
		QueryExecutor<String, User> queryExecutor = DynamoDBStore.getInstances().get(CACHE_NAME).getQueryExecutor();
		
		for (String email: Arrays.asList("alice@email.com", "bob@email.com")) {
			
			List<InfinispanEntry<String, User>> matches = new LinkedList<>();
			queryExecutor.executeQuery(new SimpleMatchQuery<>("email", email), matches::add);
			
			matches.forEach(en -> assertEquals(email, en.getValue().getEmail()));
			assertEquals(100, matches.size());
		}
	}
}
