# Infinispan DynamoDB Cache Store

Cache loader / writer for an AWS DynamoDB database backend.

## Requirements

* Infinispan 14.0+
* Java 17+
* AWS DynamoDB database

## Features

* Implements the `AdvancedLoadWriteStore` SPI.
* Simple interface for transforming Infinispan entries to / from DynamoDB 
  items.
* Optional interface to execute arbitrary queries against DynamoDB, bypassing 
  the standard Infinispan API when the application requires it.
* Optional strongly consistent reads.
* Optional transparent table name prefixing.
* Optional transparent application of a range key to enable multi-tenancy: 
  sharing a table between multiple Infinispan caches of the same type, while 
  keeping data access isolated. The
  `DynamoDBItemTransformer.getRangeKeyAttributeName()` method overrides this 
  configuration setting. 
* If the table is not present the `start` method will create it automatically 
  using the configured table name and other properties.
* Configuration properties to create the table with a specified initial read 
  and write capacity, with enabled encryption at-rest, with enabled stream of 
  view type NEW_AND_OLD_IMAGES (required for setting up a global table with 
  replicas in two or more AWS regions).
* Optional enabling and setting of a time-to-live (TTL) attribute for the 
  table.
* Optional enabling of deletion protection for the table. 
* Optional enabling of continuous backups / point-in-time recovery for the 
  table.
* Configuration of a purge maximum read capacity to use when scanning the table 
  for expired items.
* Optional HTTP proxy.
* Optional securing of the integrity and authenticity of the stored DynamoDB 
  items with HMAC SHA-256.
* Dropwizard Metrics: Read, put, delete, process and purge operation timers.
* System property interpolation for all configuration properties using a 
  `${sys-prop-name:default-value}` format.
* Multi-level logging with Log4j2.
* Open source (Apache 2.0 license).

## Usage

* Add the Maven dependency coordinates for the DynamoDB cache store to your 
  project.
* Implement DynamoDBItemTransformer to translate between Infinispan entries 
  (key / value pairs with optional metadata) and DynamoDB items.
* Configure a DynamoDB store for each Infinispan cache that requires one, by 
  setting the attributes specified in DynamoDBStoreConfiguration. Also, see the 
  example below. Note that the DynamoDB store can be safely shared between 
  multiple replicated / distributed instances of a cache. It can also be used 
  in read-only mode.
* Make sure the AWS credentials for accessing the DynamoDB table(s) are 
  configured in way that the default AWS credentials provider chain can look
  them up, e.g. by setting the `aws.accessKeyId` and `aws.secretKey` Java  
  system properties. See https://docs.aws.amazon.
  com/sdk-for-java/v1/developer-guide/credentials.html
* If the AWS region isn't specified in the XML or programmatic configuration 
  the default AWS region chain will apply. See https://docs.aws.amazon.
  com/sdk-for-java/v1/developer-guide/java-dg-region-selection.html

## Maven

Maven coordinates:

```xml
<dependency>
    <groupId>com.nimbusds</groupId>
    <artifactId>infinispan-cachestore-dynamodb</artifactId>
    <version>[ version ]</version>
</dependency>
```

where [ version ] should be the latest stable version.

## Example XML configuration

```xml
<infinispan xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="urn:infinispan:config:14.0 http://www.infinispan.org/schemas/infinispan-config-14.0.xsd"
            xmlns="urn:infinispan:config:14.0"
            xmlns:dynamodb="urn:infinispan:config:store:dynamodb:3.0">

    <cache-container name="myCacheContainer" default-cache="users">
        <local-cache name="users">
            <persistence>
                <dynamodb:dynamodb-store
                    shared="true"
                    segmented="false"
                    region="eu-central-1"
                    item-transformer="com.nimbusds.infinispan.persistence.dynamodb.UserItemTransformer"
                    table-prefix="myapp_"
                />
            </persistence>
            <memory max-count="100"/>
        </local-cache>
    </cache-container>

</infinispan>
```

If the region is left out of the XML configuration the [default AWS region 
provider chain](https://docs.aws.amazon.
com/sdk-for-java/v1/developer-guide/java-dg-region-selection.html) will apply. 
In that case the region can be conveniently set from the `AWS_REGION` 
environment variable (recommended in cases when a *web identity token* is 
used as AWS credential).


## Change Log

* version 1.0 (2017-09-29)
    * First public release, requires Infinispan 8.2+ and Java 8+.

* version 1.1 (2017-10-11)
    * Simplifies DynamoDBItemTransformer interface. The hash key attribute
      name is returned directly by the getHashKeyAttributeName method. The
      hash key value is resolved and returned directly by the resolveHashKey
      method. This is a breaking change.

* version 1.2 (2017-10-17)
    * Adds DynamoDBQueryExecutor interface and supporting classes for executing
      direct queries against the DynamoDB database. The attributes used as keys
      in the queries must be indexed (as global secondary index).

* version 1.2.1 (2017-10-17)
    * Updates SimpleMatchQueryExecutor to allow key names that otherwise clash
      with reserved key words, such as SUB, in DynamoDB.

* version 1.3 (2017-10-18)
    * Adds MetadataUtils with methods for encoding Infinispan metadata
      timestamps into DynamoDB items and parsing the metadata back.
    * Moves loggers into own package.

* version 1.4 (2017-10-18)
    * Updates MetadataUtils.addMetadata to return the (modified) Item.

* version 1.5 (2017-10-21)
    * Removes aws-access-key-id and aws-secret-access-key DynamoDB store
      configuration attributes, switches to AWS SDK's
      DefaultAWSCredentialsProviderChain which provides a richer set of
      options for setting the AWS credentials.

* version 1.5.1 (2017-11-02)
    * Adds "endpoint" parameter check for non-blank.

* version 1.5.2 (2017-11-02)
    * Fixes "region" parameter parsing (issue #5).
    
* version 1.5.3 (2017-11-24)
    * Switches to com.amazonaws:aws-java-sdk-bundle:1.11.235 dependency with 
      shaded transient dependencies to prevent potential conflicts.

* version 1.6 (2018-04-25)
    * Upgrades to org.infinispan:infinispan-core:9.0.0Final+
    * Upgrades to com.nimbusds:infinispan-cachestore-common:1.4+
    * Upgrades to com.nimbusds:common:2.27+
    * Upgrades to io.dropwizard.metrics:metrics-core:3.1.2+

* version 1.6.1 (2018-04-25)
    * Updates config XML schema.

* version 1.6.2 (2018-04-27)
    * Adds @Store(shared = true) annotation.

* version 2.0 (2018-05-25)
    * Implements AdvancedCacheExpirationWriter which passes the entire purged
      Infinispan entry to the listeners, not just the key.

* version 2.0.1 (2018-05-27)
    * Refactors ExpiredEntryReaper.

* version 2.1 (2018-07-08)
    * Supports DynamoDB table creation with data encryption at rest
      (server-side encryption). Adds new "encryption-at-rest" {true|false} XML
      attribute to the "dynamodb-store" XML element, defaults to "false" (no
      encryption).
    * Bumps AWS SDK dependency to 1.11.362.

* version 2.1.1 (2018-07-08)
    * Removes deprecated DynamoDBStoreConfigurationParser.

* version 2.2 (2018-09-05)
    * Adds a new "enable-stream" configuration XML attribute to create the
      DynamoDB table with an enabled stream of view type NEW_AND_OLD_IMAGES.
      Streams of this type are required to setup a global DynamoDB table with
      replicas in two or more AWS regions (issue #7).

* version 2.3 (2018-09-24)
    * Updates SimpleMatchQueryExecutor to fall back to table scan if no
      global secondary index (GSI) is specified.
    * Updates SimpleMatchQueryExecutor to accept MatchQuery instances,
      converting them to SimpleMatchQueries if necessary.
    * Bumps minimal com.nimbusds:infinispan-cachestore-common dependency
      dependency to 2.1.

* version 2.4 (2018-09-28)
    * Adds a new "enable-continuous-backups" configuration XML attribute to
      create the DynamoDB table with continuous backups / point in time
      recovery enabled.

* version 2.5 (2018-10-01)
    * Sanitises DynamoDB items before writing them to the table. Empty strings,
      binary data and sets, including those in nested maps, are automatically
      pruned to prevent ValidationExceptions.

* version 2.6 (2018-10-03)
    * Extends the programmatic configuration to enable setting a Dropwizard
      metric registry other than the default singleton
      com.nimbusds.common.monitor.MonitorRegistries. Use with
      new ConfigurationBuilder().persistence().metricRegistry(metricRegistry).

* version 2.7 (2018-11-29)
    * Replaces DynamoDBStore.process method with publishEntries to fix a
      threading issue when pre-loading the cache from a DynamoDB table,
      requires Infinispan 9.3+ (see pull request #2 and chat in
      http://c2id.co/6u ).
    * Infinispan 9.3.0Final becomes the minimal supported version.

* version 2.7.1 (2018-11-29)
    * Artificial version bump to repeat failed Maven Central release of 2.7.

* version 2.8 (2019-03-08)
    * Don't load and process expired entries (Infinispan 9.3 check change).

* version 2.9 (2019-03-08)
    * Adds time-to-live (TTL) support, enabled via 
      DynamoDBItemTransformer.getTTLAttributeName.
    * When continuous backup is configured it will also be enabled at startup 
      with an existing DynamoDB table. Previously it will only be enabled when 
      the table is created.
    * Fixes purge timer measurement (iss #11).

* version 3.0 (2019-03-09)
    * Automatic item expiration by DynamoDB is now enabled from the connector 
      configuration (disabled by default). Updates the XML schema (to v1.4) and 
      the programmatic configuration with a new boolean "enable-ttl" attribute, 
      defaults to `false`.
    * Updates the DynamoDBItemTransformer, adds new init(InitContext) method 
      to signal if automatic item expiration is enabled by the configuration.
    * Failing to enable automatic item expiration (TTL) during startup will now 
      result in a PersistenceException.
 
* version 3.1 (2019-03-15)
    * Adds new static MetadataUtils.addMetadata(Item,InternalMetadata,boolean)
      method for adding an optional "ttl" item field for automatic DynamoDB
      item expiration.
  
* version 3.2 (2019-03-29)
    * Adds new DynamoDBStore.getTable() method.

* version 3.3 (2019-03-29)
    * Don't throw PersistenceException when DynamoDBStore.start() tries to 
      enable TTL for the DynamoDB table where TTL is already enabled (iss #12).
    * Don't throw PersistenceException when DynamoDBStore.start() tries to 
      enable continuous backups for the DynamoDB table where continuous backups
      are being enabled (iss #10).
    * Fixes DS0162 error log message, appends exception message.  
    * Logs error instead of warning if the continuous backups status for the 
      DynamoDB table cannot be determined.
    * Logs error instead of warning if the TTL status for the DynamoDB table 
      cannot be determined.

* version 3.3.1 (2019-03-29)
    * Bumps max Infinispan dependency to 9.4.99.Final.
    
* version 3.4 (2019-04-27)
    * Adds new configuration for making strongly consistent reads (eventually 
      consistent by default). Updates the XML schema (to v1.5) and the 
      programmatic configuration with a new boolean "consistent-reads" 
      attribute, defaults to `false`.

* version 3.4.1 (2019-04-27)
    * Fixes configuration logging format for DS0003, DS0012 and DS0014.

* version 3.5 (2019-06-03)
    * Adds new configuration for limiting the number of expired entries to 
      purge during a run of the expired entry reaper task. Updates the XML 
      schema (to v1.6) and the programmatic configuration with a new boolean 
      "purge-limit" attribute, defaults to `-1` (no limit).
    * Updates expired entry reaping to conserve memory.

* version 3.5.1 (2019-10-10)
    * Bumps dependencies.
    
* version 3.5.2 (2019-12-31)
    * Updates to com.amazonaws:aws-java-sdk-bundle:1.11.632
    
* version 3.6 (2020-05-05)
    * Adds configuration support for an HTTP proxy with "http-proxy-host" and
      "http-proxy-port".
    * Updates to com.amazonaws:aws-java-sdk-bundle:1.11.728
    
* version 3.6.1 (2020-05-05)
    * Ignore empty or blank "http-proxy-host" attributes.
    
* version 3.7 (2020-09-07)
    * Stored DynamoDB items can be optionally secured with a SHA-256 based 
      Message Authentication Code (HMAC) by setting a 256-bit BASE64-encoded 
      secret key in the "hmac-sha256-key" DynamoDB configuration XML attribute.
      The DynamoDB items will thus be provided with an "_hmac#256" to secure  
      their integrity and authenticity in storage. If the "_hmac#256" is 
      missing on read or the HMAC SHA-256 check fails the DynamoDB store will 
      throw a PersistenceException with appropriate message and increment the 
      "<cache-name>.dynamoDB.invalidItemHmacCounter". This HMAC SHA-256 
      configuration can be enabled for empty tables only.
      
* version 3.7.1 (2020-09-09)
    * Fixes thread-safety issue in HMAC computation.
    * Logs the entire item JSON on invalid HMAC (DS0131).

* version 3.7.2 (2020-09-09)
    * ItemHMAC switches to MessageDigest.isEqual to prevent potential timing 
      attacks.

* version 3.7.3 (2021-02-12)
    * Updates dependencies.
    * Replaces com.amazonaws:aws-java-sdk-bundle dependency with leaner
      com.amazonaws:aws-java-sdk-dynamodb.

* version 4.0 (2021-02-14)
    * Adds new DynamoDBItemTransformer.getRangeKeyAttributeName method, the 
      default implementation returns null.
    * Adds new DynamoDBItemTransformer.resolvePrimaryKey, the default 
      implementation returns a primary key with a hash key value and no range 
      key value.
    * Deprecates DynamoDBItemTransformer.resolveHashKey in favour of
      resolvePrimaryKey.
    * New PrimaryKeyValue class.

* version 4.1 (2021-02-17)
    * Adds DynamoDBStore.getItemTransformer method.

* version 4.1.1 (2021-02-17)
    * Logs (TRACE level) item put under DS0132.
    * Adds cache name to TRACE level log lines where applicable.

* version 4.1.2 (2021-02-19)
    * Adds extra logging around table creation.

* version 4.1.3 (2021-02-19)
    * Empty or blank apply-range-key must return null in config API (iss #14).
    * Require non-empty range key value when a range key is set (iss #15).

* version 4.1.4 (2021-02-19)
    * Log stored HMAC, computed HMAC and original item when an invalid HMAC is 
      detected (iss #17).

* version 4.1.5 (2021-02-19)
    * Log stored (DS0132) and retrieved (DS0111) item in JSON format.  

* version 4.1.6 (2021-04-12)
    * Makes org.kohsuke.metainf-services:metainf-services:1.8 optional.
    * Updates to com.nimbusds:common:2.45.1 and adds explicit
      org.apache.logging.log4j:log4j-api:2.14.0

* version 4.1.7 (2021-04-18)
    * Bumps and bounds JSON Smart.

* version 4.1.8 (2022-01-04)
    * Updates to com.amazonaws:aws-java-sdk-dynamodb:1.11.1034
    * Updates to com.nimbusds:common:2.45.4
    * Updates to net.minidev:json-smart:2.4.7
    * Updates DropWizard to 4.1.29
    * Updates Log4j to 2.17.1

* version 4.1.9 (2022-01-05)
    * Updates to com.amazonaws:aws-java-sdk-dynamodb:1.12.132
    
* version 4.1.10 (2022-04-18)
    * Updates to com.amazonaws:aws-java-sdk-dynamodb:1.12.201
    * Updates to org.kohsuke.metainf-services:metainf-services:1.9
    * Updates to com.nimbusds:common:2.48
    * Adds dependency to com.amazonaws:aws-java-sdk-sts:1.12.201

* version 4.2 (2022-04-18)
    * Makes the "region" configuration attribute optional. If the "region" 
     property isn't explicitly set the default AWS provider chain will apply.
     The XML schema is updated to v1.9.

* version 4.2.2 (2022-08-12)
    * Bumps dependencies.
  
* version 5.0 (2022-11-12)
    * Upgrades to Infinispan 14.0
    * Upgrades the dynamodb-store XML schema to v2.0.
    * Upgrades to Java 11.
    * Updates to com.amazonaws:aws-java-sdk-dynamodb:1.12.340
    * Updates to com.amazonaws:aws-java-sdk-sts:1.12.340
    * Updates DropWizard to 4.2.12

* version 5.0.1 (2023-02-15)
    * Updates to com.amazonaws:aws-java-sdk-dynamodb:1.12.406
    * Updates to com.amazonaws:aws-java-sdk-sts:1.12.406
    * Updates other dependencies.
    * Fixes NPE in DynamoDBStore.write when Metadata is null.

* version 5.0.2 (2023-09-23)
    * Updates to Infinispan 14.0.17
    * Updates to NimbusDS Common 2.51
    * Updates to JSON Smart 2.4.10
    * Updates to Dropwizard Metrics 4.2.17

* version 5.0.3 (2023-10-25)
    * Updates to com.amazonaws:aws-java-sdk-dynamodb:1.12.471
    * Updates to com.amazonaws:aws-java-sdk-sts:1.12.471
    * Updates to Infinispan 14.0.19
    * Updates to com.nimbusds:common:2.52
    * Updates to DropWizard Metrics 4.2.19
    * Updates to Log4j to 2.21.1

* version 5.1 (2023-11-10)
    * New optional purge-max-read-capacity XML configuration attribute. Sets a
      maximum read capacity to use when scanning a DynamoDB table for expired 
      items. Any write capacity consumed to delete expired items is bounded by 
      the maximum read capacity and will never exceed it. Defaults to -1.0, in 
      which case the maximum read capacity is set to 1/10th of the reported 
      read capacity provisioned for the table.
    * The purge limit configuration is changed from an int to a long type.
    * Unchecked Exceptions thrown in the ExpiredEntryReaper must be logged and 
      swallowed, not thrown (iss #21).

* version 5.1.1 (2023-11-10)
    * Fixes DynamoDBStoreWithXMLConfig_purgeMaxReadCapacityTest metric cleanup. 

* version 5.2 (2023-11-13)
    * The purge-max-read-capacity XML configuration attribute accepts 
      percentage measures, the default value becomes 10.0%.

* version 6.0 (2023-12-10)
    * Java 17.
    * Updates to com.nimbusds:infinispan-cachestore-common:4.0
    * Updates to com.nimbusds:common:3.0.1
    * Updates to JSON Smart 2.5.0
    * Updates to AWS SDK 1.12.609
    * Updates to com.google.guava:guava:32.1.3-jre
    * Updates to DropWizard Metrics 4.2.23
    * Updates to Log4j 2.22.0

* version 6.0.1 (2024-01-29)
    * Updates to AWS SDK 1.12.646
    * Updates to Log4j 2.22.1

* version 7.0 (2024-10-08)
    * Upgrades the XML schema to v3.0.
    * New optional "deletion-protection" XML configuration attribute. Enables
      deletion protection for a new or existing DynamoDB table.
    * Refactors the configuration API.
    * Renames the optional "indexed-attributes" XML configuration property to 
      "index-attributes".
    * Renames the optional "enable-continuous-backups" XML configuration 
      property to "continuous-backups".
    * Renames the optional "enable-stream" XML configuration property to 
      "stream".
    * Enabling "continuous-backups" (point-in-time recovery, PITR) for a table 
      may require the request to be retried due to a transient 
      ContinuousBackupsUnavailableException with a message suggesting retrial.
      This DynamoDB behaviour is observed at times when enabling continuous
      backups after new table creation, despite the prior `waitForActive` call.
      The request will be retried 5 times, with a wait time increasing by 1 
      second (iss #25).
    * Fixes a bug to ensure "continuous-backups" (PITR) is applied to a table 
      that was left in an incomplete PITR state after a 
      ContinuousBackupsUnavailableException without retrial where DynamoDB 
      reports enabled continuous backups and disabled PITR (iss #25).
    * Renames the optional "enable-ttl" XML configuration property to "ttl".
    * Updates to Infinispan 14.0.32.Final
    * Updates to org.kohsuke.metainf-services:metainf-services:1.11
    * Updates to JSON Smart 2.5.1
    * Updates to com.google.guava:guava:33.2.1-jre
    * Updates to DropWizard Metrics 4.2.26
    * Updates to Log4j 2.23.1